/*
 *  CoreObject.h
 *  CoreObject
 *
 *  Created by Oleg Gordiychuck on 30.05.12.
 *  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef CoreObject_
#define CoreObject_

/* The classes below are exported */
#pragma GCC visibility push(default)

class CoreObject
{
	public:
		void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
