/*
 *  CoreObject.cp
 *  CoreObject
 *
 *  Created by Oleg Gordiychuck on 30.05.12.
 *  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include <iostream>
#include "CoreObject.h"
#include "CoreObjectPriv.h"

void CoreObject::HelloWorld(const char * s)
{
	 CoreObjectPriv *theObj = new CoreObjectPriv;
	 theObj->HelloWorldPriv(s);
	 delete theObj;
};

void CoreObjectPriv::HelloWorldPriv(const char * s) 
{
	std::cout << s << std::endl;
};

