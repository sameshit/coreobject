/*
 *  CoreObjectPriv.h
 *  CoreObject
 *
 *  Created by Oleg Gordiychuck on 30.05.12.
 *  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
 *
 */

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class CoreObjectPriv
{
	public:
		void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
