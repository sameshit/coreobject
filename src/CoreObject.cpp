#include "CoreObject.h"

using namespace CoreObjectLib;

CoreObject::CoreObject(bool disable_memory_logging)
{
    COErr::Init();
	_logger = new Logger();
   	_memory	= new Memory(_logger,disable_memory_logging);
	long_new(_memory,_thread_cache,_memory);
	long_new(_memory,_loop,_thread_cache,_memory,_logger);
	_ASSERT_ERROR(_loop->Start());
    srand ((unsigned int) time(NULL) );
}

CoreObject::CoreObject(const char *logger_filename,bool disable_memory_logging)
{
    COErr::Init();
	_logger = new Logger(logger_filename);
   	_memory	= new Memory(_logger,disable_memory_logging);
	long_new(_memory,_thread_cache,_memory);
	long_new(_memory,_loop,_thread_cache,_memory,_logger);
	_ASSERT_ERROR(_loop->Start());
    srand ((unsigned int) time(NULL) );
}

CoreObject::~CoreObject()
{
	_ASSERT_ERROR(_loop->Stop());
	long_delete(_memory,_loop);
	long_delete(_memory,_thread_cache);
  	delete _memory;
	delete _logger;
    COErr::Free();
}
