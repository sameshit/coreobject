#ifndef HRESULTTOSTRING____H
#define HRESULTTOSTRING____H

#include "ComUniquePtr.h"

namespace CoreObjectLib
{
	class WinErrorUtils
	{
	public:
		static const char* HResultToString(HRESULT hr);
		static std::string	GetLastErrorString();
	};

#define HRTOSTRING(hr) CoreObjectLib::WinErrorUtils::HResultToString(hr)
#define PRINTHR(hr) " HResult = " << HRTOSTRING(hr) << " (" << hr << ")"
}
#endif