#ifndef COMUNIQUEPTR__H
#define COMUNIQUEPTR__H

#include "../../common/config.h"

namespace CoreObjectLib
{
template <typename T>
	class ComUniquePtr
	{
	public:
		ComUniquePtr()
		:_ptr(nullptr){}

		ComUniquePtr(T *ptr)
		:_ptr(ptr){}
		
		virtual ~ComUniquePtr()
		{
			if (_ptr != nullptr)
				_ptr->Release();
		}

		T* Detach()		{T *ret = _ptr; _ptr = nullptr; return ret;}
		void Attach(T *ptr) {Reset(); _ptr = ptr;}

		T* Get() {return _ptr;}
		T** Receive() {Reset();return &_ptr;}
		void Reset()
		{
			if (_ptr != nullptr)
			{
				_ptr->Release();
				_ptr = nullptr;
			}
		}

		T* operator->()
		{
			return _ptr;
		}

		T& operator*()
		{
			return *_ptr;
		}
	private:
		T* _ptr;
	};
}

#endif