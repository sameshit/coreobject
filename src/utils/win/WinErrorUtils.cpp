#include "WinErrorUtils.h"
#include <DSound.h>
#include <Mferror.h>

using namespace CoreObjectLib;
using namespace std;

#define HRCASE(err) \
	case err: \
	return #err;

const char* WinErrorUtils::HResultToString(HRESULT hr)
{
	switch(hr)
	{
		HRCASE(E_UNEXPECTED);
		HRCASE(MF_E_INVALIDSTREAMNUMBER);
		HRCASE(MF_E_TRANSFORM_NEED_MORE_INPUT);
		HRCASE(MF_E_TRANSFORM_STREAM_CHANGE);
		HRCASE(MF_E_TRANSFORM_TYPE_NOT_SET);
		HRCASE(E_OUTOFMEMORY);
		HRCASE(S_OK);
		HRCASE(E_INVALIDARG);
		HRCASE(MF_E_INVALIDREQUEST);
		HRCASE(MF_E_INVALIDMEDIATYPE);
		HRCASE(MF_E_INVALIDTYPE);		
		HRCASE(MF_E_TRANSFORM_CANNOT_CHANGE_MEDIATYPE_WHILE_PROCESSING);
		HRCASE(DSERR_BADFORMAT);
		HRCASE(DSERR_INVALIDCALL);
		HRCASE(DSERR_PRIOLEVELNEEDED);
		HRCASE(DSERR_UNSUPPORTED);
		HRCASE(MF_E_NOTACCEPTING);
	default:
		return "Unknown HResult value";
	}
}

std::string WinErrorUtils::GetLastErrorString()
{
    //Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0)
        return "No error message has been recorded";

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), messageBuffer, 0, NULL);

	stringstream ss;
    std::string message(messageBuffer, size);
	ss <<message<<"("<<errorMessageID<<")";

    //Free the buffer.
    LocalFree(messageBuffer);

	return ss.str();
}
