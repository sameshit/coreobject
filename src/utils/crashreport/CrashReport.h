#ifndef CRASHREPORT_H_INCLUDED
#define CRASHREPORT_H_INCLUDED

#include "SystemInfo.h"
#include "../../core/event/Event.h"
#include "../variant/Variant.h"

namespace CoreObjectLib
{
    typedef Event<Variant&> CrashEvent;

    class LIBEXPORT CrashReport
    {
    public:
        static bool Register();
        static bool UnRegister();
        static CrashEvent OnCrash;
    private:
		static bool _registered;
        static void DoReport(const std::string& signum, const std::string& signame,
							const std::string& stacktrace);
#if defined(OS_LINUX)
        static void CatchSignalCallback(int sig_num, siginfo_t * info, void * ucontext);
#elif defined(OS_WIN)
		static LONG WINAPI CatchExceptionCallback(struct _EXCEPTION_POINTERS *ptrs);
#endif
    };
}

#endif // CRASHREPORT_H_INCLUDED
