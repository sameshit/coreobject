#include "CrashReport.h"

using namespace CoreObjectLib;
using namespace std;

bool CrashReport::_registered = false;
CrashEvent CrashReport::OnCrash;

/*
bool CrashReport::Register()
{
    RETURN_MSG_IF_TRUE(_registered,"Crash report is already registered");
#if defined(OS_LINUX)
    struct sigaction sigact;

    sigact.sa_sigaction = CrashReport::CatchSignalCallback;
    sigact.sa_flags = SA_RESTART | SA_SIGINFO;


    RETURN_MSG_IF_TRUE(sigaction(SIGSEGV, &sigact, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGSEGV");
    RETURN_MSG_IF_TRUE(sigaction(SIGABRT, &sigact, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGABRT");
    RETURN_MSG_IF_TRUE(sigaction(SIGFPE, &sigact, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGABRT");
#endif
    _registered = true;
    return true;
}

bool CrashReport::UnRegister()
{
    RETURN_MSG_IF_FALSE(_registered,"Crash report wasn't registered");
#if defined(OS_LINUX)
    RETURN_MSG_IF_TRUE(sigaction(SIGSEGV, (struct sigaction*)NULL, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGSEGV");
    RETURN_MSG_IF_TRUE(sigaction(SIGABRT, (struct sigaction*)NULL, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGABRT");
    RETURN_MSG_IF_TRUE(sigaction(SIGFPE, (struct sigaction*)NULL, (struct sigaction *)NULL) != 0,
                       "Couldn't set sigaction for SIGABRT");
#endif
    _registered = false;
    return true;
}
*/
#if defined(OS_LINUX)
void CrashReport::CatchSignalCallback(int signum, siginfo_t * info, void * ucontext)
{
    DoReport(signum,string(strsignal(signum)));
}
#endif

void CrashReport::DoReport(const string &signum,const string& signame,const string& stacktrace)
{
    Variant report;
    SystemInfo system_info;
    vector<string> function_addrs;
    string str_report;
    size_t cpu_count;

    report["Signal"]["Value"]=signum;
    report["Signal"]["Name"] =signame;
    report["StackTrace"] = stacktrace;

    if (system_info.GetCPUCount(&cpu_count))
    {
        report["CPU"]["Count"]["IsValid"] = true;
        report["CPU"]["Count"]["Value"] = cpu_count;
    }
    else
    {
        report["CPU"]["Count"]["IsValid"] = false;
        report["CPU"]["Count"]["Error"] = COErr::Get();
    }

    if (system_info.GetCPUInfo(&str_report))
    {
        report["CPU"]["Info"]["IsValid"] = true;
        report["CPU"]["Info"]["Value"] = str_report;
    }
    else
    {
        report["CPU"]["Info"]["IsValid"] = false;
        report["CPU"]["Info"]["Error"] = COErr::Get();
    }

    if (system_info.GetRAMInfo(&str_report))
    {
        report["RAM"]["IsValid"] = true;
        report["RAM"]["Value"] = str_report;
    }
    else
    {
        report["RAM"]["IsValid"] = false;
        report["RAM"]["Error"] = COErr::Get();
    }

    if (system_info.GetOSInfo(&str_report))
    {
        report["OS"]["IsValid"] = true;
        report["OS"]["Value"] = str_report;
    }
    else
    {
        report["OS"]["IsValid"] = false;
        report["OS"]["Error"] = COErr::Get();
    }

    OnCrash(report);
    exit(EXIT_FAILURE);
}
