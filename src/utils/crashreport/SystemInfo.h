#ifndef SystemInfo__H
#define SystemInfo__H

#include "../../core/error/COErr.h"

namespace CoreObjectLib
{
    class SystemInfo
    {
        public:
        SystemInfo();
        virtual ~SystemInfo();

        bool GetCPUCount        (size_t *cpu_count);
        bool GetCPUInfo         (std::string *cpu_info);
        bool GetRAMInfo         (std::string *ram_info);
        bool GetOSInfo          (std::string *os_info);
    };
}

#endif
