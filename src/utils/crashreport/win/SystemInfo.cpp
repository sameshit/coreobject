#include "../SystemInfo.h"
#include <Dbghelp.h>

using namespace CoreObjectLib;
using namespace std;

inline DWORD CountSetBits(ULONG_PTR bitMask)
{
	DWORD LSHIFT = sizeof(ULONG_PTR)*8 - 1;
	DWORD bitSetCount = 0;
	ULONG_PTR bitTest = (ULONG_PTR)1 << LSHIFT;
	DWORD i;

	for (i = 0; i <= LSHIFT; ++i)
	{
		bitSetCount += ((bitMask & bitTest)?1:0);
		bitTest/=2;
	}

	return bitSetCount;
}

SystemInfo::SystemInfo()
{

}

SystemInfo::~SystemInfo()
{

}

bool SystemInfo::GetCPUCount(size_t* cpu_count)
{
	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION info = NULL,ptr = NULL;
	DWORD length = 0;
	DWORD byteOffset = 0;

	GetLogicalProcessorInformation(info,&length);
	RETURN_MSG_IF_FALSE(info == nullptr,"GetLogi...() info is not null");
	RETURN_MSG_IF_FALSE(GetLastError() == ERROR_INSUFFICIENT_BUFFER,
						"GetLogi...() failed: "<<GetLastError());
	info = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(length);
	RETURN_MSG_IF_FALSE(GetLogicalProcessorInformation(info,&length)==TRUE,"GetLogi()...2 failed");

	ptr = info;
	*cpu_count = 0;
	while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= length)
    {
		switch (ptr->Relationship)
		{
			case RelationProcessorCore:
				*cpu_count += CountSetBits(ptr->ProcessorMask);
			break;
			default:
			break;
		}
		byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
		ptr++;
	}
	free(info);
	return true;
}

bool SystemInfo::GetCPUInfo(string *cpu_info)
{
	HKEY central_processor,processor;
	LONG ret_val;
	DWORD i,key_size,value_size;
	char key_name[1024],value[1024];
	stringstream ss;
	string central_proc_string = "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\";

	ret_val = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
						  central_proc_string.c_str(),
					      0,KEY_READ,&central_processor);

	RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS,"RegOpenKeyEx error: "<<ret_val);
	i = 0;
	do
	{
		stringstream full_path;

		key_size = sizeof(key_name);
		value_size = sizeof(value);

		ret_val = RegEnumKeyEx(central_processor,i,key_name,&key_size,
							   nullptr,nullptr,nullptr,nullptr);
		if (ret_val == ERROR_NO_MORE_ITEMS)
			break;

		RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS,
						  "RegEnumKeyEx error: "<<ret_val);
		full_path << central_proc_string<<key_name;
		ret_val = RegOpenKeyEx(HKEY_LOCAL_MACHINE,full_path.str().c_str(),0,KEY_READ,&processor);
		RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS,
							"RegOpenKeyEx2 error: "<<ret_val<<", while opening: "<<full_path.str());

		ret_val = RegQueryValueEx(processor,"ProcessorNameString",
								  nullptr,nullptr,(LPBYTE)value,&value_size);
		RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS,
							"RegQueryValueEx error: "<<ret_val);
		ss << "["<<i<<"]: "<<string(value,value_size)<<endl;
		ret_val = RegCloseKey(processor);
		RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS, "RegCloseKey2 error: "<<ret_val);
		++i;
	}
	while (true);

	ret_val = RegCloseKey(central_processor);
	RETURN_MSG_IF_FALSE(ret_val == ERROR_SUCCESS,"RegCloseKey error: "<<ret_val);
	*cpu_info = ss.str();
	return true;
}

bool SystemInfo::GetRAMInfo(string *ram_info)
{
	MEMORYSTATUSEX statex;
	stringstream ss;

	statex.dwLength = sizeof (statex);

	RETURN_MSG_IF_TRUE(GlobalMemoryStatusEx (&statex) == FALSE, 
		"GlobalMemoryStatusEx failed: "<<GetLastError());

	ss <<"Memory usage: "<< statex.dwMemoryLoad<<"%"<<endl<<
		"Total memory: "<<statex.ullTotalPhys/1024<<" kb"<<endl<<
		"Free memory: "<<statex.ullAvailPhys/1024<<" kb";

	*ram_info = ss.str();
	return true;
}



bool SystemInfo::GetOSInfo(string *os_info)
{
	OSVERSIONINFOEX version_info;
	stringstream ss;

	version_info.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	RETURN_MSG_IF_FALSE(GetVersionEx((OSVERSIONINFO*)&version_info)==TRUE,
		"GetVersionEx error: "<<GetLastError());

	switch(version_info.dwMajorVersion)
	{
	case 5:
		switch (version_info.dwMinorVersion)
		{
		case 0:
			ss << "Windows 2000";
		break;
		case 1:
			ss << "Windows XP";;
		break;
		case 2:
			if (GetSystemMetrics(SM_SERVERR2) == 0)
				ss<< "Windows Server 2003";
			else
				ss<< "Windows Server R2";
		break;
		default:
			ss<< "Unknown";
		}
	break;
	case 6:
		switch(version_info.dwMinorVersion)
		{
		case 0:
			if (version_info.wProductType == VER_NT_WORKSTATION)
				ss<< "Windows Vista";
			else
				ss<<"Windows Server 2008";
		break;
		case 1:
			if (version_info.wProductType == VER_NT_WORKSTATION)
				ss<< "Windows 7";
			else
				ss<<"Windows Server 2008 R2";
		break;
		case 2:
			if (version_info.wProductType == VER_NT_WORKSTATION)
				ss<< "Windows 8";
			else
				ss<<"Windows Server 20012";
		break;
		default:
			ss << "Unknown";
		}
	break;
	default:
		ss << "Unknown";
	break;
	}

	ss<<", "<<version_info.szCSDVersion;
	ss<<". Major: "<<version_info.dwMajorVersion<<", minor: "<<version_info.dwMinorVersion;
	ss<<". SP major: "<<version_info.wServicePackMajor<<", SP minor: "<<version_info.wServicePackMinor;

	*os_info = ss.str();
	return true;
}