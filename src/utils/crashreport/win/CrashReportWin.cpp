#include "../CrashReport.h"
#include <Dbghelp.h>

using namespace CoreObjectLib;
using namespace std;

bool CrashReport::Register()
{
    RETURN_MSG_IF_TRUE(_registered,"Crash report is already registered");

	RETURN_MSG_IF_TRUE(SetUnhandledExceptionFilter(CrashReport::CatchExceptionCallback) ==nullptr,
					  "Couldn't set crash callback");
    _registered = true;
    return true;
}

bool CrashReport::UnRegister()
{
	RETURN_MSG_IF_FALSE(_registered,"Crash report is not registered");

	SetUnhandledExceptionFilter(nullptr);
	_registered = false;
	return true;
}

void ContextToStackFrame(const CONTEXT& ctx, STACKFRAME64& sf)
{
	sf.AddrReturn.Mode = sf.AddrFrame.Mode = sf.AddrPC.Mode = sf.AddrStack.Mode = sf.AddrBStore.Mode = AddrModeFlat;
	sf.Virtual = TRUE;

#ifndef _WIN64

	sf.AddrPC.Offset = sf.AddrReturn.Offset = ctx.Eip;
	sf.AddrStack.Offset = ctx.Esp;
	sf.AddrFrame.Offset = ctx.Ebp;
	sf.AddrBStore = sf.AddrFrame;

#elif (defined(_IA64_) || defined(_M_IA64_))

	sf.AddrPC.Offset = sf.AddrReturn.Offset = ctx.StIIP;
	sf.AddrStack.Offset = ctx.IntSp;
	sf.AddrBStore.Offset = ctx.RsBSP;
	sf.AddrFrame = sf.AddrBStore;
		
#elif (defined(_AMD64_) || defined(_M_AMD64))

	sf.AddrPC.Offset = sf.AddrReturn.Offset = ctx.Rip;
	sf.AddrFrame.Offset = ctx.Rbp;
	sf.AddrStack.Offset = ctx.Rsp;
	sf.AddrBStore = sf.AddrFrame;

#elif
#error "Unknown Platform for context structure"
#endif
}

bool AddressToSymbol(HANDLE hProc, DWORD64 address, std::string& name)
{
	bool success = false;
	std::ostringstream outputBuffer;
	outputBuffer << reinterpret_cast<PVOID>(address) << " : ";

	// setup the variables and structures required
	BYTE arr[sizeof(SYMBOL_INFO) + (MAX_SYM_NAME - 1)] = {0};
	PSYMBOL_INFO inf = reinterpret_cast<PSYMBOL_INFO>(arr);
	inf->SizeOfStruct = sizeof(SYMBOL_INFO);
	inf->MaxNameLen = MAX_SYM_NAME;
	
	DWORD64 disp = 0;
	IMAGEHLP_MODULE64 modInf = {0};
	modInf.SizeOfStruct = 0x248;
	// modInf.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);

	if(SymGetModuleInfo64(hProc, address, &modInf))
	{
		outputBuffer << modInf.ModuleName;
		if(SymFromAddr(hProc, address, &disp, inf))
		{
			outputBuffer << "!" << inf->Name;
			if(disp)
			{
				outputBuffer << "+0x" << std::hex << disp;
			}
		}
		else
		{
			if(modInf.BaseOfImage)
			{
				outputBuffer << "+0x" << std::hex << address - modInf.BaseOfImage;
			}
		}
		name = outputBuffer.str();
		success = true;
	}
	return success;
}

#define SIGCASE(name) \
	case name: \
	signame = #name; \
	break

LONG CrashReport::CatchExceptionCallback(struct _EXCEPTION_POINTERS *ptrs)
{
	string signame,cur_addr;
	stringstream signum,stacktrace;

	switch(ptrs->ExceptionRecord->ExceptionCode)
	{
		SIGCASE(EXCEPTION_ACCESS_VIOLATION);
		SIGCASE(EXCEPTION_ARRAY_BOUNDS_EXCEEDED);
		SIGCASE(EXCEPTION_BREAKPOINT);
		SIGCASE(EXCEPTION_DATATYPE_MISALIGNMENT);
		SIGCASE(EXCEPTION_FLT_DENORMAL_OPERAND);
		SIGCASE(EXCEPTION_FLT_DIVIDE_BY_ZERO);
		SIGCASE(EXCEPTION_FLT_INEXACT_RESULT);
		SIGCASE(EXCEPTION_FLT_INVALID_OPERATION);
		SIGCASE(EXCEPTION_FLT_OVERFLOW);
		SIGCASE(EXCEPTION_FLT_STACK_CHECK);
		SIGCASE(EXCEPTION_FLT_UNDERFLOW);
		SIGCASE(EXCEPTION_ILLEGAL_INSTRUCTION);
		SIGCASE(EXCEPTION_IN_PAGE_ERROR);
		SIGCASE(EXCEPTION_INT_DIVIDE_BY_ZERO);
		SIGCASE(EXCEPTION_INT_OVERFLOW);
		SIGCASE(EXCEPTION_INVALID_DISPOSITION);
		SIGCASE(EXCEPTION_NONCONTINUABLE_EXCEPTION);
		SIGCASE(EXCEPTION_PRIV_INSTRUCTION);
		SIGCASE(EXCEPTION_SINGLE_STEP);
		SIGCASE(EXCEPTION_STACK_OVERFLOW);
	default:
		signame = "Unkonwn";
	}
	signum << ptrs->ExceptionRecord->ExceptionCode;

	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, GetCurrentProcessId());
	if(hProc)
	{
		//DWORD opts = SymGetOptions();
		SymSetOptions(SYMOPT_UNDNAME | SYMOPT_CASE_INSENSITIVE | SYMOPT_LOAD_LINES | 
									SYMOPT_OMAP_FIND_NEAREST | SYMOPT_NO_PROMPTS | 
									SYMOPT_DEBUG);
		if(SymInitialize(hProc, NULL, TRUE))
		{
			std::vector<std::string> frames;
			// make a copy of th context since it'll be modified by StackWalk64
			CONTEXT ctx = *(ptrs->ContextRecord);
			STACKFRAME64 frame = {0};
			// initialize the stackframe from the context
			ContextToStackFrame(ctx, frame);
			// get the machine type from the exe header
			PIMAGE_NT_HEADERS pHeaders = ImageNtHeader(GetModuleHandle(NULL));
			WORD machType = pHeaders->FileHeader.Machine;
			// do the hustle

			AddressToSymbol(hProc,
				(DWORD64)ptrs->ExceptionRecord->ExceptionAddress,
				cur_addr);
			stacktrace << cur_addr<<endl;

			while(StackWalk64(machType, hProc, GetCurrentThread(), &frame, &ctx, NULL,
					&SymFunctionTableAccess64, &SymGetModuleBase64, NULL))
			{
				std::string symName;
				if(AddressToSymbol(hProc, frame.AddrPC.Offset, symName))
					stacktrace << symName <<endl;
			}
			// display the trace
		}
		CloseHandle(hProc);
	}
	DoReport(signum.str(),signame,stacktrace.str());
	return EXCEPTION_CONTINUE_SEARCH;
}