#include "../SystemInfo.h"

using namespace CoreObjectLib;
using namespace std;

SystemInfo::SystemInfo()
{

}

SystemInfo::~SystemInfo()
{

}

bool SystemInfo::GetCPUCount(size_t *count)
{
    size_t bytes_read;
    FILE * fp;
    char res[128];
    char * result;

    fp = popen("/bin/cat /proc/cpuinfo |grep -c '^processor'","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);

    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    result = new char[2];
    result[0] = res[0];
    result[1] = '\0';

    *count = atoi(result);
    delete [] result;
    return true;
}

bool SystemInfo::GetCPUInfo(std::string *cpu_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("/bin/cat /proc/cpuinfo | grep 'model name'","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    cpu_info->assign(res,bytes_read);
    return true;
}

bool SystemInfo::GetRAMInfo(std::string *ram_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("/bin/cat /proc/meminfo | grep -e MemTotal -e MemFree","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    ram_info->assign(res,bytes_read);
    return true;
}

bool SystemInfo::GetOpenGLInfo(std::string *opengl_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("glxinfo | grep OpenGL","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    opengl_info->assign(res,bytes_read);
    return true;
}
bool SystemInfo::GetVideoInfo(string *video_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("lspci | grep VGA","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    video_info->assign(res,bytes_read);
    return true;
}

bool SystemInfo::GetAudioInfo(string *audio_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("lspci | grep audio","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    audio_info->assign(res,bytes_read);
    return true;
}

bool SystemInfo::GetOSInfo(string *os_info)
{
    uint32_t count;
    size_t bytes_read;
    FILE * fp;
    char res[1024];

    fp = popen("uname -a","r");
    RETURN_MSG_IF_TRUE(fp == nullptr,"popen() failed: "<<errno);
    bytes_read = fread(res, 1, sizeof(res)-1, fp);
    fclose(fp);
    RETURN_MSG_IF_TRUE(bytes_read <= 0, "fread() failed("<<bytes_read<<"). Errno: "<<errno);

    os_info->assign(res,bytes_read);
    return true;
}

bool SystemInfo::GetStackTrace(string *summary, vector<string> *function_addresses)
{
    void * array[150];
    char ** messages;
    int size;
    char *offset_end;
    size_t bracket_pos;
    string line;

    size = backtrace(array, sizeof(array));
    messages = backtrace_symbols(array, size);
    RETURN_MSG_IF_TRUE(messages == nullptr, "backtrace_symbols() returned NULL");

    summary->clear();
    for (int i = 1; i < size; ++i)
    {
        offset_end = nullptr;

        line.assign(messages[i]);
        bracket_pos = line.find('[');
        if (bracket_pos == string::npos)
            function_addresses->push_back(string("!!! Couldn't parse"));
        else
            function_addresses->push_back(line.substr(bracket_pos+1,line.size()-bracket_pos-2));
        summary->append(line);
        summary->append("\n");
    }
    free(messages);
    return true;
}
