/*
 * File:   Utils.h
 * Author: v0id
 *
 * Created on 3 січня 2011, 23:54
 */

#ifndef UTILS_H
#define	UTILS_H

#include "Rational.h"

namespace CoreObjectLib
{

namespace Utils
{
    template <typename Enumeration>
    auto AsInteger(Enumeration const value)
    -> typename std::underlying_type<Enumeration>::type
    {
        return static_cast<typename std::underlying_type<Enumeration>::type>(value);
    }
    
    inline uint16_t GetBe16(const uint8_t *data)
    {
          uint16_t val;
          val =  data[0]  << 8;
          val |= data[1];
          return val;
    }

    inline uint32_t GetBe32(const uint8_t *data)
    {
        uint32_t val;
        val =  GetBe16(data) << 16;
        val |= GetBe16(&data[2]);
        return val;
    }

    inline uint64_t GetBe64(const uint8_t *data )
    {
          uint64_t val;
          val =  (uint64_t)GetBe32(data) << 32;
          val |= (uint64_t)GetBe32(&data[4]);
          return val;
    }
    inline void PutBe16(uint8_t *data,uint16_t val)
    {
	  data[0] = val >> 8;
	  data[1] = (uint8_t)val;
    }
    inline void PutBe32(uint8_t* data,uint32_t val)
    {
          data[0] = val >> 24;
          data[1] = val >> 16;
          data[2] = val >> 8;
          data[3] = val;
    }
    inline void PutBe64(uint8_t* data,uint64_t val)
    {
          PutBe32(data,     (uint32_t)(val >> 32));
          PutBe32(&data[4], (uint32_t)(val & 0xffffffff));
    }
    inline void PutByte(uint8_t* data,int val)
    {
        data[0] = val;
    }

    inline int GetByte(const uint8_t* data)
    {
        return data[0];
    }

    inline uint8_t GetTrueBit(int pos)
    {
        uint8_t ret_val;
        switch (pos)
        {
            case 0:
                ret_val = 1;
            break;
            case 1:
                ret_val = 2;
            break;
            case 2:
                ret_val = 4;
            break;
            case 3:
                ret_val = 8;
            break;
            case 4:
                ret_val = 16;
            break;
            case 5:
                ret_val = 32;
            break;
            case 6:
                ret_val = 64;
            break;
            case 7:
                ret_val = 128;
            break;
            default:
                assert(false);
        }
        return ret_val;
    }


#ifdef OS_WIN
	inline bool SetSocketNonBlock(SOCKET &sock)
#else
	inline bool SetSocketNonBlock(int &sock)
#endif
	{
#ifdef OS_WIN
		u_long non_block_mode = 1;

		return ioctlsocket(sock,FIONBIO,&non_block_mode) == NO_ERROR;
#else
		int fcntl_flags = 0;
	    fcntl_flags = fcntl(sock, F_GETFL, 0);

		if(fcntl_flags == -1)
			return false;
        if(fcntl(sock, F_SETFL, fcntl_flags | O_NONBLOCK) == -1)
            return false;
		return true;
#endif
	}

#ifdef OS_WIN
	inline void CloseSocket(SOCKET &sock)
#else
	inline void CloseSocket(int &sock)
#endif
	{
#ifdef OS_WIN
		closesocket(sock);
#else
		close(sock);
#endif
	}

#ifdef OS_WIN
	inline bool SocketReuseAddr(SOCKET &sock)
#else
	inline bool SocketReuseAddr(int &sock)
#endif
	{
#ifdef OS_WIN
	int on = 1;

	return setsockopt( sock, SOL_SOCKET,SO_REUSEADDR, (char *)&on,sizeof(on)) == on;
#else
	int on = 1;

    return setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) ) == 0;
#endif
	}

	inline void Delay(uint32_t milliseconds)
	{
#ifdef OS_WIN
		Sleep((DWORD)milliseconds);
#else
		usleep(milliseconds * 1000);
#endif
	}

#ifdef OS_WIN
	inline DWORD CountSetBits(ULONG_PTR bitMask)
	{
		DWORD LSHIFT = sizeof(ULONG_PTR)*8 - 1;
		DWORD bitSetCount = 0;
		ULONG_PTR bitTest = (ULONG_PTR)1 << LSHIFT;
		DWORD i;

		for (i = 0; i <= LSHIFT; ++i)
		{
			bitSetCount += ((bitMask & bitTest)?1:0);
			bitTest/=2;
		}

		return bitSetCount;
	}
#endif

	inline uint32_t GetProcessorCount()
	{
		#ifdef OS_WIN
		PSYSTEM_LOGICAL_PROCESSOR_INFORMATION info = NULL,ptr = NULL;
		DWORD length = 0;
		DWORD byteOffset = 0;
		uint32_t count = 0;

		GetLogicalProcessorInformation(info,&length);
		assert(info == NULL);
		assert(GetLastError() == ERROR_INSUFFICIENT_BUFFER);
		info = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(length);
		assert(GetLogicalProcessorInformation(info,&length)==TRUE);


		ptr = info;
		while (byteOffset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= length)
        {
			switch (ptr->Relationship)
			{
				case RelationProcessorCore:
					count += CountSetBits(ptr->ProcessorMask);
				break;
				default:

				 break;
			}
			byteOffset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
			ptr++;
		}
		free(info);
		return count;

		#elif defined (OS_LINUX)

        uint32_t count;
        size_t bytes_read;
        FILE * fp;
        char res[128];
        fp = popen("/bin/cat /proc/cpuinfo |grep -c '^processor'","r");
        assert(fp != 0);
        bytes_read = fread(res, 1, sizeof(res)-1, fp);
        fclose(fp);

        char * result = new char[2];
        result[0] = res[0];
        result[1] = '\0';

        count = atoi(result);

        delete [] result;

        return count;

		#else
        int nm[2];
        size_t len = 4;
        uint32_t count;

        nm[0] = CTL_HW; nm[1] = HW_AVAILCPU;
        sysctl(nm, 2, &count, &len, NULL, 0);

        if(count < 1) {
            nm[1] = HW_NCPU;
            sysctl(nm, 2, &count, &len, NULL, 0);
            if(count < 1) { count = 1; }
        }
        return count;
		#endif
	}

    inline void* AllocAligned(uint32_t alignment, size_t size)
	{
#ifdef OS_WIN
		return _aligned_malloc(size,alignment);
#else
		void *ptr;
		assert( posix_memalign(&ptr,alignment,size) == 0);
		return ptr;
#endif
	}

    inline void FreeAligned(void *ptr)
	{
#ifdef OS_WIN
		_aligned_free(ptr);
#else
		free(ptr);
#endif
	}
}

}

#endif	/* UTILS_H */

