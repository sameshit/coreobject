#ifndef COTYPES__H
#define COTYPES__H

#include "../../common/config.h"

namespace CoreObjectLib
{
	class Rational
	{
	public:
		Rational(const uint32_t &num=0,const uint32_t &denom=0):numerator(num),denomerator(denom) {}
		virtual ~Rational() {}


		uint32_t numerator,denomerator;

		inline float AsFloat()
		{
			return ((float)numerator)/((float)denomerator);
		}

		inline double AsDouble()
		{
			return ((double)numerator)/((double)denomerator);
		}

		inline uint32_t AsUint32()
		{
			return numerator/denomerator;
		}

		inline Rational& operator=(const Rational& other)
		{
			numerator = other.numerator;
			denomerator = other.denomerator;
			return *this;
		}

		inline bool operator== (const Rational& other) const
		{
			return numerator == other.numerator && denomerator == other.denomerator;
		}
		inline bool operator!= (const Rational& other) const
		{
			return !((*this)==other);
		}
		inline bool operator < (const Rational& other) const
		{
			return numerator*other.denomerator<other.numerator*denomerator;
		}
		inline bool operator <= (const Rational& other) const
		{
			return numerator*other.denomerator<=other.numerator*denomerator;
		}
		inline bool operator > (const Rational& other) const
		{
			return numerator*other.denomerator>other.numerator*denomerator;
		}
		inline bool operator >= (const Rational& other) const
		{
			return numerator*other.denomerator>=other.numerator*denomerator;
		}
	};
}

#endif