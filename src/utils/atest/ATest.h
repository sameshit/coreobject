//
//  ATest.h
//  CoreObject
//
//  Created by Oleg on 10.01.13.
//
//

#ifndef CoreObject_ATest_h
#define CoreObject_ATest_h

#include "../../core/time/COTime.h"

namespace CoreObjectLib
{
    class LIBEXPORT ATest
    {
    protected:
        bool _passed;
        std::string _name;
    public:
        ATest(const char *name);
        virtual ~ATest();
        virtual void Run();
        static bool Check(const char *name);

        static bool RunAll();
    };    
    
#define ATEST_FRIEND(name) friend class Test##name;
#define ATEST_CHECK(name)  CoreObjectLib::ATest::Check(name);
    
    
#define ATEST_COERR(a) if (!a){std::cout<<std::endl<<"Expression "<<#a<<" threw error: "<<COErr::Get()<<std::endl;_passed=false;return;}
#define ATEST_TRUE(a) if (!a){std::cout<<std::endl<<"Expression "<<#a<<" is not equal true"<<std::endl;_passed=false;return;}
#define ATEST_EQUAL(a,b) if (a != b){std::cout<<std::endl<<"Expression "<<#a<<" != "<<#b<<" ("<<a<<" != "<<b<<")"<<std::endl;_passed=false;return;}
#define ATEST_NOT_EQUAL(a,b) if (a == b){std::cout<<std::endl<<"Expression "<<#a<<" == "<<#b<<" ("<<a<<" == "<<b<<")"<<std::endl;_passed=false;return;}
#define ATEST_LESS(a,b) if (a >= b){std::cout<<std::endl<<"Expression "<<#a<<" >= "<<#b<<" ("<<a<<" >= "<<b<<")"<<std::endl;_passed=false;return;}
#define ATEST_LESS_EQUAL(a,b) if (a > b){std::cout<<std::endl<<"Expression "<<#a<<" > "<<#b<<" ("<<a<<" > "<<b<<")"<<std::endl;_passed=false;return;}
#define ATEST_GREATER(a,b) if (a <= b){std::cout<<std::endl<<"Expression "<<#a<<" <= "<<#b<<" ("<<a<<" <= "<<b<<")"<<std::endl;_passed=false;return;}
#define ATEST_GREATER_EQUAL(a,b) if (a < b){std::cout<<std::endl<<"Expression "<<#a<<" < "<<#b<<" ("<<a<<" < "<<b<<")"<<std::endl;_passed=false;return;}

#define ATEST(name) \
    class Test##name \
    :public CoreObjectLib::ATest \
    { public: \
        Test##name(const char *test_name) \
        :CoreObjectLib::ATest(test_name) { _passed=true; } \
        void Run(); \
    }; \
    Test##name name(#name); \
    void Test##name::Run()

}

#endif
