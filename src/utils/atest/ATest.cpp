//
//  ATest.cpp
//  CoreObject
//
//  Created by Oleg on 11.01.13.
//
//

#include "ATest.h"

using namespace CoreObjectLib;
using namespace std;

typedef map<string,ATest*> ATestMap;
typedef map<string,ATest*>::iterator ATestMapIterator;

ATestMap *_all_tests = NULL;

ATest::ATest(const char *name)
:_passed(false),_name(name)
{
    pair<ATestMapIterator,bool> some_pair;
    
    if (_all_tests == NULL)
        _all_tests = new ATestMap;
    
    some_pair = _all_tests->insert(make_pair(_name,this));
    if (some_pair.second == false)
    {
        cout<<"Trying to create test with existing name("<<name<<"). Aborting..."<<endl;
        abort();
    }
}

ATest::~ATest()
{
    _all_tests->erase(_name);
    if (_all_tests->empty())
    {
        delete _all_tests;
        _all_tests = NULL;
    }
}

void ATest::Run()
{
    std::cout <<"ATest internal error: Run() method wasn't implemented!"<< std::endl;
}

bool ATest::RunAll()
{
    ATestMapIterator it;
    bool passed = true;
    COTime begin,end;
    uint32_t i;
    
    if (_all_tests == NULL)
    {
        std::cout<<"There are no tests for running"<<std::endl;
        return false;
    }
    
    for (it = _all_tests->begin(),i=0;
         it !=_all_tests->end() && passed;
         ++it,++i)
    {
        std::cout <<"*"<<i<<". Running test "<<(*it).first <<"...";
        
        begin.Update();
        (*it).second->Run();
        end.Update();
        
        passed = (*it).second->_passed;
        if (passed)
            std::cout<<"PASSED("<<end-begin<<" ms)"<<std::endl;
        else
            std::cout<<"FAILED("<<end-begin<<" ms)"<<std::endl;
    }
    
    return passed;
}

bool ATest::Check(const char* name)
{
    COTime begin,end;
    string str_name(name);
    ATestMapIterator it;
    
    it = _all_tests->find(str_name);
    if (it == _all_tests->end())
    {
        cout <<"* No such test with name "<<str_name<<endl;
        return false;
    }
    
    cout <<"* Running test "<<str_name <<"...";
    
    begin.Update();
    (*it).second->Run();
    end.Update();
    
    if ((*it).second->_passed)
        std::cout<<"PASSED("<<end-begin<<" ms)"<<std::endl;
    else
        std::cout<<"FAILED("<<end-begin<<" ms)"<<std::endl;
    
    return (*it).second->_passed;
}