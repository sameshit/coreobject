//
//  Variant.cpp
//  CoreObject
//
//  Created by Oleg on 19.06.13.
//
//

#include "Variant.h"

using namespace CoreObjectLib;
using namespace std;

Variant::Variant(const std::string& name)
:_name(name),_type(VariantType::Unknown)
{

}

Variant::Variant(const char *name)
:_name(name),_type(VariantType::Unknown)
{

}

Variant::~Variant()
{
    Clear();
}

void Variant::Clear()
{
    for (auto it = _map.begin();
         it != _map.end();
         ++it)
        delete (*it).second;

    _map.clear();
}

bool Variant::Delete(const char *keyname)
{
    auto it = _map.find(keyname);
    if (it == _map.end())
        return false;
    else
    {
        delete (*it).second;
        _map.erase(it);
        return true;
    }
}

bool Variant::SaveToFile(const char *filename, const std::string &data)
{
    ofstream os;

    os.open(filename);
    if (os.is_open())
    {
        os << data;
        os.close();
        return true;
    }
    else
    {
        COErr::Set("Couldn't open file");
        return false;
    }
}

bool Variant::LoadFromFile(const char *filename, std::string *data)
{
    ifstream is;

    is.open(filename,ifstream::in | ifstream::binary);
    if (is.is_open())
    {
        is.seekg(0, ios::end);
        data->reserve((size_t)is.tellg());
        is.seekg(0, ios::beg);

        data->assign((istreambuf_iterator<char>(is)),
                      istreambuf_iterator<char>());

        is.close();
        return true;
    }
    else
    {
        COErr::Set("Couldn't open file");
        return false;
    }
}

#define PRINT_TAB \
    for (i = 0; i < tab_count; ++i) \
        ss <<"    ";

void Variant::SerializeXML(std::string *data, uint32_t tab_count)
{
    stringstream ss;
    string str;
    uint32_t i;


    PRINT_TAB
    ss << "<" << _name << ">";
    if (_type != VariantType::Variant)
        ss << _val;
    else
    {
        for (auto it = _map.begin();
             it != _map.end();
             ++it)
        {
            (*it).second->SerializeXML(&str,tab_count + 1);
            ss<<endl<<str;
        }
        ss<<endl;
        PRINT_TAB
    }
    ss << "</"<<_name<<">";
    data->assign(ss.str());
}

bool Variant::SerializeXMLToFile(const char *filename)
{
    string str;

    SerializeXML(&str);
    return SaveToFile(filename, str);
}


bool Variant::DeserializeXML(const string &data)
{
    ssize_t out_pos;
    Clear();

    return ParseXML(data, &out_pos);
}

bool Variant::ParseXML(const std::string &data,ssize_t *out_pos)
{
    string open_tag,data_inside;
    ssize_t open_tag_begin,open_tag_end,close_tag_begin,temp,inside_pos,close_tag_to_skip;
    Variant *child;
    bool parse_success;

    open_tag_begin = data.find("<");
    open_tag_end   = data.find(">");

    if (open_tag_begin == string::npos || open_tag_end == string::npos || open_tag_begin > open_tag_end)
    {
        COErr::Set("XML parsing error: couldn't find open tag");
        return false;
    }
    open_tag = data.substr(open_tag_begin+1,open_tag_end-open_tag_begin-1);

    close_tag_to_skip = 0;
    temp = open_tag_begin + 1;
    close_tag_begin = data.find("</"+open_tag+">",temp);
    if (close_tag_begin == string::npos)
    {
        COErr::Set("XML parsing error: couldn't find close tag");
        return false;
    }
    for (;
         open_tag_begin != string::npos && close_tag_begin > open_tag_begin;
         temp=open_tag_begin+1, ++close_tag_to_skip)
        open_tag_begin = data.find("<"+open_tag+">",temp);

    for (;
         close_tag_to_skip > 0 && close_tag_begin != string::npos;
         temp=close_tag_begin+1,--close_tag_to_skip)
        close_tag_begin = data.find("</"+open_tag+">",temp);

    if (close_tag_begin == string::npos)
    {
        COErr::Set("XML parsing error: couldn't find close tag");
        return false;
    }

    data_inside = data.substr(open_tag_end+1,close_tag_begin-open_tag_end-1);
    *out_pos = close_tag_begin+open_tag.size()+3;
//    cout <<"'"<< data.substr(*out_pos)<<"'" <<endl;
//    cout <<"'"<< data_inside <<"'"<< endl;
    _name = open_tag;


    parse_success = false;
    inside_pos = 0;

    child = new Variant;
    while (child->ParseXML(data_inside,&inside_pos))
    {
        parse_success = true;
        _map.insert(make_pair(child->GetName(), child));
        child = new Variant;
        data_inside = data_inside.substr(inside_pos);
    }
    delete child;

    if (parse_success)
        _type = VariantType::Variant;
    else
    {
		_type = VariantType::String;
        _val = data_inside;
    }

    return true;
}

bool Variant::DeserializeXMLFromFile(const char *filename)
{
    string str;

    if (!LoadFromFile(filename, &str))
        return false;

    return DeserializeXML(str);
}

bool Variant::SetType(const VariantType &type)
{
	RETURN_MSG_IF_TRUE (type == VariantType::Variant || _type == VariantType::Variant,
		"Couldn't set type to or from VariantType::Variant");
	_type = type;
	return true;
}
