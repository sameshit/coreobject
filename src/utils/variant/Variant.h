//
//  Variant.h
//  CoreObject
//
//  Created by Oleg on 19.06.13.
//
//

#ifndef __CoreObject__Variant__
#define __CoreObject__Variant__

#include "../../common/config.h"
#include "../../core/error/COErr.h"

namespace CoreObjectLib
{
    enum class VariantType
    {
		Unknown,
        UnsignedInt8,
		UnsignedInt16,
		UnsignedInt32,
		UnsignedInt64,
		SignedInt8,
		SignedInt16,
		SignedInt32,
		SignedInt64,
		Float,
		Double,
		Bool,
		String,
        Variant,
    };

    class Variant;

    typedef std::unordered_map<std::string,Variant*> VariantMap;
    typedef std::unordered_map<std::string,Variant*>::iterator VariantMapIterator;

    class Variant
    {
    public:
        Variant(const std::string &name);
        Variant(const char *name = "");
        virtual ~Variant();

        void        SerializeXML(std::string *data, uint32_t tab_count = 0);
        bool        SerializeXMLToFile(const char *filename);
        inline bool SerializeXMLToFile(const std::string &filename)
                    {return SerializeXMLToFile(filename.c_str());}
        inline bool DeserializeXML(const uint8_t *data, const ssize_t& size)
                    {return DeserializeXML(std::string((char*)data,size));}
        inline bool DeserializeXML(const char *data)
                    {return DeserializeXML(std::string((char*)data));}
        bool        DeserializeXML(const std::string &data);
        bool        DeserializeXMLFromFile(const char *filename);
        inline bool DeserializeXMLFromFile(const std::string &filename)
                    {return DeserializeXMLFromFile(filename.c_str());}
        void        Clear();
        bool        Delete(const char *key_name);
        inline bool Delete(const std::string &key_name)
                    {return Delete(key_name.c_str());}

        Variant&    operator=(const Variant         &val);
        Variant&    operator=(const uint8_t         &val);
        Variant&    operator=(const uint16_t        &val);
        Variant&    operator=(const uint32_t        &val);
        Variant&    operator=(const uint64_t        &val);
        Variant&    operator=(const int8_t          &val);
        Variant&    operator=(const int16_t         &val);
        Variant&    operator=(const int32_t         &val);
        Variant&    operator=(const int64_t         &val);
        Variant&    operator=(const float           &val);
        Variant&    operator=(const double          &val);
        Variant&    operator=(const std::string     &val);
        Variant&    operator=(const char            *val);
        Variant&    operator=(const bool            &val);


        Variant&    operator[](const char *key);
        Variant&    operator[](const std::string &key);
        Variant&    operator[](const uint8_t         &val);
        Variant&    operator[](const uint16_t        &val);
        Variant&    operator[](const uint32_t        &val);
        Variant&    operator[](const uint64_t        &val);
        Variant&    operator[](const int8_t          &val);
        Variant&    operator[](const int16_t         &val);
        Variant&    operator[](const int32_t         &val);
        Variant&    operator[](const int64_t         &val);
        Variant&    operator[](const float           &val);
        Variant&    operator[](const double          &val);
        Variant&    operator[](const bool            &val);

        uint8_t             AsUInt8() const;
        uint16_t            AsUInt16()const;
        uint32_t            AsUInt32()const;
        uint64_t            AsUInt64()const;
        int8_t              AsInt8()  const;
        int16_t             AsInt16() const;
        int32_t             AsInt32() const;
        int64_t             AsInt64() const;
        float               AsFloat() const;
        double              AsDouble()const;
        const std::string&  AsString()const;
        const char*         AsChar()  const;
        bool                AsBool()  const;
        void                AsData(uint8_t **data, ssize_t *size)const;

        inline const std::string&   GetName()  const
                                        {return _name;}
        inline void                 SetName(const std::string &name)
                                        {_name.assign(name);}
        inline void                 SetName(const char *name)
                                        {_name.assign(name);}
		inline const VariantType&	GetType()
										{return _type;}
		bool						SetType(const VariantType &type);
    private:
        VariantType         _type;
        std::string         _name;
        std::string         _val;
        VariantMap          _map;

        bool ParseXML(const std::string &data,ssize_t *out_pos);
        bool SaveToFile(const char *filename, const std::string &data);
        bool LoadFromFile(const char *filename, std::string *data);
    };

}

#endif /* defined(__CoreObject__Variant__) */
