//
//  VariantGetters.inl
//  CoreObject
//
//  Created by Oleg on 20.06.13.
//
//


#include "Variant.h"

using namespace CoreObjectLib;
using namespace std;

uint8_t Variant::AsUInt8() const
{
    uint8_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

uint16_t Variant::AsUInt16() const
{
    uint16_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

uint32_t Variant::AsUInt32() const
{
    uint32_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

uint64_t Variant::AsUInt64() const
{
    uint64_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

int8_t Variant::AsInt8() const
{
    int8_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

int16_t Variant::AsInt16() const
{
    int16_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

int32_t Variant::AsInt32() const
{
    int32_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

int64_t Variant::AsInt64() const
{
    int64_t val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

float Variant::AsFloat() const
{
    float val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

double Variant::AsDouble() const
{
    double val;
    stringstream ss(_val);
    
    ss >> val;
    return val;
}

bool  Variant::AsBool() const
{
    bool val;
    stringstream ss(_val);
    
    ss >>boolalpha>> val;
    return val;
}

const std::string& Variant::AsString() const
{
    return _val;
}

const char* Variant::AsChar() const
{
    return _val.c_str();
}

void Variant::AsData(uint8_t **data, ssize_t *size) const
{
    *data = (uint8_t*)_val.c_str();
    *size = _val.size();
}