//
//  VariantOperaators.inl
//  CoreObject
//
//  Created by Oleg on 20.06.13.
//
//

#include "Variant.h"

using namespace CoreObjectLib;
using namespace std;

Variant&    Variant::operator=(const Variant         &val)
{
    _val    = val._val;
    _type   = val._type;
    _map    = val._map;

    return *this;
}

Variant&    Variant::operator=(const uint8_t         &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::UnsignedInt8;
    return *this;
}

Variant&    Variant::operator=(const uint16_t        &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::UnsignedInt16;
    return *this;
}

Variant&    Variant::operator=(const uint32_t        &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::UnsignedInt32;
    return *this;
}

Variant&    Variant::operator=(const uint64_t        &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::UnsignedInt64;
    return *this;
}

Variant&    Variant::operator=(const int8_t          &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::SignedInt8;
    return *this;
}

Variant&    Variant::operator=(const int16_t         &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::SignedInt16;
    return *this;
}

Variant&    Variant::operator=(const int32_t         &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::SignedInt32;
    return *this;
}

Variant&    Variant::operator=(const int64_t         &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::SignedInt64;
    return *this;
}

Variant&    Variant::operator=(const float           &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::Float;
    return *this;
}

Variant&    Variant::operator=(const double          &val)
{
    stringstream ss;

    ss << val;
    _val.assign(ss.str());
	_type = VariantType::Double;
    return *this;
}

Variant&    Variant::operator=(const string     &val)
{
    _val.assign(val);
	_type = VariantType::String;
    return *this;
}

Variant&    Variant::operator=(const char     *val)
{
    _val.assign(val);
	_type = VariantType::String;
    return *this;
}

Variant&    Variant::operator=(const bool &val)
{
    stringstream ss;

    ss <<boolalpha<<val;
    _val.assign(ss.str());
	_type = VariantType::Bool;
    return *this;
}

Variant&    Variant::operator[](const char *key)
{
    string str_key(key);
    return operator[](str_key);
}

Variant&    Variant::operator[](const string &str_key)
{
    VariantMapIterator it;

    _type = VariantType::Variant;
    it = _map.find(str_key);
    if (it == _map.end())
    {
        Variant *new_var = new Variant(str_key);


        _map.insert(make_pair(str_key,new_var));
        return *new_var;
    }
    else
        return *((*it).second);
}

#define VAR_OPERATOR_KEY(type) \
Variant&    Variant::operator[](const type& key) \
{ \
    stringstream ss; \
    ss<<key; \
    return operator[](ss.str()); \
}

VAR_OPERATOR_KEY(uint8_t);
VAR_OPERATOR_KEY(uint16_t);
VAR_OPERATOR_KEY(uint32_t);
VAR_OPERATOR_KEY(uint64_t);
VAR_OPERATOR_KEY(int8_t);
VAR_OPERATOR_KEY(int16_t);
VAR_OPERATOR_KEY(int32_t);
VAR_OPERATOR_KEY(int64_t);
VAR_OPERATOR_KEY(float);
VAR_OPERATOR_KEY(double);
VAR_OPERATOR_KEY(bool);

