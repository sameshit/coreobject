#ifndef STRINGCONVERT__H
#define STRINGCONVERT__H

#include "../../common/config.h"

namespace CoreObjectLib
{
	class StringConvert
	{
	public:
		static std::wstring Utf8ToUtf16(const std::string &str);
		static std::string  Utf16ToUtf8(const std::wstring &str);
	};
}

#endif