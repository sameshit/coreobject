#include "../StringConvert.h"

using namespace CoreObjectLib;
using namespace std;

wstring StringConvert::Utf8ToUtf16(const string &str)
{
	int size_needed;
	wstring ret_str;

	size_needed = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), (int)str.size(), NULL, 0);
	ret_str.resize(size_needed);
	MultiByteToWideChar              (CP_UTF8, 0, str.c_str(), (int)str.size(), (wchar_t*)ret_str.c_str(), size_needed);

    return ret_str;
}

string StringConvert::Utf16ToUtf8(const wstring &str)
{
	int size_needed;
	string ret_str;
	
	size_needed = WideCharToMultiByte(CP_UTF8, 0, str.c_str(), (int)str.size(), NULL, 0, NULL, NULL);
	ret_str.resize(size_needed);
	WideCharToMultiByte              (CP_UTF8, 0, str.c_str(), (int)str.size(), (char *)ret_str.c_str(), size_needed, NULL, NULL);
 
	return ret_str;
}