#ifndef COREOBJECT__H
#define COREOBJECT__H

#include "common/config.h"
#include "core/memory/Memory.h"
#include "core/logger/Logger.h"
#include "core/event/Event.h"
#include "core/thread/ThreadCache.h"
#include "core/loop/Loop.h"
#include "utils/misc/Utils.h"
#include "core/time/COTime.h"
#include "core/containers/SharedQueue.h"
#include "utils/atest/ATest.h"
#include "utils/variant/Variant.h"
#include "utils/crashreport/CrashReport.h"
#include "utils/stringconvert/StringConvert.h"
#ifdef OS_WIN
#include "utils/win/WinErrorUtils.h"
#endif

namespace CoreObjectLib
{

class LIBEXPORT CoreObject
{
public:
	CoreObject(bool disable_memory_logging = false);
    CoreObject(const char* logger_filename,bool disable_memory_logging = false);
	virtual ~CoreObject();
	inline Memory* 		GetMemory() 	{return _memory;}
	inline Loop* 		GetLoop()	{return _loop;}
	inline ThreadCache* GetThreadCache() {return _thread_cache;}
	inline Logger*      GetLogger()	{return _logger;}
	inline Thread*		GetThread()	{return _thread_cache->GetThread();}
    inline Scheduler*   GetScheduler() {return _loop->_scheduler;}
	inline void			ReleaseThread(Thread **pthread) {_thread_cache->ReleaseThread(pthread);}
    inline bool         IsLoopThread() {return _loop->IsLoopThread();}
private:
	Loop 				*_loop;
	Memory              *_memory;
	Logger              *_logger;
	ThreadCache			*_thread_cache;
};

}

#endif
