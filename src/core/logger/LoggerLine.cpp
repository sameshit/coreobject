#include "LoggerLine.h"

using namespace CoreObjectLib;
using namespace std;

#if defined(DEBUG_MODE_ON)
LoggerLine::LoggerLine(Logger *logger, const char *filename,
					const char *function, int line,
					const LoggerLevel &level)
:_logger(logger),_level(level)
{
	const char *sk_file;
    ssize_t len;

	_info = new LoggerLineInfo;
	
	LogLevel();
	LogTime();
    len = strlen(filename);
    if (len > 20)
        sk_file = filename + (len - 20);
    else
        sk_file = filename;
    
	_info->ss <<", file: "<< sk_file << ", func: " << function << ", line: " << line;
	_info->ss <<"]"<< endl;
}
#else
LoggerLine::LoggerLine(Logger *logger, const LoggerLevel& level)
:_logger(logger),_level(level)
{
	_info = new LoggerLineInfo;

	LogLevel();
	LogTime();
	_info->ss <<"]"<< endl;
}
#endif

LoggerLine::LoggerLine(const LoggerLine &logger_line)
:_info(logger_line._info),_logger(logger_line._logger),_level(logger_line._level)
{
	++_info->ref;
}

LoggerLine::~LoggerLine()
{
	--_info->ref;
	if (_info->ref == 0)
	{
		if (_logger->GetLoggerLevel() >= _level)
		{
			_info->ss << endl<<endl;
			_logger->Write(_info->ss.str());
		}
		delete _info;
	}
}

void LoggerLine::LogLevel()
{
	_info->ss << "[";
	switch (_level)
	{
	case LoggerLevel::Verbose:
		_info->ss <<"Verbose";
	break;
	case LoggerLevel::Error:
		_info->ss <<"Error";
	break;
	case LoggerLevel::Info:
		_info->ss <<"Info";
	break;
	case LoggerLevel::None:
		_info->ss <<"None";
	break;
	case LoggerLevel::Warn:
		_info->ss << "Warn";
	break;
	default:
	break;
	}
	_info->ss <<", thread id: "<<COErr::GetNumericThreadId();
}

void LoggerLine::LogTime()
{
	char prefix_format[20] = "";
	time_t now;
  
    time(&now);

#ifdef OS_WIN
	struct tm win_timeinfo;
	if(localtime_s(&win_timeinfo,&now) != 0)
		_info->ss << ", time: TIME ERROR:";
	else
	{
		strftime(prefix_format,20,"%I:%M:%S",&win_timeinfo);
		_info->ss <<", time: "<< prefix_format;
	}
#else
	struct tm* timeinfo;
    timeinfo = localtime(&now);
	strftime(prefix_format,20,"%I:%M:%S",timeinfo);
	_info->ss <<", time: " << prefix_format;
#endif 
}

