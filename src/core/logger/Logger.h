#ifndef LOGGEREVENTHANDLER_H
#define LOGGEREVENTHANDLER_H

#include "../../common/config.h"
#include "../thread/SpinLock.h"
#include "../thread/Semaphore.h"
#include "../thread/Thread.h"

namespace CoreObjectLib
{
	enum class LoggerLevel
	{
		None = 1,
		Error,
		Warn,
		Info,
		Verbose,
	};

	class LIBEXPORT Logger
	{
	public:
		Logger();
		Logger(const char* filename);	
		virtual ~Logger();
		inline void SetLoggerLevel(const LoggerLevel &level) {_level = level;}
		inline const LoggerLevel& GetLoggerLevel() {return _level;}

		bool Write(const std::string &str);
		inline bool Write(const char *str) {return Write(std::string(str));}
		bool Flush();
	private:
		LoggerLevel					_level;
		bool						_exit,_flush;
		Thread						*_write_thread;
		Semaphore					_sem,_flush_sem;
		SpinLock					_write_lock;
		std::queue<std::string>		_write_q;
		bool 						_isfile;
		std::ostream 				*out_stream;

		void ProcessWrite();
	};
}

#endif
