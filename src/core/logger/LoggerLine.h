#ifndef LOGGERLINE__H
#define LOGGERLINE__H

#include "Logger.h"

#ifdef DEBUG_MODE_ON
	#define SET_LOGGER_LINE(text,level) \
		CoreObjectLib::LoggerLine(_core->GetLogger(),__FILE__,__FUNCTION__,__LINE__,level) << text

	#define SET_LONG_LOGGER_LINE(logger,text,level) \
		CoreObjectLib::LoggerLine(logger,__FILE__,__FUNCTION__,__LINE__,level)<< text

	#define _SET_LOGGER_LINE(text,level) CoreObjectLib::LoggerLine(_logger,__FILE__,__FUNCTION__,__LINE__,level)<< text
#else
	#define SET_LOGGER_LINE(text,level)  \
		CoreObjectLib::LoggerLine(_core->GetLogger(),level)<< text

	#define SET_LONG_LOGGER_LINE(logger,text,level) \
		CoreObjectLib::LoggerLine(logger,level)<< text

	#define _SET_LOGGER_LINE(text,level) CoreObjectLib::LoggerLine(_logger,level)<< text
#endif

	#define LOG_ERROR(text) SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Error)
	#define LOG_WARN(text) SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Warn)
	#define LOG_INFO(text) SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Info)
	#define LOG_VERBOSE(text) SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Verbose)
	#define LOG_FLUSH() _core->GetLogger()->Flush()

	#define LONG_LOG_ERROR(logger,text) SET_LONG_LOGGER_LINE(logger,text,CoreObjectLib::LoggerLevel::Error)
	#define LONG_LOG_WARN(logger,text) SET_LONG_LOGGER_LINE(logger,text,CoreObjectLib::LoggerLevel::Warn)
	#define LONG_LOG_INFO(logger,text) SET_LONG_LOGGER_LINE(logger,text,CoreObjectLib::LoggerLevel::Info)
	#define LONG_LOG_VERBOSE(logger,text) SET_LONG_LOGGER_LINE(logger,text,CoreObjectLib::LoggerLevel::Verbose)
	#define LONG_LOG_FLUSH(logger) logger->Flush()

	#define _LOG_ERROR(text) _SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Error)
	#define _LOG_WARN(text) _SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Warn)
	#define _LOG_INFO(text) _SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Info)
	#define _LOG_VERBOSE(text) _SET_LOGGER_LINE(text,CoreObjectLib::LoggerLevel::Verbose)
	#define _LOG_FLUSH() _logger->Flush()

namespace CoreObjectLib
{
	class LIBEXPORT LoggerLine
	{
	public:
#if defined(DEBUG_MODE_ON)
		LoggerLine	(Logger *logger, const char *filename,
					const char *function, int line,
					const LoggerLevel &level=LoggerLevel::Error);
#else
		LoggerLine(Logger *logger,const LoggerLevel& level=LoggerLevel::Error);
#endif		
		LoggerLine(const LoggerLine& logger_line);
		
		virtual ~LoggerLine();

		template<typename T>
		LoggerLine& operator << (const T& val)
		{
			_info->ss << val; 
			return *this;
		}
		typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
		typedef CoutType& (*StandardEndLine)(CoutType&);

		LoggerLine& operator <<(StandardEndLine endl)
		{
			endl(_info->ss);
			return *this;
		}
	private:
		class LoggerLineInfo
		{
		public:
			LoggerLineInfo():ref(1) {}
			virtual ~LoggerLineInfo () {}

			int ref;
			std::stringstream ss;
		} *_info;

		void LogTime();
		void LogLevel();

		Logger *_logger;
		LoggerLevel _level;
	};
}

#endif