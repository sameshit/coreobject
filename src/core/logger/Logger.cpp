#include "Logger.h"
using namespace std;
using namespace CoreObjectLib;
Logger::Logger()
	:_isfile(false),_exit(false),_flush(false),_level(LoggerLevel::Verbose)
{
	out_stream = &cout;
	_write_thread = new Thread();
	_write_thread->OnThread.Attach(this,&Logger::ProcessWrite);
	_write_thread->Wake();
}

Logger::Logger(const char *filename)
:_isfile(true),_exit(false),_flush(false),_level(LoggerLevel::Verbose)
{
	out_stream = new ofstream(filename);
	_write_thread = new Thread;
	_write_thread->OnThread.Attach(this,&Logger::ProcessWrite);
	_write_thread->Wake();
}

Logger::~Logger()
{
	_write_lock.Lock();
	_exit = true;
	_write_lock.UnLock();

	_sem.Post();

	delete _write_thread;
	if (_isfile)
		delete out_stream;
}

bool Logger::Write(const string& str)
{
	_write_lock.Lock();
	_write_q.push(str);
	_write_lock.UnLock();
	return _sem.Post();
}

bool Logger::Flush()
{
	_write_lock.Lock();
	_flush = true;
	_write_lock.UnLock();
	
	RETURN_IF_FALSE(_sem.Post());
	return _flush_sem.Wait();
}

void Logger::ProcessWrite()
{
	string str;
	bool empty,exit,flush;

	_write_lock.Lock();
	exit = _exit;
	flush = _flush;
	if (flush)
		_flush = false;
	_write_lock.UnLock();

	if (exit)
		return;

	_sem.Wait();

	do
	{
		_write_lock.Lock();
		empty = _write_q.empty();
		if (!empty)
		{
			str = _write_q.front();
			_write_q.pop();
		}
		_write_lock.UnLock();
		if (!empty)
			*out_stream<<str;
	}
	while(!empty);

	if (flush)
		_flush_sem.Post();
}