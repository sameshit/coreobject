//
//  ScopedLock.h
//  CoreObject
//
//  Created by Oleg on 01.04.13.
//
//

#ifndef CoreObject_ScopedLock_h
#define CoreObject_ScopedLock_h

#include "SpinLock.h"
#include "Mutex.h"

namespace CoreObjectLib
{
    template<class Mutex>
    class ScopedLock
    {
    public:
        typedef Mutex MutexType;
        
        ScopedLock(MutexType &mutex)
        :_mutex(&mutex)
        {
            _mutex->Lock();
        }
        ScopedLock(MutexType *mutex)
        :_mutex(mutex)
        {
            _mutex->Lock();
        }
        
        virtual ~ScopedLock()
        {
            _mutex->UnLock();
        }
    private:
        MutexType *_mutex;
    };
    
    typedef ScopedLock<SpinLock>    ScopedSpin;
    typedef ScopedLock<Mutex>       ScopedMutex;
}

#endif
