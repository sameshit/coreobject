#ifndef SPINLOCK___H
#define SPINLOCK___H

#include "../../common/config.h"

namespace CoreObjectLib
{

class LIBEXPORT SpinLock
    {
    public:
        SpinLock();
        virtual ~SpinLock();

        void Lock();
        void UnLock();
    private:
#if defined(OS_X)
        OSSpinLock _lock;
#elif defined(OS_LINUX)
        pthread_spinlock_t _lock;
#elif defined(OS_WIN)
		CRITICAL_SECTION _lock;
#endif
    };

}
#endif
