#include "Thread.h"

int success_status = 0;

using namespace CoreObjectLib;

Thread::Thread()
:_exit(false),_sleeping(true)
{
	_mutex = new Mutex();
	_wake_sem = new Semaphore(0);
	_sleep_sem = new Semaphore(0);



#ifdef OS_WIN
	_thread_handle = CreateThread(
			NULL,							 // default security attributes
            0,								 // use default stack size
			Thread::OnSystemThread,			 // thread function name
            (LPVOID)this,					 // argument to thread function
            0,								 // use default creation flags
            &_thread_id);					 // returns the thread identifier

	assert( _thread_handle != NULL );
#else
	int ret;
   	ret = pthread_create(&_thread_id,NULL,Thread::OnSystemThread,(void*)this);
	assert( ret == 0);
#endif

	_sleep_sem->Wait();
}

Thread::~Thread()
{
	_mutex->Lock();
	_exit = true;
	_mutex->UnLock();

	if (_sleeping)
		Wake();

#ifdef OS_WIN
	if( WaitForSingleObject(_thread_handle,INFINITE) == WAIT_FAILED)
		abort();
#else
	int *status;
	int ret;
	ret = pthread_join(_thread_id,(void**)&status);
#endif

	delete _sleep_sem;
	delete _wake_sem;
	delete _mutex;
}

bool Thread::Wake()
{
	_mutex->Lock();
	if (!_sleeping)
	{
		_mutex->UnLock();
		COErr::Set("Thread is not sleeping!");
		return false;
	}
	_sleeping = false;
	_mutex->UnLock();

	if(!_wake_sem->Post())
		return false;

	return true;
}

bool Thread::Sleep()
{
	_mutex->Lock();
	if (_sleeping)
	{
		_mutex->UnLock();
		COErr::Set("Thread is already sleeping!");
		return false;
	}
	_sleeping = true;

	_mutex->UnLock();

	if (!(COErr::GetThreadId() == _my_thread_id))
	if (!_sleep_sem->Wait())
		return false;

	return true;
}

bool Thread::IsSleeping()
{
	bool ret_val;
	_mutex->Lock();
	ret_val = _sleeping;
	_mutex->UnLock();
	return ret_val;
}

void Thread::Exit()
{
    _mutex->Lock();
    _exit = true;
    _mutex->UnLock();
}


#ifdef OS_WIN
DWORD WINAPI Thread::OnSystemThread(LPVOID data)
#else
void* Thread::OnSystemThread(void *data)
#endif
{
	Thread *thread = (Thread *)data;
	bool exit,sleeping;

    COErr::Init();

    thread->_my_thread_id = COErr::GetThreadId();
	while(true)
	{
		thread->_mutex->Lock();
		exit = thread->_exit;
		sleeping = thread->_sleeping;
		thread->_mutex->UnLock();

		if (exit)
#ifdef OS_WIN
			ExitThread(0);
#else
			pthread_exit((void*)success_status);
#endif
		else if (sleeping)
		{
			thread->_sleep_sem->Post();
			thread->_wake_sem->Wait();
		}
		else
			thread->OnThread();
	}
    COErr::Free();
}
