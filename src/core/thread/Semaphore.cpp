#include "Semaphore.h"
#include "../error/COErr.h"

using namespace CoreObjectLib;
using namespace std;

Semaphore::Semaphore(uint32_t initial_value)
{
#if defined(OS_WIN)
    _count = (LONG)initial_value;
	_sem = CreateSemaphore(NULL,_count,LONG_MAX,NULL);
	assert(_sem != NULL);
#elif defined (OS_LINUX)
    sem_init(&_sem, 0, initial_value);
#elif defined (OS_X)
    kern_return_t ret = semaphore_create(mach_task_self(),&_sem, SYNC_POLICY_FIFO, initial_value);
    assert(ret == KERN_SUCCESS);
#endif
}

Semaphore::~Semaphore()
{
#if defined (OS_WIN)
	BOOL res = CloseHandle(_sem);
	assert(res == TRUE);
#elif defined(OS_LINUX)
    sem_destroy(&_sem);
#elif defined (OS_X)
    kern_return_t ret = semaphore_destroy(mach_task_self(), _sem);
    assert(ret == KERN_SUCCESS);
#endif
}

bool Semaphore::Wait(uint32_t milliseconds)
{
#if defined (OS_WIN)
	switch (WaitForSingleObject(_sem,(DWORD)milliseconds))
	{
	case WAIT_OBJECT_0:
		InterlockedDecrement(&_count);
		COErr::SetCode(NOERR);
		return true;
	break;
	case WAIT_TIMEOUT:
		COErr::Set("Timeout");
		COErr::SetCode(ERR_TIMEOUT);
		return false;
	break;
	default:
		COErr::Set("Wait error");
		COErr::SetCode(ERR_WAIT);
		return false;
	break;
	}
#elif defined (OS_LINUX)
    struct timespec ts;
    ts.tv_sec  = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;

    if(sem_timedwait(&_sem,&ts) == 0)
    {
        COErr::Set("");
        COErr::SetCode(ERR_TIMEOUT);
        return true;
    }

    if(errno == ETIMEDOUT)
    {
        COErr::Set("Timeout");
        COErr::SetCode(ERR_TIMEOUT);
    }
    else
    {
        COErr::Set("Wait error");
        COErr::SetCode(ERR_WAIT);
    }
    return false;
#elif defined (OS_X)
    mach_timespec_t ts;
    ts.tv_sec  = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;

    switch (semaphore_timedwait(_sem, ts))
    {
        case KERN_SUCCESS:
            COErr::SetCode(NOERR);
            return true;
        break;
        case KERN_OPERATION_TIMED_OUT:
            COErr::Set("Timeout");
            COErr::SetCode(ERR_TIMEOUT);
            return false;
        break;
        default:
            COErr::Set("Wait error");
            COErr::SetCode(ERR_WAIT);
            return false;
        break;
    }
#endif
}


bool Semaphore::Wait()
{
#if defined (OS_WIN)
	return Wait(INFINITE);
#elif defined (OS_LINUX)
    if(sem_wait(&_sem) == -1)
    {
        COErr::Set() << "sem_wait() failed with code: "<<errno;
        COErr::SetCode(ERR_WAIT);
        return false;
    }

    return true;
#elif defined (OS_X)
    kern_return_t ret;
    stringstream str;
    do
    {
        ret = semaphore_wait(_sem);
    }
    while (ret == KERN_ABORTED);

    if (ret != KERN_SUCCESS)
    {
        str << "semaphore_wait() error. Code: "<<ret;
        COErr::Set(str.str().c_str());
        COErr::SetCode(ERR_WAIT);
        return false;
    }
    return true;
#endif
}


bool Semaphore::Post()
{
#if defined (OS_WIN)
	InterlockedIncrement(&_count);
	RETURN_MSG_AND_CODE_IF_TRUE(ReleaseSemaphore(_sem,1,NULL) != TRUE,"Post() failed",GetLastError());
	return true;
#elif defined (OS_LINUX)
    RETURN_MSG_AND_CODE_IF_TRUE(sem_post(&_sem) == -1,"Post() failed",errno);
    return true;
#elif defined (OS_X)
    kern_return_t ret = semaphore_signal(_sem);
    RETURN_MSG_AND_CODE_IF_TRUE(ret != KERN_SUCCESS,"Post() failed",ret);

    return true;
#endif
}
