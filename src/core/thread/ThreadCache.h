#ifndef THREADCACHE__H
#define THREADCACHE__H

#include "Thread.h"
#include "../memory/Memory.h"

namespace CoreObjectLib
{

	class ThreadCache
	{
	public:
		ThreadCache(Memory *memory);
		virtual ~ThreadCache();

		Thread* GetThread();
		void	ReleaseThread(Thread **pthread);
	private:
		std::unordered_set<Thread *> _sleeping_threads;
		Memory *_memory;
		SpinLock _lock;
	};

}

#endif