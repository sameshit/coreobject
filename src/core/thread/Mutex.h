#ifndef MUTEX___H
#define MUTEX___H

#include "../../common/config.h"

namespace CoreObjectLib
{

class LIBEXPORT Mutex
{
	public:
		Mutex();
		~Mutex();
		void Lock();
		void UnLock();
	private:
#ifdef OS_WIN
		CRITICAL_SECTION _mutex;
#else
		pthread_mutex_t _mutex;
#endif

};

}
#endif
