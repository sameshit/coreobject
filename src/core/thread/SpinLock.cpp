#include "SpinLock.h"

using namespace CoreObjectLib;

SpinLock::SpinLock()
#ifdef OS_X
:_lock(OS_SPINLOCK_INIT)
#endif
{
#if defined(OS_LINUX)
    int ret;
    ret = pthread_spin_init(&_lock,0);
    assert(ret == 0);
#elif defined(OS_WIN)
	InitializeCriticalSection(&_lock);
#endif
}

SpinLock::~SpinLock()
{
#if defined(OS_LINUX)
    int ret;
    ret = pthread_spin_destroy(&_lock);
    assert(ret == 0);
#elif defined(OS_WIN)
	DeleteCriticalSection(&_lock);
#endif
}

void SpinLock::Lock()
{
#if defined(OS_X)
    OSSpinLockLock(&_lock);
#elif defined(OS_LINUX)
    pthread_spin_lock(&_lock);
#elif defined(OS_WIN)
	EnterCriticalSection(&_lock);
#endif
}

void SpinLock::UnLock()
{
#if defined(OS_X)
    OSSpinLockUnlock(&_lock);
#elif defined(OS_LINUX)
    pthread_spin_unlock(&_lock);
#elif defined(OS_WIN)
	LeaveCriticalSection(&_lock);
#endif
}
