#ifndef THREAD___H
#define THREAD___H

#include "../../common/config.h"
#include "../event/Event.h"
#include "Mutex.h"
#include "Semaphore.h"
#include "../error/COErr.h"
#include "ScopedLock.h"

namespace CoreObjectLib
{
class LIBEXPORT Thread
{
	public:
		bool Wake();
		bool Sleep();
		bool IsSleeping();
        void Exit();
        inline const ThreadId& GetThreadId() {return _my_thread_id;}

		Event<> OnThread;
	private:
		Thread();
		~Thread();

        ThreadId _my_thread_id;
		friend class ThreadCache;
        friend class Memory;
		friend class Logger;

#ifdef OS_WIN
		static DWORD WINAPI	OnSystemThread(LPVOID data);
#else
		static void* 	OnSystemThread(void *data);
#endif
		Mutex 			*_mutex;
		Semaphore		*_wake_sem,*_sleep_sem;
		bool			_exit,_sleeping;

#ifdef OS_WIN
		HANDLE _thread_handle;
		DWORD  _thread_id;
#else
		pthread_t 		_thread_id;
#endif
};

}

#endif
