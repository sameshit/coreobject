#include "Mutex.h"

using namespace CoreObjectLib;

Mutex::Mutex()
{
#ifdef OS_WIN
	InitializeCriticalSection(&_mutex);
#else
	int ret;
	ret = pthread_mutex_init(&_mutex,NULL);
	assert(ret == 0);
#endif
}

Mutex::~Mutex()
{
#ifdef OS_WIN
	DeleteCriticalSection(&_mutex);
#else
	int ret;
	ret = pthread_mutex_destroy(&_mutex);
	assert(ret == 0);
#endif
}

void Mutex::Lock()
{
#ifdef OS_WIN
	EnterCriticalSection(&_mutex);
#else
	pthread_mutex_lock(&_mutex);
#endif
}

void Mutex::UnLock()
{
#ifdef OS_WIN
	LeaveCriticalSection(&_mutex);
#else
	pthread_mutex_unlock(&_mutex);
#endif
}
