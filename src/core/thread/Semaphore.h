#ifndef SEMAPHORE__H
#define SEMAPHORE__H

#include "../../common/config.h"

namespace CoreObjectLib
{
	enum SemaphoreError
	{
		NOERR = 0,
		ERR_WAIT = 1,
		ERR_TIMEOUT = 2,
	};

class LIBEXPORT Semaphore
{
public:
	Semaphore(uint32_t initial_value = 0);
	virtual ~Semaphore();

	bool Wait();
	bool Wait(uint32_t milliseconds);
	bool Post();

private:
#if defined(OS_WIN)
	HANDLE _sem;
	LONG   _count;
#elif defined (OS_LINUX)
    sem_t _sem;
#elif defined (OS_X)
    semaphore_t _sem;
#endif
};

}

#endif
