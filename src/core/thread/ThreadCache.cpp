#include "ThreadCache.h"

using namespace CoreObjectLib;
using namespace std;

ThreadCache::ThreadCache(Memory* memory)
:_memory(memory)
{
	
}

ThreadCache::~ThreadCache()
{
	unordered_set<Thread*>::iterator it;
    Thread *thread;

	for (it = _sleeping_threads.begin(); it != _sleeping_threads.end(); it ++)
    {
        thread = (*it);
		long_delete(_memory,thread);
    }

	_sleeping_threads.clear();
}

Thread* ThreadCache::GetThread()
{
	Thread *thread;
	unordered_set<Thread*>::iterator it;

	_lock.Lock();
	if (_sleeping_threads.size() == 0)
		long_new(_memory,thread);
	else
	{
		it = _sleeping_threads.begin();
		thread = (*it);
		_sleeping_threads.erase(it);
	}
	_lock.UnLock();

	return thread;
}

void ThreadCache::ReleaseThread(Thread **pthread)
{
	Thread *thread = *pthread;

	if(!thread->IsSleeping())
		thread->Sleep();

	thread->OnThread.Reset();

	_lock.Lock();
	_sleeping_threads.insert(thread);
	_lock.UnLock();

	*pthread = NULL;
}