#ifndef TCPSESSION_H
#define TCPSESSION_H

#include "../../socket/BaseSocket.h"

namespace CoreObjectLib
{
	#if defined (OS_WIN)
		static GUID _ex_GuidGetAcceptExSockAddrs	= WSAID_GETACCEPTEXSOCKADDRS;
		static GUID _ex_GuidDisconnectEx			= WSAID_DISCONNECTEX;
	#endif
	
	class TcpSession : public BaseSocket
	{
	protected:
	    bool						_connected;
		#ifdef OS_WIN
			LPFN_DISCONNECTEX			_ex_lpfnDisconnectEx;
		#endif
	private:
    	#if defined (OS_WIN)
			LPFN_GETACCEPTEXSOCKADDRS	_ex_lpfnGetAcceptExSockAddrs;
			char						_ex_buffer [(sizeof(SOCKADDR_IN) + 16) * 2];
		#endif				
		#if defined (OS_LINUX) || defined (OS_X) || defined (OS_IOS)
		    void *tcpServer;
		#endif	
		
		friend class Loop;
		friend class TcpServerIO;
	public:
		TcpSession(Memory *memory, Logger *logger, IO_TYPE io_type = IO_TCP_SESSION);
		TcpSession(Memory *memory, Logger *logger, int sock, struct sockaddr_in addr, socklen_t len, IO_TYPE io_type = IO_TCP_SESSION);
		virtual ~TcpSession();
	};
}

#endif //TCPSESSIONIO_H
