#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "TcpServerIO.h"
#include "../../../CoreObject.h"

namespace CoreObjectLib
{
	class TcpServer : public TcpServerIO
	{
		bool _started;
    protected:
		CoreObject *_core;
	public:
		TcpServer(CoreObject *core);
		virtual ~TcpServer();

		bool Start(uint16_t port);
	};
}

#endif //TCPSERVER_H
