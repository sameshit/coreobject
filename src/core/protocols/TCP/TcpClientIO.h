#ifndef TCPCLIENTIO_H
#define TCPCLIENTIO_H

#include "TcpSession.h"

namespace CoreObjectLib
{
	#if defined (OS_WIN)
		static GUID _ex_GuidConnectEx				= WSAID_CONNECTEX;
	#endif

	class TcpClientIO : public TcpSession
	{
	protected:
#ifdef OS_WIN
		bool Bind(uint16_t port = 0);
#endif
        bool ConnectIO(const char *host, uint16_t port);
	private:
   		#if defined (OS_WIN)
			LPFN_CONNECTEX		_ex_lpfnConnectEx;
		#endif
	public:
		TcpClientIO(Memory *memory, Logger *logger);
		virtual ~TcpClientIO();

        bool Send(uint8_t *data, ssize_t size);

		Event<> OnConnect;
		Event<> OnDisconnect;
		Event<uint8_t *, ssize_t &> OnRecv;

		bool Disconnect();
	};
}

#endif //TCPCLIENTIO_H
