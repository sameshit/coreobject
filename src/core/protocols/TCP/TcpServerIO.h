#ifndef TCPSERVERIO_H
#define TCPSERVERIO_H

#include "TcpClientIO.h"
#include "../../thread/Mutex.h"

#define LISTEN_COUNT_DEFAULT_VALUE 1024

namespace CoreObjectLib
{
	#if defined (OS_WIN)
		static GUID _ex_GuidAcceptEx			= WSAID_ACCEPTEX;
	#endif

	class TcpServerIO : public BaseSocket
	{
	private:
		#if defined (OS_WIN)
			OVERLAPPEDEX		 _ovlp_for_close;
			LPFN_ACCEPTEX		 _ex_lpfnAcceptEx;
		#endif

		std::set<TcpSession *>	 _sessions;
		friend class Loop;
	public:
		TcpServerIO(Memory *memory, Logger *logger);
		virtual ~TcpServerIO();

		bool Bind(uint16_t port);

		Event<TcpSession *> OnConnect;
		Event<TcpSession *> OnDisconnect;
		Event<TcpSession *, uint8_t *, ssize_t &> OnRecv;

		TcpSession   *GetLastSession();
        
		bool Send(TcpSession *session,uint8_t *data,ssize_t size);
	};
}

#endif //TCPSERVERIO_H
