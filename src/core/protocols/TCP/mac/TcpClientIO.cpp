#include "../TcpClientIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpClientIO::TcpClientIO(Memory *memory, Logger *logger)
:TcpSession(memory, logger, IO_TCP_CLIENT)
{
    _connected = false;
}

TcpClientIO::~TcpClientIO()
{
    if (_connected)
        assert(shutdown(_sock,SHUT_RDWR) == 0);
    
    _connected = false;
}

bool TcpClientIO::ConnectIO(const char *host, uint16_t port)
{
    struct hostent *server = gethostbyname(host);

    memcpy(&_addr.sin_addr,server->h_addr_list[0],server->h_length);
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(port);
    

	CHECK_RETURN2(connect(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr_in)) < 0 && errno != EINPROGRESS,"connect failed",errno);
	    
	return true;
}

bool TcpClientIO::Send(uint8_t *data, ssize_t size)
{
    ssize_t bytes_sent;
    
    bytes_sent = send(_sock, data, size, 0);
    
    CHECK_RETURN2(bytes_sent != size,"send() failed",errno);
    
    return true;
}