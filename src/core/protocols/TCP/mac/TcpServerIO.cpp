#include "../TcpServerIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpServerIO::TcpServerIO(Memory *memory, Logger *logger) : BaseSocket(memory, logger, IO_TCP_SERVER)
{

}

TcpServerIO::~TcpServerIO()
{
	std::set<TcpSession *>::iterator it;
    TcpSession *tcp_session;

	for(it = _sessions.begin(); it != _sessions.end(); it++)
    {
        tcp_session = (*it);
		long_delete(_memory, tcp_session);
    }
}

bool TcpServerIO::Bind(uint16_t port)
{
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = INADDR_ANY;

    CHECK_RETURN2(bind(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr)) < 0,"bind() failed",errno);
    
	CHECK_RETURN2(listen(_sock, LISTEN_COUNT_DEFAULT_VALUE) < 0,"listen() failed",errno);

    return true;
}

bool TcpServerIO::Send(TcpSession *session, uint8_t *data, ssize_t size)
{
    ssize_t bytes_sent;
    
    bytes_sent = send(session->_sock, data, size, 0);
    
    CHECK_RETURN2(bytes_sent != size,"send() failed",errno);
    
    return true;        
}