#include "../TcpClient.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpClient::TcpClient(CoreObject *core) :TcpClientIO(core->GetMemory(), core->GetLogger()),_core(core)
{
	
}

TcpClient::~TcpClient()
{
    if (_connected)
        _core->GetLoop()->Delete(this);
}

bool TcpClient::Connect(const char *host, uint16_t port)
{
    COND_RETURN(!ConnectIO(host, port));    
	COND_RETURN(!_core->GetLoop()->Add(this));
    
    return true;
}
