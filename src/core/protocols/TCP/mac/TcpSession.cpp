#include "../TcpSession.h"
using namespace CoreObjectLib;

TcpSession::TcpSession(Memory *memory, Logger *logger, IO_TYPE io_type)
:BaseSocket(memory, logger, io_type)
,_connected(true)
{

}

TcpSession::TcpSession(Memory *memory, Logger *logger, int sock, struct sockaddr_in addr, socklen_t len, IO_TYPE io_type) 
:BaseSocket(memory, logger, io_type,sock)
,_connected(true)
{

this->_addr = addr;
this->_len = len;

}

TcpSession::~TcpSession()
{
    if(_connected)
        assert(shutdown(_sock,SHUT_RDWR) == 0);
}

