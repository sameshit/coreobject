#include "TcpServer.h"
using namespace CoreObjectLib;

TcpServer::TcpServer(CoreObject *core) :TcpServerIO(core->GetMemory(), core->GetLogger()),_started(false),_core(core)
{
	
}

TcpServer::~TcpServer()
{
	if (_started)
		ASSERT_ERROR(_core->GetLoop()->Delete(this));
}

bool TcpServer::Start(uint16_t port)
{
	RETURN_IF_FALSE(TcpServerIO::Bind(port));
	RETURN_IF_FALSE(_core->GetLoop()->Add(this));

	_started = true;

	return true;
}