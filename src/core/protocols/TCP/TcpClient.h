#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "TcpClientIO.h"
#include "../../../CoreObject.h"

namespace CoreObjectLib
{
	class TcpClient : public TcpClientIO
	{
		bool _started;
    protected:
		CoreObject *_core;
	public:
		TcpClient(CoreObject *core);
		virtual ~TcpClient();

		bool Connect(const char *host, uint16_t port);
	};
}

#endif //TCPCLIENT_H
