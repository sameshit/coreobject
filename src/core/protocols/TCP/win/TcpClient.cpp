#include "../TcpClient.h"
using namespace CoreObjectLib;

TcpClient::TcpClient(CoreObject *core) : _core(core), TcpClientIO(core->GetMemory(), core->GetLogger()),_started(false)
{
	
}

TcpClient::~TcpClient()
{
	if (_started)
	{
		if (_connected)
			Disconnect();

		ASSERT_ERROR(_core->GetLoop()->Delete(this));
	}
}

bool TcpClient::Connect(const char *host, uint16_t port)
{
	RETURN_IF_FALSE(Bind());
	RETURN_IF_FALSE(_core->GetLoop()->Add(this));
	RETURN_IF_FALSE(TcpClientIO::ConnectIO(host, port));
	
	_started = true;
	
	return true;
}
