#include "../TcpClientIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;


TcpClientIO::TcpClientIO(Memory *memory, Logger *logger) : TcpSession(memory, logger, IO_TCP_CLIENT)
{
	DWORD dwBytes;

	if(WSAIoctl(_sock, SIO_GET_EXTENSION_FUNCTION_POINTER, 
		&_ex_GuidConnectEx, sizeof(GUID),
		&_ex_lpfnConnectEx, sizeof(LPFN_CONNECTEX), &dwBytes,
		NULL, NULL) == SOCKET_ERROR)
		_FATAL_ERROR2("Couldn't load Connectex",WSAGetLastError());
}

TcpClientIO::~TcpClientIO()
{

}

bool TcpClientIO::Disconnect()
{
	RETURN_MSG_IF_FALSE(_connected,"Not connected");
	RETURN_MSG_AND_CODE_IF_FALSE(_ex_lpfnDisconnectEx(_sock, 0, 0, 0),"DisconnectEx() failed",WSAGetLastError());

	return true;
}

bool TcpClientIO::Bind(uint16_t port)
{
	_addr.sin_family = AF_INET;
	uint8_t i = 0;
	
	if(port == 0)
		_addr.sin_port = htons(rand()%20000 + 10000);
	else
		_addr.sin_port = htons(port);

	_addr.sin_addr.S_un.S_addr = INADDR_ANY;

	while(bind(_sock, (sockaddr *)&_addr, sizeof(sockaddr_in)) != 0)
	{
		RETURN_MSG_AND_CODE_IF_TRUE (i < 10,"Couldnt' bind on provided port",WSAGetLastError()) ;
		++i;
		_addr.sin_port += 1;
	}
	
	return true;
}

bool TcpClientIO::ConnectIO(const char *host, uint16_t port)
{
	_addr.sin_port = htons(port);
	_addr.sin_addr.S_un.S_addr = inet_addr(host);

	if(!_ex_lpfnConnectEx(_sock, (const sockaddr *)&_addr, sizeof(sockaddr_in), 0, 0, NULL, &_wsaOverlapped))
		RETURN_MSG_AND_CODE_IF_TRUE(WSAGetLastError() != WSA_IO_PENDING,"WSAGetLastError is not io pending",WSAGetLastError());

	return true;
}

bool TcpClientIO::Send(uint8_t *data,ssize_t size)
{
	WSABUF buf;
	buf.buf = (char *)data;
	buf.len = size;

	RETURN_MSG_AND_CODE_IF_TRUE(WSASend(_sock, &buf, 1, &_dwBytes, _socketFlags, 0, 0) == 0,"WSASend() failed",WSAGetLastError());
	
	return true;
}