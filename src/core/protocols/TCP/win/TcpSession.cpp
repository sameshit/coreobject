#include "../TcpSession.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpSession::TcpSession(Memory *memory, Logger *logger, IO_TYPE io_type) : BaseSocket(memory, logger, io_type)
	,_connected(false)
{
	this->_wsaOverlapped.pData = (void *)this;

	DWORD dwBytes;

	if(WSAIoctl(_sock, SIO_GET_EXTENSION_FUNCTION_POINTER, 
		&_ex_GuidGetAcceptExSockAddrs, sizeof(GUID),
		&_ex_lpfnGetAcceptExSockAddrs, sizeof(LPFN_GETACCEPTEXSOCKADDRS), &dwBytes, 
		NULL, NULL) == SOCKET_ERROR)
		_FATAL_ERROR2("Couldn't load acceptex function",WSAGetLastError());

	if(WSAIoctl(_sock, SIO_GET_EXTENSION_FUNCTION_POINTER, 
		&_ex_GuidDisconnectEx, sizeof(GUID),
		&_ex_lpfnDisconnectEx, sizeof(LPFN_DISCONNECTEX), &dwBytes,
		NULL, NULL) == SOCKET_ERROR)
		_FATAL_ERROR2("Couldn't load disconnectex function",WSAGetLastError());
}

TcpSession::~TcpSession()
{

}