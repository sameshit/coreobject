#include "../TcpServerIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpServerIO::TcpServerIO(Memory *memory, Logger *logger) : BaseSocket(memory, logger, IO_TCP_SERVER)
{
	DWORD dwBytes;

	int err_code = WSAIoctl(_sock, SIO_GET_EXTENSION_FUNCTION_POINTER, 
	&_ex_GuidAcceptEx, sizeof(GUID),
	&_ex_lpfnAcceptEx, sizeof(LPFN_ACCEPTEX), &dwBytes,
	NULL, NULL);
	
	if(err_code == SOCKET_ERROR)
		_FATAL_ERROR2("Couldn't load AcceptEx",WSAGetLastError());
}

TcpServerIO::~TcpServerIO()
{
	
}

bool TcpServerIO::Bind(uint16_t port)
{
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(port);
	_addr.sin_addr.S_un.S_addr = INADDR_ANY;

	RETURN_MSG_AND_CODE_IF_TRUE(bind(_sock, (sockaddr *)&_addr, _len) < 0,"Couldn't bind on provided port",WSAGetLastError());
	RETURN_MSG_AND_CODE_IF_TRUE(listen(_sock, LISTEN_COUNT_DEFAULT_VALUE) == SOCKET_ERROR,"Couldn't listen()",WSAGetLastError());

	return true;
}

bool TcpServerIO::Send(TcpSession *tcpSession, uint8_t *buffer, ssize_t size)
{
	WSABUF buf;
	buf.buf = (char *)buffer;
	buf.len = size;

	RETURN_MSG_AND_CODE_IF_TRUE(WSASend(tcpSession->_sock, &buf, 1, &_dwBytes, _socketFlags, 0, 0) == 0,"WSASend() failed",WSAGetLastError());

	return true;
}
