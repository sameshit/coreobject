#include "../TcpClientIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpClientIO::TcpClientIO(Memory *memory, Logger *logger)
:TcpSession(memory, logger, IO_TCP_CLIENT)
{
    _connected = false;
}

TcpClientIO::~TcpClientIO()
{
    if (_connected)
        shutdown(_sock,SHUT_RDWR);
}

bool TcpClientIO::ConnectIO(const char *host, uint16_t port)
{
    struct hostent *server = gethostbyname(host);

	_addr.sin_family = AF_INET;
	memcpy(&server->h_addr, (char *)&_addr.sin_addr.s_addr, server->h_length);
	_addr.sin_port = htons(port);

	RETURN_MSG_IF_TRUE(connect(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr_in)) < 0 && errno != EINPROGRESS,
               "connect() failed. Errno: "<<errno);

	_connected = true;
	return true;
}

bool TcpClientIO::Send(uint8_t *data,ssize_t size)
{
  RETURN_MSG_IF_TRUE (send(_sock,data,size,0) == -1, "send() failed. Errno: "<<errno);
  return true;
}
