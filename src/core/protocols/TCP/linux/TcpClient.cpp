#include "../TcpClient.h"
using namespace CoreObjectLib;

TcpClient::TcpClient(CoreObject *core) : _core(core), TcpClientIO(core->GetMemory(), core->GetLogger())
{

}

TcpClient::~TcpClient()
{
   _core->GetLoop()->Delete(this);
}

bool TcpClient::Connect(const char *host, uint16_t port)
{
  RETURN_IF_TRUE(!TcpClientIO::ConnectIO(host, port));
  RETURN_IF_TRUE(!_core->GetLoop()->Add(this));
  return true;
}
