#include "../TcpServerIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

TcpServerIO::TcpServerIO(Memory *memory, Logger *logger) : BaseSocket(memory, logger, IO_TCP_SERVER)
{

}

TcpServerIO::~TcpServerIO()
{
	std::set<TcpSession *>::iterator it;
	TcpSession *session;

	for(it = _sessions.begin(); it != _sessions.end(); it++)
	{
	    session = (*it);
	    long_delete(_memory, session);
	}

}

bool TcpServerIO::Bind(uint16_t port)
{
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = INADDR_ANY;

    RETURN_MSG_IF_TRUE(bind(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr)) < 0,"bind() failed. Errno: "<<errno);
    RETURN_MSG_IF_TRUE(listen(_sock, LISTEN_COUNT_DEFAULT_VALUE) < 0,"listen() failed. Errno: "<<errno);

    return true;
}

bool TcpServerIO::Send(TcpSession *session, uint8_t *data,ssize_t size)
{
    RETURN_MSG_IF_TRUE(send(session->_sock,data,size,0) == -1,"send() failed. Errno: "<<errno);
    return true;
}
