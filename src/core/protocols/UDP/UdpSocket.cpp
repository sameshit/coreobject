#include "UdpSocket.h"
using namespace CoreObjectLib;

UdpSocket::UdpSocket(CoreObject *core)
:UdpSocketIO(core->GetMemory(), core->GetLogger()),
_core(core),_deleted(false)
{
	
}

UdpSocket::~UdpSocket()
{
    if (!_deleted)
        ASSERT_ERROR(_core->GetLoop()->Delete(this));
}

bool UdpSocket::Bind(const uint16_t &port)
{
	RETURN_IF_FALSE(UdpSocketIO::Bind(port));
	RETURN_IF_FALSE(_core->GetLoop()->Add(this));

	return true;
}

bool UdpSocket::Bind(const char *ip,const uint16_t &port)
{
	RETURN_IF_FALSE(UdpSocketIO::Bind(ip,port));
	RETURN_IF_FALSE(_core->GetLoop()->Add(this));
    
	return true;
}

bool UdpSocket::DeleteFromLoop()
{
    if (_deleted)
    {
        COErr::Set("This socket has been already deleted from Loop");
        return false;
    }
    
    _deleted = true;
    return _core->GetLoop()->Delete(this);
}