#include "../UdpSocketIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

UdpSocketIO::UdpSocketIO(Memory *memory, Logger *logger) : BaseSocket(memory, logger, IO_UDP_SOCKET)
{
	DWORD dwBytes;
	
	if(WSAIoctl(_sock, SIO_GET_EXTENSION_FUNCTION_POINTER, 
		&_ex_GuidSendMsg, sizeof(GUID),
		&_ex_lpfnSendMsg, sizeof(LPFN_WSASENDMSG), &dwBytes,
		NULL, NULL) == SOCKET_ERROR)
		_FATAL_ERROR2("Unable to load WSASendMsg()",WSAGetLastError());
}

UdpSocketIO::~UdpSocketIO()
{

}

bool UdpSocketIO::Bind(const uint16_t& port)
{
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = INADDR_ANY;

    RETURN_MSG_AND_CODE_IF_TRUE(bind(_sock, (sockaddr *)&_addr, sizeof(sockaddr_in)) < 0,"Couldn't bind on provided port",WSAGetLastError());
    return true;
}

bool UdpSocketIO::Bind(const char *ip,const uint16_t& port)
{
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = inet_addr(ip);

    RETURN_MSG_AND_CODE_IF_TRUE(bind(_sock, (sockaddr *)&_addr, sizeof(sockaddr_in)) < 0,"Couldn't bind on provided port",WSAGetLastError());
    return true;
}

bool UdpSocketIO::SendTo(uint8_t *buffer, ssize_t size,
		struct sockaddr *addr, socklen_t len)
{
	WSABUF buf_send;
	buf_send.buf = (char *)buffer;
	buf_send.len = size;

	RETURN_MSG_AND_CODE_IF_TRUE(WSASendTo(_sock, &buf_send, 1, NULL, _socketFlags, addr, len, 0, 0) == 0,"WSASendTo() failed",WSAGetLastError());
		
	return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, sockaddr * addr, socklen_t len)
{
	DWORD		dwBytes;
	WSABUF		wsaBufs[2];

	wsaBufs[0].buf = (CHAR *)data1;
	wsaBufs[0].len = size1;
	wsaBufs[1].buf = (CHAR *)data2;
	wsaBufs[1].len = size2;
	
	ssize_t total_size = size1 + size2;

	RETURN_MSG_AND_CODE_IF_TRUE((WSASendTo(_sock, wsaBufs, 2, &dwBytes, 0, addr, len, 0, 0) != 0 || dwBytes!= total_size),
		"WSASendTo() failed()",WSAGetLastError());
	
	return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, sockaddr * addr, socklen_t len)
{
	DWORD		dwBytes;
	WSABUF		wsaBufs[3];

	wsaBufs[0].buf = (CHAR *)data1;
	wsaBufs[0].len = size1;
	wsaBufs[1].buf = (CHAR *)data2;
	wsaBufs[1].len = size2;
	wsaBufs[2].buf = (CHAR *)data3;
	wsaBufs[2].len = size3;
	
	ssize_t total_size = size1 + size2 + size3;

	RETURN_MSG_AND_CODE_IF_TRUE((WSASendTo(_sock, wsaBufs, 3, &dwBytes, 0, addr, len, 0, 0) != 0 || dwBytes!= total_size),"WSASendTo() failed",WSAGetLastError() );
	
	return true;
}

bool UdpSocketIO::SendTo(uint8_t * data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, uint8_t *data4, ssize_t size4, sockaddr * addr, socklen_t len)
{
	DWORD		dwBytes;
	WSABUF		wsaBufs[4];

	wsaBufs[0].buf = (CHAR *)data1;
	wsaBufs[0].len = size1;
	wsaBufs[1].buf = (CHAR *)data2;
	wsaBufs[1].len = size2;
	wsaBufs[2].buf = (CHAR *)data3;
	wsaBufs[2].len = size3;
	wsaBufs[3].buf = (CHAR *)data4;
	wsaBufs[3].len = size4;

	ssize_t total_size = size1 + size2 + size3 + size4;

	RETURN_MSG_AND_CODE_IF_TRUE((WSASendTo(_sock, wsaBufs, 4, &dwBytes, 0, addr, len, 0, 0) != 0 || dwBytes!= total_size),"WSASendTo() failed",
		WSAGetLastError());
	
	return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, uint8_t *data4, ssize_t size4, uint8_t *data5, ssize_t size5, sockaddr * addr, socklen_t len)
{
	DWORD		dwBytes;
	WSABUF		wsaBufs[5];

	wsaBufs[0].buf = (CHAR *)data1;
	wsaBufs[0].len = size1;
	wsaBufs[1].buf = (CHAR *)data2;
	wsaBufs[1].len = size2;
	wsaBufs[2].buf = (CHAR *)data3;
	wsaBufs[2].len = size3;
	wsaBufs[3].buf = (CHAR *)data4;
	wsaBufs[3].len = size4;
	wsaBufs[4].buf = (CHAR *)data5;
	wsaBufs[4].len = size5;

	ssize_t total_size = size1 + size2 + size3 + size4 + size5;

	RETURN_MSG_AND_CODE_IF_TRUE((WSASendTo(_sock, wsaBufs, 5, &dwBytes, 0, addr, len, 0, 0) != 0 || dwBytes!= total_size),"WSASendTo() failed",WSAGetLastError());
	
	return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, uint8_t *data4, ssize_t size4, uint8_t *data5, ssize_t size5,uint8_t *data6,ssize_t size6, sockaddr * addr, socklen_t len)
{
	DWORD		dwBytes;
	WSABUF		wsaBufs[6];

	wsaBufs[0].buf = (CHAR *)data1;
	wsaBufs[0].len = size1;
	wsaBufs[1].buf = (CHAR *)data2;
	wsaBufs[1].len = size2;
	wsaBufs[2].buf = (CHAR *)data3;
	wsaBufs[2].len = size3;
	wsaBufs[3].buf = (CHAR *)data4;
	wsaBufs[3].len = size4;
	wsaBufs[4].buf = (CHAR *)data5;
	wsaBufs[4].len = size5;
	wsaBufs[5].buf = (CHAR *)data6;
	wsaBufs[5].len = size6;

	ssize_t total_size = size1 + size2 + size3 + size4 + size5 + size6;

	RETURN_MSG_AND_CODE_IF_TRUE((WSASendTo(_sock, wsaBufs, 5, &dwBytes, 0, addr, len, 0, 0) != 0 || dwBytes!= total_size),"WSASendTo() failed",WSAGetLastError());
	
	return true;
}