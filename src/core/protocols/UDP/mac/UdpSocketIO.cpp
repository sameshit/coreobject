#include "../UdpSocketIO.h"
#include "../../../error/COErr.h"
using namespace CoreObjectLib;

UdpSocketIO::UdpSocketIO(Memory *memory, Logger *logger) : BaseSocket(memory, logger, IO_UDP_SOCKET)
{

}

UdpSocketIO::~UdpSocketIO()
{

}

bool UdpSocketIO::Bind(const uint16_t &port)
{
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = INADDR_ANY;

    CHECK_RETURN2(bind(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr_in)) < 0,"bind() failed",errno);
    
    return true;
}

bool UdpSocketIO::Bind(const char *ip, const uint16_t &port)
{
    _addr.sin_family        = AF_INET;
    _addr.sin_port          = htons(port);
    _addr.sin_addr.s_addr   = inet_addr(ip);
    
    CHECK_RETURN2(bind(_sock, (struct sockaddr *)&_addr, sizeof(struct sockaddr_in)) < 0,"bind() failed",errno);
    
    return true;
}

bool UdpSocketIO::SendTo(uint8_t *buffer, ssize_t size,struct sockaddr *addr, socklen_t len)
{
	CHECK_RETURN2(sendto(_sock, buffer, size, 0, (struct sockaddr *)addr, len) != size,"sendto() failed",errno);

    return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2,struct sockaddr *addr, socklen_t len)
{
    struct msghdr _msg;
    
    memset(&_msg, 0, sizeof(struct msghdr));
    
    struct iovec  _iovs[2];
    
    _msg.msg_name = addr;
    _msg.msg_namelen = len;
    
    _iovs[0].iov_base = data1;
    _iovs[0].iov_len  = size1;
    _iovs[1].iov_base = data2;
    _iovs[1].iov_len  = size2;
    
    _msg.msg_iov = _iovs;
    _msg.msg_iovlen = 2;
    
    CHECK_RETURN2(sendmsg(_sock, &_msg, 0) != size1 + size2,"sendmsg() failed",errno);

    return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, 
		struct sockaddr *addr, socklen_t len)
{
    struct msghdr _msg;
    
    memset(&_msg, 0, sizeof(struct msghdr));
    
    struct iovec  _iovs[3];
    
    _msg.msg_name = addr;
    _msg.msg_namelen = len;
    
    _iovs[0].iov_base = data1;
    _iovs[0].iov_len  = size1;
    _iovs[1].iov_base = data2;
    _iovs[1].iov_len  = size2;
    _iovs[2].iov_base = data3;
    _iovs[2].iov_len  = size3;
    
    _msg.msg_iov = _iovs;
    _msg.msg_iovlen = 3;
    
    CHECK_RETURN2(sendmsg(_sock, &_msg, 0) != size1 + size2 + size3,"sendmsg() failed",errno);

    return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, 
		uint8_t *data4, ssize_t size4, struct sockaddr *addr, socklen_t len)
{
    struct msghdr _msg;
    
    memset(&_msg, 0, sizeof(struct msghdr));
    
    struct iovec  _iovs[4];
    
    _msg.msg_name = addr;
    _msg.msg_namelen = len;
    
    _iovs[0].iov_base = data1;
    _iovs[0].iov_len  = size1;
    _iovs[1].iov_base = data2;
    _iovs[1].iov_len  = size2;
    _iovs[2].iov_base = data3;
    _iovs[2].iov_len  = size3;
    _iovs[3].iov_base = data4;
    _iovs[3].iov_len  = size4;
    
    _msg.msg_iov = _iovs;
    _msg.msg_iovlen = 4;
    
    CHECK_RETURN2(sendmsg(_sock, &_msg, 0) != size1 + size2 + size3 + size4,"sendmsg() failed",errno);

    return true;
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3, 
		uint8_t *data4, ssize_t size4, uint8_t *data5, ssize_t size5, struct sockaddr *addr, socklen_t len)
{
    struct msghdr _msg;
    
    memset(&_msg, 0, sizeof(struct msghdr));
    
    struct iovec  _iovs[5];
    
    _msg.msg_name = addr;
    _msg.msg_namelen = len;
    
    _iovs[0].iov_base = data1;
    _iovs[0].iov_len  = size1;
    _iovs[1].iov_base = data2;
    _iovs[1].iov_len  = size2;
    _iovs[2].iov_base = data3;
    _iovs[2].iov_len  = size3;
    _iovs[3].iov_base = data4;
    _iovs[3].iov_len  = size4;
    _iovs[4].iov_base = data5;
    _iovs[4].iov_len  = size5;
    
    _msg.msg_iov = _iovs;
    _msg.msg_iovlen = 5;
    
    CHECK_RETURN2(sendmsg(_sock, &_msg, 0) != size1 + size2 + size3 + size4 + size5,"sendmsg() failed",errno);

    return true;        
}

bool UdpSocketIO::SendTo(uint8_t *data1, ssize_t size1, uint8_t *data2, ssize_t size2, uint8_t *data3, ssize_t size3,
                         uint8_t *data4, ssize_t size4, uint8_t *data5, ssize_t size5,uint8_t *data6,ssize_t size6, struct sockaddr *addr, socklen_t len)
{
    struct msghdr _msg;
    
    memset(&_msg, 0, sizeof(struct msghdr));
    
    struct iovec  _iovs[6];
    
    _msg.msg_name = addr;
    _msg.msg_namelen = len;
    
    _iovs[0].iov_base = data1;
    _iovs[0].iov_len  = size1;
    _iovs[1].iov_base = data2;
    _iovs[1].iov_len  = size2;
    _iovs[2].iov_base = data3;
    _iovs[2].iov_len  = size3;
    _iovs[3].iov_base = data4;
    _iovs[3].iov_len  = size4;
    _iovs[4].iov_base = data5;
    _iovs[4].iov_len  = size5;
    _iovs[5].iov_base = data6;
    _iovs[5].iov_len  = size6;
    
    _msg.msg_iov = _iovs;
    _msg.msg_iovlen = 6;
    
    CHECK_RETURN2(sendmsg(_sock, &_msg, 0) != size1 + size2 + size3 + size4 + size5 + size6,"sendmsg() failed",errno);
    
    return true;        
}
