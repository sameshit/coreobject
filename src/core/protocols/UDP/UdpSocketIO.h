#ifndef UDPSOCKETIO_H
#define UDPSOCKETIO_H

#include "../../socket/BaseSocket.h"

namespace CoreObjectLib
{
	#if defined (OS_WIN)
		static GUID _ex_GuidSendMsg = WSAID_WSASENDMSG;
	#endif
    
	class UdpSocketIO : public BaseSocket
	{
		#if defined (OS_WIN)
			LPFN_WSASENDMSG		_ex_lpfnSendMsg;
		#endif
	public:
		UdpSocketIO(Memory *memory, Logger *logger);
		virtual ~UdpSocketIO();

		Event<uint8_t *, ssize_t &, sockaddr *, socklen_t &> OnRecvFrom;
		
		bool SendTo(uint8_t *buffer, ssize_t size, sockaddr *addr, socklen_t len);
		bool SendTo(uint8_t *, ssize_t, uint8_t *, ssize_t, sockaddr *, socklen_t len);
		bool SendTo(uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, sockaddr *, socklen_t len);
		bool SendTo(uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, sockaddr *, socklen_t len);
		bool SendTo(uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, sockaddr *, socklen_t len);
        bool SendTo(uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t, uint8_t *, ssize_t,uint8_t*,ssize_t, sockaddr *, socklen_t len);
        protected:
            bool Bind(const uint16_t &port);
            bool Bind(const char *ip, const uint16_t &port);
	};
}

#endif //UDPSOCKETIO_H
