#ifndef UDPSOCKET_H
#define UDPSOCKET_H

#include "UdpSocketIO.h"
#include "../../../CoreObject.h"

namespace CoreObjectLib
{
	class LIBEXPORT UdpSocket : public UdpSocketIO
	{
	protected:
		CoreObject *_core;
        bool _deleted;
        bool DeleteFromLoop(); // CALL THIS METHOD ONLY FROM DESTRUCTOR TO STOP HANDLING SOCKET EVENTS
	public:
		UdpSocket(CoreObject *core);
		virtual ~UdpSocket();

		bool Bind(const uint16_t &port);
        bool Bind(const char *ip, const uint16_t &port);
        inline bool Bind(const std::string &ip, const uint16_t &port) {return Bind(ip.c_str(),port);}
	};
}

#endif //UDPSOCKET_H
