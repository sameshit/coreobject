#include "Timer.h"
using namespace CoreObjectLib;

Timer::Timer(CoreObject *core, uint64_t interval) 
:TimerIO(core->GetMemory(), core->GetLogger(), interval),_core(core)
{
	ASSERT_ERROR(_core->GetLoop()->Add(this));
}

Timer::~Timer()
{
	ASSERT_ERROR(_core->GetLoop()->Delete(this));
}