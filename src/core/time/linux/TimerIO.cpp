#include "../TimerIO.h"
using namespace CoreObjectLib;

TimerIO::TimerIO(Memory *memory, Logger *logger, uint64_t timeout) : _timeout(timeout), BaseIO(memory, logger, IO_TIMER)
{
    struct timespec now;
    if(clock_gettime(CLOCK_MONOTONIC, &now) == -1)
        _FATAL_ERROR("Error: cannot get clock time: "<<errno);

    _spec.it_value.tv_sec = now.tv_sec + _timeout / 1000;
	_spec.it_value.tv_nsec = now.tv_nsec;

    _spec.it_interval.tv_sec = _timeout / 1000;
    _spec.it_interval.tv_nsec = (_timeout % 1000) * 1000000;

    _timer = timerfd_create(CLOCK_MONOTONIC, 0);

    if(timerfd_settime(_timer, TFD_TIMER_ABSTIME, &_spec, 0) != 0)
        _FATAL_ERROR("Error: cannot set time to timer! Errno = " << errno);
}

TimerIO::~TimerIO()
{
    if(_timer > 0)
        close(_timer);
}
