#include "../COTime.h"
using namespace CoreObjectLib;

COTime::COTime()
{
	Update();
}

COTime::~COTime()
{
}

void COTime::Update()
{
    struct timespec now;

    if(clock_gettime(CLOCK_MONOTONIC, &now) != 0)
    {
        //ToDo: log cannot get time!
        printf("Error: clock_gettime: %i\n", errno);
    }

    _time = ((now.tv_sec * 1000) + (now.tv_nsec / 1000000));
}
