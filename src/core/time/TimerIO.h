#ifndef TIMERIO_H
#define TIMERIO_H

#include "COTime.h"
#include "../socket/BaseIO.h"
#include "../event/Event.h"

namespace CoreObjectLib
{
	class TimerIO : public BaseIO
	{
    private:
        #if defined (OS_WIN)
            COTime                _next_timeout;
            OVERLAPPEDEX        _wsaOverlapped;
			friend class Scheduler;
        #elif defined (OS_LINUX)
            int                 _timer;
            struct itimerspec   _spec;
            epoll_event         _ev;
        #elif defined (OS_X) || defined (OS_IOS)
            int                 _timer;
            static int          _last_timer;
        #endif

        uint64_t                _timeout;

        friend class Loop;
	public:

		TimerIO(Memory *memory, Logger *logger, uint64_t timeout);
		virtual ~TimerIO();

		Event<> OnTimer;
	};
}

#endif //TIMERIO_H
