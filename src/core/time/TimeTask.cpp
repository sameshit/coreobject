#include "TimeTask.h"

using namespace CoreObjectLib;

TimeTask::TimeTask()
:_timeout(0)
{

}

TimeTask::~TimeTask()
{

}

void TimeTask::SetTimeout(const uint64_t& timeout)
{
    _timeout = timeout;
}