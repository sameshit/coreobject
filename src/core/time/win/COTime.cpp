#include "../COTime.h"
using namespace CoreObjectLib;

COTime::COTime()
{
	Update();
}

COTime::~COTime()
{
}

void COTime::Update()
{
	OSVERSIONINFO version;

	if(!GetVersionEx(&version))
	{
		_time = GetTickCount();
		return;
	}

	if(version.dwMajorVersion == 6)
		_time = GetTickCount64();	//Chech for comple on XP
	else
		_time = GetTickCount();
}