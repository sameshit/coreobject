#include "../TimerIO.h"
using namespace CoreObjectLib;

TimerIO::TimerIO(Memory *memory, Logger *logger, uint64_t timeout) : _timeout(timeout), BaseIO(memory, logger, IO_TIMER)
{
	memset(&_wsaOverlapped, 0, sizeof(OVERLAPPEDEX));
	this->_wsaOverlapped.pData = (void *)this;
	_wsaOverlapped.hEvent = WSACreateEvent();

	_next_timeout.Update();
	_next_timeout += timeout;
}

TimerIO::~TimerIO()
{
	WSACloseEvent(_wsaOverlapped.hEvent);
}