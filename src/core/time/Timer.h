#ifndef TIMER___HH
#define TIMER___HH

#include "TimerIO.h"
#include "../../CoreObject.h"

namespace CoreObjectLib
{
	class LIBEXPORT Timer
	:public TimerIO
	{
    protected:
		CoreObject *_core;
	public:
		Timer(CoreObject *core, uint64_t interval);
		~Timer();
	};
}

#endif //TIMER_H
