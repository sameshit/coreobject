#ifndef CoreObject_TimeTask_h
#define CoreObject_TimeTask_h

#include "../../common/config.h"
#include "../event/Event.h"
#include "../time/COTime.h"
#include "../socket/BaseIO.h"

namespace CoreObjectLib
{
    class LIBEXPORT TimeTask
    {
	public:
        TimeTask();
        virtual ~TimeTask();
        
        void SetTimeout(const uint64_t &timeout);
        Event<> OnTimeTask;
    private:
        uint64_t _timeout;
        COTime _next_timeout;
        
        friend class Scheduler;
        friend class Loop;
#if defined(OS_WIN)
		OVERLAPPEDEX        _overlapped;
#endif
    };
}


#endif
