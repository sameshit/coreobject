#ifndef COTIME____H
#define COTIME____H

#include "../../common/config.h"


namespace CoreObjectLib
{
	class LIBEXPORT COTime
	{
    private:
        uint64_t _time;
	public:
		COTime();
		virtual ~COTime();

		void Update();


		bool		operator		<	(const COTime &t) const
		{
			return _time < t._time;
		}
		bool		operator		>	(const COTime &t) const
		{
			return _time > t._time;
		}

		bool		operator		<	(const uint64_t value) const
		{
			return _time < value;
		}

		bool		operator		>	(const uint64_t	value) const
		{
			return _time > value;
		}

		bool		operator		>=	(const COTime &t) const
		{
			return _time >= t._time;
		}

		bool		operator		<=  (const COTime &t) const
		{
			return _time >= t._time;
		}

		bool		operator		<=	(const uint64_t value) const
		{
			return _time <= value;
		}

		bool		operator		>=	(const uint64_t	value) const
		{
			return _time >= value;
		}

		bool		operator		==	(const COTime &t) const
		{
			return _time == t._time;
		}
		uint64_t	operator		() ()const
		{
			return _time;
		}
		COTime		operator		+	(const uint64_t value)
		{
			COTime t;
			t._time = _time + value;
			return t;
		}
		COTime		operator		+	(const COTime &t)
		{
			COTime ret;
			ret._time = _time + t._time;
			return ret;
		}
		COTime&		operator		+=	(const uint64_t value)
		{
			_time += value;
			return *this;
		}

		COTime&		operator		+=	(const COTime &t)
		{
			_time += t._time;
			return *this;
		}
		uint64_t	operator		-	(const COTime &t)
		{
			return _time - t._time;
		}
		uint64_t	operator		-	(const uint64_t value)
		{
			return _time - value;
		}
		COTime&		operator		-=  (const COTime &t)
		{
			_time -= t._time;
			return *this;
		}
		COTime&		operator		-=  (const uint64_t value)
		{
			_time -= value;
			return *this;
		}
		COTime&		operator		=	(const uint64_t value)
		{
			_time = value;
			return *this;
		}
		COTime&		operator		=	(const COTime &t)
		{
			_time = t._time;
			return *this;
		}
	};
}

#endif
