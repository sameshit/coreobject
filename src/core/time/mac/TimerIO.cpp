#include "../TimerIO.h"
using namespace CoreObjectLib;

int TimerIO::_last_timer = 1000;


TimerIO::TimerIO(Memory *memory, Logger *logger, uint64_t timeout) : BaseIO(memory, logger, IO_TIMER), 
_timer(_last_timer),_timeout(timeout)
{
    ++_last_timer;
}

TimerIO::~TimerIO()
{

}
