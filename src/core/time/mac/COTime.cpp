#include "../COTime.h"
using namespace CoreObjectLib;

COTime::COTime()
{
	Update();
}

COTime::~COTime()
{
    
}

void COTime::Update()
{   
    mach_timebase_info_data_t info;
    
    mach_timebase_info(&info);
    
    _time = mach_absolute_time();
    
    _time *= info.numer;
    _time /= info.denom;    
    _time /= 1000000;
}