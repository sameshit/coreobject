#ifndef BASEIO_H
#define BASEIO_H

#include "../../utils/misc/Utils.h"
#include "../../common/config.h"
#include "../event/Event.h"
#include "../memory/Memory.h"
#include "../logger/LoggerLine.h"
#include "../thread/Semaphore.h"

namespace CoreObjectLib
{
	enum IO_TYPE {
		IO_TCP_SERVER		,
		IO_TCP_CLIENT		,
		IO_TCP_SESSION		,
		IO_UDP_SOCKET		,
		IO_TIMER			,
        IO_IDLE				,
		IO_TIMETASK			,
	};

    #if defined (OS_WIN)
	struct OVERLAPPEDEX : public WSAOVERLAPPED {
		void *pData;
		void *lpServer;
	};
	#endif

	class BaseIO
	{
	protected:
    	Semaphore *_sem;
		#ifdef OS_WIN
			OVERLAPPEDEX	_wsaOverlapped;
		#endif
	private:
		friend class Loop;
        IO_TYPE _ioType;
	public:
		Memory *_memory;
		Logger *_logger;

		BaseIO(Memory *memory, Logger *logger, IO_TYPE ioType);
		virtual ~BaseIO();
	};
}

#endif //BASEIO_H
