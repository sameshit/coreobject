#include "../BaseSocket.h"
#include "../../error/COErr.h"
using namespace CoreObjectLib;

BaseSocket::BaseSocket(Memory *memory, Logger *logger, IO_TYPE io_type, int sock) : BaseIO(memory, logger, io_type)
{
    if(sock != -1)
    {
		_sock = sock;
	}
	else
	{
	    switch(io_type)
	    {
   	    case IO_TCP_CLIENT:
	    case IO_TCP_SERVER:
	    case IO_TCP_SESSION:
	        _sock = socket(AF_INET, SOCK_STREAM,0 /*IPPROTO_TCP*/);
	        break;
	    case IO_UDP_SOCKET:
	        _sock = socket(AF_INET, SOCK_DGRAM,0 /*IPPROTO_UDP*/);
	        break;
        default:
            _FATAL_ERROR("Invalid sock type");
	    }
	}

	if(_sock == -1)
	    _FATAL_ERROR("Couldn't create socket. Errno: "<<errno);

	int on = 1;
	if (setsockopt(_sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(int)) !=0 )
        _FATAL_ERROR("setsockopt() failed: "<<errno);

	assert(Utils::SetSocketNonBlock(_sock));
}

const char *BaseSocket::GetHostName()
{
	return inet_ntoa(_addr.sin_addr);
}

uint16_t BaseSocket::GetPort()
{
	return ntohs(_addr.sin_port);
}

BaseSocket::~BaseSocket()
{
    if(_sock != -1)
	{
        shutdown(_sock, SHUT_RDWR);
	    close(_sock);
	}
}
