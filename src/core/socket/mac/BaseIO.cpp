#include "../BaseIO.h"
using namespace CoreObjectLib;

BaseIO::BaseIO(Memory *memory, Logger *logger, IO_TYPE ioType) 
: _ioType(ioType),_memory(memory), _logger(logger)
{
    _fast_new(_sem);
}

BaseIO::~BaseIO() 
{  
    _fast_delete(_sem);
}
