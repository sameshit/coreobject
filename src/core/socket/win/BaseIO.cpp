#include "../BaseIO.h"
using namespace CoreObjectLib;

BaseIO::BaseIO(Memory *memory, Logger *logger, IO_TYPE ioType) : _memory(memory), _logger(logger), _ioType(ioType)
{
	long_new(_memory,_sem);

	memset(&_wsaOverlapped, 0, sizeof(OVERLAPPEDEX));
	_wsaOverlapped.hEvent = WSACreateEvent();
	_wsaOverlapped.pData = this;
}

BaseIO::~BaseIO() 
{  
	long_delete(_memory,_sem);
	WSACloseEvent(_wsaOverlapped.hEvent);
}
