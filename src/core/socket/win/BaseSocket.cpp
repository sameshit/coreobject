#include "../BaseSocket.h"
using namespace CoreObjectLib;

BaseSocket::BaseSocket(Memory *memory, Logger *logger, IO_TYPE io_type, SOCKET sock) : BaseIO(memory, logger, io_type)
{
	_wsaBuf.buf = (char *)_buffer;
	_wsaBuf.len = BUFFER_MAX_SIZE;

	_socketFlags = 0;

	_len = sizeof(SOCKADDR_IN);

	if(sock != INVALID_SOCKET)
		_sock = sock;
	else
	{
		switch(io_type)
		{
		case IO_TCP_SERVER:
		case IO_TCP_CLIENT:
		case IO_TCP_SESSION:
			{
				_sock = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED);
				break;
			}
		case IO_UDP_SOCKET:
			{
				_sock = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0, 0, WSA_FLAG_OVERLAPPED);
				break;
			}
		default:
			break;
		}
	}

	if(_sock == INVALID_SOCKET)
	{
		_LOG_ERROR ("Error: cannot create socket.  " << WSAGetLastError());
		abort();
	}
}

const char *BaseSocket::GetHostName()
{
	return inet_ntoa(_addr.sin_addr);
}

uint16_t BaseSocket::GetPort()
{
	return ntohs(_addr.sin_port);
}

BaseSocket::~BaseSocket()
{
	if(_sock != INVALID_SOCKET)
	{
		closesocket(_sock);
		_sock = INVALID_SOCKET;
	}
}
