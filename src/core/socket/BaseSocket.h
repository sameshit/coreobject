#ifndef BASESOCKET_H
#define BASESOCKET_H

#include "BaseIO.h"

#define BUFFER_MAX_SIZE 65535

namespace CoreObjectLib
{
	class BaseSocket : public BaseIO
	{
	public:
		#if defined (OS_WIN)
			BaseSocket(Memory *memory, Logger *logger, IO_TYPE io_type, SOCKET sock = INVALID_SOCKET);
		#elif defined (OS_LINUX) || defined (OS_X) || defined (OS_IOS)
			BaseSocket(Memory *memory, Logger *logger, IO_TYPE io_type, int sock = -1);
		#else
			IMPLEMENT ME
		#endif

		virtual ~BaseSocket();

		const char *GetHostName();
		uint16_t	GetPort();

	protected:
		#if defined (OS_WIN)
			SOCKET			_sock;
			SOCKADDR_IN		_addr;
			INT				_len;

			HANDLE			_hCompletionPort;
			WSABUF			_wsaBuf;
			DWORD			_dwBytes;
			DWORD			_socketFlags;
			uint8_t			_buffer [BUFFER_MAX_SIZE];
		#elif defined (OS_LINUX)
            int                 _sock;
            struct sockaddr_in  _addr;
	    struct epoll_event  _ev;
            socklen_t           _len;
		#elif defined (OS_X) || defined (OS_IOS)
            int                 _sock;
            socklen_t           _len;
            struct sockaddr_in  _addr;        
		#endif
		friend class Loop;
	};
}

#endif //BASESOCKET_H
