#include "../COErr.h"

using namespace std;
using namespace CoreObjectLib;

static uint32_t numeric_thread_id_count = 0;

class COErrStruct
{
public:
	COErrStruct() {}
	virtual ~COErrStruct() {}
	ThreadId thread_id;
	stringstream ss;
	int code;
	uint32_t reference_count;
	string err_string;
};

__declspec(thread) COErrStruct *err_struct;
__declspec(thread) bool is_init = false;

bool ThreadId::operator==(const ThreadId& thread_id) const
{
    return _thread_id == thread_id._thread_id;
}

ThreadId& ThreadId::operator=(const ThreadId& thread_id)
{
    _thread_id = thread_id._thread_id;
    return *this;
}

void COErr::Init()
{
	if (is_init)
		++err_struct->reference_count;
	else
	{
		err_struct = new COErrStruct;
		err_struct->thread_id._thread_id = GetCurrentThreadId();
		err_struct->thread_id._numeric_thread_id = numeric_thread_id_count;
		++numeric_thread_id_count;
		is_init = true;
	}
}

void COErr::Free()
{
	if (is_init)
	{
		--err_struct->reference_count;
		if (err_struct->reference_count == 0)
		{
			is_init = false;
			delete err_struct;
		}
	}
}

ostream& COErr::Set()
{
	err_struct->ss.str("");
	err_struct->ss.clear();
	return err_struct->ss;
}

void COErr::Set(const char *msg)
{
	err_struct->ss.str("");
	err_struct->ss.clear();
	err_struct->ss << msg;
}

void COErr::Set(const char *msg,int code)
{
	Set(msg);
	err_struct->code = code;
}

const char* COErr::Get()
{
	err_struct->err_string = err_struct->ss.str();
	return err_struct->err_string.c_str();
}

void COErr::SetCode(int code)
{
	err_struct->code = code;
}

int COErr::GetCode()
{
	return err_struct->code;
}

const ThreadId& COErr::GetThreadId()
{
	return err_struct->thread_id;
}

const uint32_t& COErr::GetNumericThreadId()
{
	return err_struct->thread_id._numeric_thread_id;
}