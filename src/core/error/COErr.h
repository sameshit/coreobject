#ifndef COERR__H
#define COERR__H
#include "../../common/config.h"

namespace CoreObjectLib
{
    class ThreadId;

	class LIBEXPORT COErr
	{
    private:
        static void Init();
        static void Free();
        friend class Thread;
        friend class CoreObject;
	public:
		static std::ostream& Set();
		static void Set(const char *msg);
        static void Set(const char *msg,int code);
		static const char* Get();
		static void SetCode(int code);
		static int GetCode();
        static const ThreadId& GetThreadId();
		static const uint32_t& GetNumericThreadId();

		static void SegFault()
		{
			int *ptr = nullptr;
			*ptr = 0;
		}
	};

	class LIBEXPORT ThreadId
	{
	public:
		ThreadId() {}
		virtual ~ThreadId() {}

        bool operator==(const ThreadId& thread_id) const;
        ThreadId& operator=(const ThreadId& thread_id);
		inline const uint32_t& GetNumericId() {return _numeric_thread_id;}
        private:
            friend class COErr;
			friend class LoggerLine;
			uint32_t _numeric_thread_id;
#if defined(OS_LINUX)
            pid_t _thread_id;
#elif defined(OS_X)
            mach_port_t _thread_id;
#elif defined(OS_WIN)
			DWORD _thread_id;
#endif
	};
}

#define RETURN_IF_FALSE(cond) if(!(cond))return false;
#define RETURN_IF_TRUE(cond) if((cond))return false;
#define RETURN_MSG_IF_FALSE(cond,msg) if(!(cond)){CoreObjectLib::COErr::Set()<< msg;return false;}
#define RETURN_MSG_IF_TRUE(cond,msg) if((cond)){CoreObjectLib::COErr::Set() << msg;return false;}
#define RETURN_MSG_AND_CODE_IF_FALSE(cond,msg,code) if (!(cond)){CoreObjectLib::COErr::Set() << msg; CoreObjectLib::COErr::SetCode(code);return false;}
#define RETURN_MSG_AND_CODE_IF_TRUE(cond,msg,code) if ((cond)){CoreObjectLib::COErr::Set() << msg; CoreObjectLib::COErr::SetCode(code);return false;}
#define ABORT_IF_FALSE(cond) if(!(cond)){LOG_ERROR("Abort: "<<CoreObjectLib::COErr::Get());LOG_FLUSH();CoreObjectLib::COErr::SegFault();}
#define ABORT_IF_TRUE(cond) if((cond)){LOG_ERROR("Abort: "<<CoreObjectLib::COErr::Get());LOG_FLUSH();CoreObjectLib::COErr::SegFault();}
#define ABORT_MSG_IF_FALSE(cond,msg) if (!(cond)){LOG_ERROR(msg);LOG_FLUSH(); CoreObjectLib::COErr::SegFault(); }
#define ABORT_MSG_IF_TRUE(cond,msg) if ((cond)){LOG_ERROR(msg);LOG_FLUSH(); CoreObjectLib::COErr::SegFault(); }

#define ASSERT_ERROR(cond) do{if(!(cond)){LOG_ERROR(CoreObjectLib::COErr::Get() <<" .Code :" << CoreObjectLib::COErr::GetCode());LOG_FLUSH();CoreObjectLib::COErr::SegFault();}}while(false)
#define _ASSERT_ERROR(cond) do{if(!(cond)){_LOG_ERROR(CoreObjectLib::COErr::Get() <<" .Code :" << CoreObjectLib::COErr::GetCode());_LOG_FLUSH(); CoreObjectLib::COErr::SegFault();}}while(false)
#define LONG_ASSERT_ERROR(logger,cond) do{if(!(cond)){LONG_LOG_ERROR(logger,CoreObjectLib::COErr::Get() <<" .Code :" << CoreObjectLib::COErr::GetCode()); LONG_LOG_FLUSH(logger); CoreObjectLib::COErr::SegFault();}}while(false)

#define FATAL_ERROR(msg) do{LOG_ERROR(msg);LOG_FLUSH();CoreObjectLib::COErr::SegFault();}while(false)
#define _FATAL_ERROR(msg) do{_LOG_ERROR(msg);_LOG_FLUSH();CoreObjectLib::COErr::SegFault();}while(false)
#define LONG_FATAL_ERROR(logger,msg) do{LONG_LOG_ERROR(logger,msg);LONG_LOG_FLUSH(logger);CoreObjectLib::COErr::SegFault();}while(false)

#define FATAL_ERROR2(msg,add) do{LOG_ERROR(msg<< " " <<". Additional info: " << add);LOG_FLUSH();CoreObjectLib::COErr::SegFault();}while(false)
#define _FATAL_ERROR2(msg,add) do{_LOG_ERROR(msg<< " " <<". Additional info: " << add);_LOG_FLUSH();CoreObjectLib::COErr::SegFault();}while(false)
#define LONG_FATAL_ERROR2(msg,add) do{LONG_LOG_ERROR(logger,msg<< " " <<". Additional info: " << add);LONG_LOG_FLUSH(logger);CoreObjectLib::COErr::SegFault();}while(false)

#endif
