#include "../COErr.h"
#include "../../thread/SpinLock.h"

using namespace CoreObjectLib;
using namespace std;

class COErrStruct
{
    public:
    COErrStruct() {}
    virtual ~COErrStruct() {}
    int         code;
    stringstream ss;
    string error;
    ThreadId    thread_id;
    uint32_t    reference_count;
};

__thread COErrStruct *coerr=nullptr;

bool ThreadId::operator==(const ThreadId& thread_id) const
{
    return _thread_id == thread_id._thread_id;
}

ThreadId& ThreadId::operator=(const ThreadId& thread_id)
{
    _thread_id = thread_id._thread_id;
    return *this;
}

void COErr::Init()
{
    if (coerr != nullptr)
        ++coerr->reference_count;
    else
    {
        coerr = new COErrStruct;
        coerr->reference_count = 0;
        coerr->thread_id._thread_id = syscall(SYS_gettid);
    }
}

void COErr::Free()
{
    if (coerr != nullptr)
    {
        --coerr->reference_count;
        if (coerr->reference_count == 0)
        {
            delete coerr;
            coerr = nullptr;
        }
    }
}

void COErr::Set(const char *msg)
{
   coerr->ss << msg;
}

void COErr::Set(const char *msg,int code)
{
    Set(msg);
    coerr->code = code;
}

const char* COErr::Get()
{
    coerr->error = coerr->ss.str().c_str();
    return (const char*)coerr->error.c_str();
}

ostream& COErr::Set()
{
    return coerr->ss;
}

void COErr::SetCode(int code)
{
    coerr->code = code;
}

int COErr::GetCode()
{
    return coerr->code;
}

const ThreadId& COErr::GetThreadId()
{
    return coerr->thread_id;
}
