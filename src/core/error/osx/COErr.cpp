#include "../COErr.h"

using namespace std;
using namespace CoreObjectLib;

const uint32_t err_msg_size = 128;

typedef struct COErrStruct
{
    ThreadId    thread_id;
    int         code;
    char        msg[err_msg_size];
    uint32_t    reference_count;
}COErrStruct;

static pthread_key_t key;
static pthread_once_t key_once = PTHREAD_ONCE_INIT;

static void make_key()
{
    pthread_key_create(&key, NULL);
}

bool ThreadId::operator==(const ThreadId& thread_id) const
{
    return _thread_id == thread_id._thread_id;
}


ThreadId& ThreadId::operator=(const ThreadId& thread_id)
{
    _thread_id = thread_id._thread_id;
    return *this;
}

void COErr::Init()
{
    COErrStruct *err_struct;
    
    pthread_once(&key_once, make_key);
    
    if ((err_struct = (COErrStruct*)pthread_getspecific(key)) == NULL)
    {
        err_struct = new COErrStruct;
        err_struct->thread_id._thread_id = pthread_mach_thread_np(pthread_self());
        err_struct->reference_count = 0;
        pthread_setspecific(key, (void*)err_struct);
    }
    else
        ++err_struct->reference_count;
}

void COErr::Free()
{
    COErrStruct *err_struct;
    
    if ((err_struct = (COErrStruct*)pthread_getspecific(key)) != NULL)
    {
        --err_struct->reference_count;
        if (err_struct->reference_count <=0)
            pthread_setspecific(key, NULL);
    }
}

void COErr::Set(const char *msg)
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    sprintf(err_struct->msg,"Error: %s",msg);
}

void COErr::Set(const char *msg,int code)
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    sprintf(err_struct->msg,"Error: %s",msg);
    err_struct->code = code;
}

const char* COErr::Get()
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    return err_struct->msg;
}

void COErr::SetCode(int code)
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    err_struct->code = code;
}

int COErr::GetCode()
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    return err_struct->code;
}

const ThreadId& COErr::GetThreadId()
{
    COErrStruct *err_struct;
    
    err_struct = (COErrStruct*)pthread_getspecific(key);
    assert(err_struct != NULL);
    
    return err_struct->thread_id;
}
