#include "PointerInfo.h"

using namespace CoreObjectLib;

PointerInfo::PointerInfo(size_t size,uint32_t alignment)
:_alignment(alignment)
{
	if(alignment != 0)
		_ptr	= Utils::AllocAligned(alignment,size);
	else
		_ptr    = malloc(size);
    _size   = size;
    _freed  = false;
}

PointerInfo::~PointerInfo()
{
	if(IsAligned())
		Utils::FreeAligned(_ptr);
	else
	    free(_ptr);    
}


void PointerInfo::SetDebugInfo(const char *filename, const char *function, int line, const char *time)
{
    ssize_t len;
    
    len = strlen(filename);
    if (len > 20)
    {
        _filename.assign("...");
        _filename.append(filename + (len - 20));
    }
    else
        _filename.assign(filename);
    _function.assign(function);
    _time.assign(time);
    _line = line;
}

