#ifndef MEMORY___H
#define MEMORY___H

#include "../logger/LoggerLine.h"
#include "PointerInfo.h"

#ifdef DEBUG_MODE_ON
#include "../thread/SpinLock.h"
#endif

namespace CoreObjectLib
{
#ifdef DEBUG_MODE_ON
    
class LIBEXPORT Memory
{
public:
    Memory(Logger *logger,bool disable_memory_logging);
    virtual ~Memory();

    void*       Alloc(size_t size,const char* filename,const char* function,int line);
    void        Free(void* ptr,const char* filename,const char* function,int line);
	void*		AlignedAlloc(uint32_t alignment,size_t size,const char *filename,const char *function,int line);
    
    template <typename T,typename ... Args>
    void New(T **ptr, const char *filename, const char *function, int line, const Args& ... args)
    {
        *ptr = (T*)Alloc(sizeof(T),filename,function,line);
        new (*ptr)T(args...);
    }
    
    template <typename A,typename T,typename ... Args>
    void TypedNew(T **ptr, const char *filename, const char *function, int line, const Args& ... args)
    {
        *ptr = (T*)Alloc(sizeof(A),filename,function,line);
        new (*ptr)A(args...);
    }
    
    template <typename T>
    void Delete(T **ptr, const char *filename, const char *function, int line)
    {
        (*ptr)->~T();
        Free(*ptr,filename,function,line);
        *ptr = nullptr;
    }
    
    template <typename T>
    void Alloc(T **ptr,size_t size,const char* filename,const char* function,int line)
    {
        *ptr = (T*)Alloc(size,filename,function,line);
    }
    
    template <typename T>
    void Free(T **ptr,const char* filename,const char* function,int line)
    {
        Free(*ptr,filename,function,line);
        *ptr = nullptr;
    }
    
    template <typename T>
    void AlignedAlloc(T **ptr,uint32_t alignment,size_t size,const char *filename,const char *function,int line)
    {
        *ptr = (T*)AlignedAlloc(alignment,size,filename,function,line);
    }
private:
    bool _disable_memory_logging;
    Logger *_logger;
    std::map<void*,PointerInfo*> _alloced_blocks;
    std::multimap<size_t,PointerInfo*> _freed_blocks;
	std::map<size_t,std::multimap<uint32_t,PointerInfo*> > _aligned_freed_blocks;
    SpinLock lock;
};


#define fast_alloc(ptr,count) _core->GetMemory()->Alloc(&ptr,sizeof(*ptr)*count,__FILE__,__FUNCTION__,__LINE__);
#define fast_free(ptr)			_core->GetMemory()->Free(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define _fast_alloc(ptr,count)	_memory->Alloc(&ptr,sizeof(*ptr)*count,__FILE__,__FUNCTION__,__LINE__)
#define _fast_free(ptr)			_memory->Free(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define long_alloc(mem,ptr,count) mem->Alloc(&ptr,sizeof(*ptr)*count,__FILE__,__FUNCTION__,__LINE__)
#define long_free(mem,ptr)         mem->Free(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define fast_aligned(ptr,alignment,count) _core->GetMemory()->AlignedAlloc(&ptr,alignment,sizeof(*ptr)*count,__FILE__,__FUNCTION__,__LINE__)
#define free_aligned(ptr) _core->GetMemory()->Free(&ptr,__FILE__,__FUNCTION__,__LINE__)
    
#define fast_new(ptr,...) _core->GetMemory()->New(&ptr,__FILE__,__FUNCTION__,__LINE__, ##__VA_ARGS__)
#define fast_delete(ptr)  _core->GetMemory()->Delete(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define _fast_new(ptr, ... ) _memory->New(&ptr,__FILE__,__FUNCTION__,__LINE__ , ##__VA_ARGS__ )
#define _fast_delete(ptr)  _memory->Delete(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define long_new(mem,ptr,...) mem->New(&ptr,__FILE__,__FUNCTION__,__LINE__,##__VA_ARGS__)
#define long_delete(mem,ptr)  mem->Delete(&ptr,__FILE__,__FUNCTION__,__LINE__)
#define typed_new(type,ptr,...) _core->GetMemory()->TypedNew<type>(&ptr,__FILE__,__FUNCTION__,__LINE__, ##__VA_ARGS__)
    
#else
	class LIBEXPORT Memory
	{
	public:
		Memory(Logger *logger,bool disable_memory_logging);
		virtual ~Memory();
        
        template <typename T,typename ... Args>
        void New(T **ptr,const Args& ... args)
        {
            *ptr = new T(args...);
        }
        
        template <typename A,typename T,typename ... Args>
        void TypedNew(T **ptr,const Args& ... args)
        {
            *ptr = new A(args...);
        }
        
        template <typename T>
        void Delete(T **ptr)
        {
            delete (*ptr);
            *ptr = nullptr;
        }
        
        template <typename T>
        void Alloc(T **ptr,size_t size)
        {
            *ptr = (T*)malloc(size);
        }
        
        template <typename T>
        void Free(T **ptr)
        {
            free(*ptr);
            *ptr = nullptr;
        }
        
        template <typename T>
        void AlignedAlloc(T **ptr,uint32_t alignment,size_t size)
        {
            *ptr = (T*)Utils::AllocAligned(alignment,size);
        }
	};

#define fast_alloc(ptr,count) _core->GetMemory()->Alloc(&ptr,sizeof(*ptr)*count)
#define fast_free(ptr)        _core->GetMemory()->Free(&ptr)
#define _fast_alloc(ptr,count) _memory->Alloc(&ptr,sizeof(*ptr)*count)
#define _fast_free(ptr)         _memory->Free(&ptr)
#define long_alloc(mem,ptr,count) mem->Alloc(&ptr,(sizeof(*ptr)*count)
#define long_free(mem,ptr)         mem->Free(&ptr)
#define fast_aligned(ptr,alignment,count) _core->GetMemory()->AlignedAlloc(&ptr,(alignment,sizeof(*ptr)*count)
#define free_aligned(ptr)	Utils::FreeAligned(ptr)
    
#define fast_new(ptr,...) _core->GetMemory()->New(&ptr,##__VA_ARGS__)
#define fast_delete(ptr)  _core->GetMemory()->Delete(&ptr)
#define _fast_new(ptr,...) _memory->New(&ptr,##__VA_ARGS__)
#define _fast_delete(ptr)  _memory->Delete(&ptr)
#define long_new(mem,ptr,...) mem->New(&ptr,##__VA_ARGS__)
#define long_delete(mem,ptr)  mem->Delete(&ptr)
#define typed_new(type,ptr,...) _core->GetMemory()->TypedNew<type>(&ptr,##__VA_ARGS__)    
#endif


}

#endif
