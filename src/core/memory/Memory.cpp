#include "Memory.h"

using namespace CoreObjectLib;
using namespace std;

#ifdef DEBUG_MODE_ON

Memory::Memory(Logger *logger,bool disable_memory_logging)
:_disable_memory_logging(disable_memory_logging)
{
    _logger = logger;
}

Memory::~Memory()
{
    std::map<void *,PointerInfo *>::iterator alloced_it;
    std::map<size_t,multimap<uint32_t,PointerInfo *> >::iterator aligned_it;
    PointerInfo *pointer_info;

	uint32_t aligned_size = 0;

	for (aligned_it = _aligned_freed_blocks.begin() ; aligned_it != _aligned_freed_blocks.end(); aligned_it ++)
		aligned_size += (*aligned_it).second.size();

    if (!_disable_memory_logging)
    {
        if (_alloced_blocks.size() != _freed_blocks.size() + aligned_size)
            _LOG_ERROR("Found memory leaks. Totally alloced ptrs count is "<<_alloced_blocks.size() <<", but freed only is " << _freed_blocks.size() + aligned_size);
        else
            _LOG_INFO("There is no memory leaks in this run using fast_alloc\\fast_new");
    }

    for (alloced_it = _alloced_blocks.begin(); alloced_it != _alloced_blocks.end(); alloced_it ++)
    {
        pointer_info = (*alloced_it).second;

        if (pointer_info->IsFreed() == false && !_disable_memory_logging)
        {
            _LOG_ERROR ("Memory leak in file: "<<pointer_info->GetFilename()<<"\nFunction(line): "<< pointer_info->GetFunction() <<"("<<pointer_info->GetLine() <<")\nTime: "<<pointer_info->GetTime());

        }
        delete pointer_info;
    }


    _freed_blocks.clear();
	_aligned_freed_blocks.clear();
    _alloced_blocks.clear();
}

void*   Memory::Alloc(size_t size,const char* filename,const char* function,int line)
{
    std::multimap<size_t,PointerInfo *>::iterator freed_it;
    PointerInfo *pointer_info;

    lock.Lock();

    freed_it = _freed_blocks.find(size);
    if (freed_it != _freed_blocks.end())
    {
        pointer_info = (*freed_it).second;
        _freed_blocks.erase(freed_it);
        pointer_info->SetFreed(false);
    }
    else
    {
        pointer_info = new PointerInfo(size);
        _alloced_blocks.insert(std::make_pair(pointer_info->GetPtr(),pointer_info));
    }

    time_t now;
    struct tm* timeinfo;
    char time_str[10];

    time(&now);
#ifdef OS_WIN
	struct tm win_timeinfo;
	localtime_s(&win_timeinfo,&now);
	timeinfo = &win_timeinfo;
#else
    timeinfo = localtime(&now);
#endif
	strftime(time_str,10,"%I:%M:%S",timeinfo);

    pointer_info->SetDebugInfo(filename,function,line,time_str);

    lock.UnLock();

    return pointer_info->GetPtr();
}

void* Memory::AlignedAlloc(uint32_t alignment,size_t size, const char *filename, const char* function, int line)
{
   std::map<size_t,multimap<uint32_t,PointerInfo *> >::iterator freed_it;
   multimap<uint32_t,PointerInfo *>::iterator in_freed_it;
   PointerInfo *pointer_info;
   bool create_new = false;

    lock.Lock();

    freed_it = _aligned_freed_blocks.find(size);
    if (freed_it != _aligned_freed_blocks.end())
	{
		in_freed_it = (*freed_it).second.find(alignment);
		if(in_freed_it != (*freed_it).second.end())
		{
			pointer_info = (*in_freed_it).second;
			(*freed_it).second.erase(in_freed_it);
			if ((*freed_it).second.size() == 0)
				_aligned_freed_blocks.erase(freed_it);
			pointer_info->SetFreed(false);
		}
		else
			create_new = true;
	}
	else
		create_new = true;

	if (create_new)
	{
		pointer_info = new PointerInfo(size,alignment);
		_alloced_blocks.insert(std::make_pair(pointer_info->GetPtr(),pointer_info));
	}

    time_t now;
    struct tm* timeinfo;
    char time_str[10];

    time(&now);
#ifdef OS_WIN
	struct tm win_timeinfo;
	localtime_s(&win_timeinfo,&now);
	timeinfo = &win_timeinfo;
#else
    timeinfo = localtime(&now);
#endif
	strftime(time_str,10,"%I:%M:%S",timeinfo);

    pointer_info->SetDebugInfo(filename,function,line,time_str);

    lock.UnLock();

    return pointer_info->GetPtr();
}

void    Memory::Free(void* ptr,const char* filename,const char* function,int line)
{
    std::map<void *,PointerInfo *>::iterator alloced_it;
    PointerInfo *pointer_info;

    lock.Lock();

    alloced_it = _alloced_blocks.find(ptr);
    if (alloced_it == _alloced_blocks.end())
    {
        _LOG_ERROR("Trying to free unexisting pointer.\nFile: "<<filename<<"\nFunction(line): "<<function<<"("<<line<<")");
        abort();
    }

    pointer_info = (*alloced_it).second;
    if (pointer_info->IsFreed())
    {
        _LOG_ERROR("Trying to free already freed pointer.\nFile: "<<filename<<"\nFunction(line): "<<function<<"("<< line<<")\n"
        "Previous file: "<<pointer_info->GetFilename()<<"\nPrevious function(line): "<<pointer_info->GetFunction() <<"("<< pointer_info->GetLine() <<")\nTime: "<<pointer_info->GetTime());
        exit(1);
    }

    pointer_info->SetFreed(true);

    time_t now;
    struct tm* timeinfo;
    char time_str[10];

    time(&now);
#ifdef OS_WIN
	struct tm win_timeinfo;
	localtime_s(&win_timeinfo,&now);
	timeinfo = &win_timeinfo;
#else
    timeinfo = localtime(&now);
#endif
	strftime(time_str,10,"%I:%M:%S",timeinfo);
    pointer_info->SetDebugInfo(filename,function,line,time_str);

	if (!pointer_info->IsAligned())
		_freed_blocks.insert(std::make_pair(pointer_info->GetSize(),pointer_info));
	else
	{
		map<size_t,multimap<uint32_t,PointerInfo * > >::iterator in_freed_it;
		multimap<uint32_t,PointerInfo *> new_multimap;

		in_freed_it = _aligned_freed_blocks.find(pointer_info->GetSize());
		if (in_freed_it == _aligned_freed_blocks.end())
		{
			new_multimap.insert(make_pair(pointer_info->GetAlignment(),pointer_info));
			_aligned_freed_blocks.insert(make_pair(pointer_info->GetSize(),new_multimap));
		}
		else
			(*in_freed_it).second.insert(make_pair(pointer_info->GetAlignment(),pointer_info));
	}
    lock.UnLock();
}

#else

Memory::Memory(Logger *logger, bool disable_memory_logging)
{

}

Memory::~Memory()
{

}

#endif
