#ifndef POINTERINFO__H
#define POINTERINFO__H

#include "../../common/config.h"
#include "../../utils/misc/Utils.h"

namespace CoreObjectLib
{

class LIBEXPORT PointerInfo
{
public:
    PointerInfo(size_t size,uint32_t alignment = 0);
    ~PointerInfo();
    
    inline bool     IsFreed()   {return _freed;}
    inline void*    GetPtr()    {return _ptr;}
    inline size_t   GetSize()   {return _size;}
    
    inline void     SetFreed(bool value) {_freed = value;}

	inline bool		IsAligned()	{return _alignment != 0;}
	inline uint32_t GetAlignment() {return _alignment;}
    
    void SetDebugInfo(const char *filename,const char* function, int line, const char *time);

    
    inline const char*     GetFilename() {return _filename.c_str();}
    inline const char*     GetFunction() {return _function.c_str();}
    inline int             GetLine()     {return _line;}
    inline const char*     GetTime()     {return _time.c_str();}    
private:
    std::string _filename;
    std::string _function;
    int         _line;
    std::string _time;    
    void        *_ptr;
    size_t      _size;
    bool        _freed;    
	uint32_t	_alignment;
};

}
#endif