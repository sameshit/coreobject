#include "../Loop.h"

using namespace CoreObjectLib;
using namespace std;

Loop::Loop(ThreadCache *thread_cache, Memory *memory, Logger *logger)
:_scheduler(NULL),_thread_cache(thread_cache)
,_thread(NULL),_exit(false),_logger(logger),_memory(memory),_kq(-1),_started(false)
{
    _fast_new(_mutex);
    _fast_new(_add_mutex);
    _fast_new(_del_mutex);
}

Loop::~Loop()
{
    if (_started)
    _ASSERT_ERROR(Stop());
    
    if (_kq != -1)
        close(_kq);
    
    _fast_delete(_mutex);
    _fast_delete(_add_mutex);
    _fast_delete(_del_mutex);
}

bool Loop::Start()
{
    struct kevent ev;
    _kq = kqueue();
    
    CHECK_RETURN2(_kq == -1,"Couldn't create kqueue",errno);    
    CHECK_RETURN2(pipe(_pipes) != 0, "Couldn't create pipes", errno); 
    
    read_pipe = _pipes[0];
    write_pipe = _pipes[1];
    
    int flags = fcntl(read_pipe, F_GETFL, 0);
    fcntl(write_pipe, F_SETFL, flags|O_NONBLOCK);
    
    EV_SET(&ev, read_pipe, EVFILT_READ,EV_ADD | EV_ENABLE, 0, 0, &read_pipe);
    
    CHECK_RETURN2(kevent(_kq, &ev, 1, NULL, 0, NULL) == -1,"Couldn't add read pipe to kq",errno);
    
    _thread = _thread_cache->GetThread();
    
    _thread->OnThread.Attach(this, &Loop::Work);
    
    COND_RETURN(!_thread->Wake());
    
    _fast_new(_scheduler, _thread_cache, _logger, write_pipe);
    
    _started = true;
    
    return true;
}

bool Loop::Stop()
{
    char ch = 'x';
    
    _mutex->Lock();
    _exit = true;
    _mutex->UnLock();
    
    if (_scheduler != NULL)
    {
        _fast_delete(_scheduler);
        _scheduler = NULL;
    }
        
    CHECK_RETURN2(write(write_pipe, &ch, 1) != 1,"Couldn't write to pipe",errno);
    
    _thread_cache->ReleaseThread(&_thread);
    
    _started = false;
    
    return true;
}

void signal_cv(int signum)
{
    fprintf(stderr,"signum is %d\n",signum);
}

void Loop::Work()
{
    bool exit;
    struct kevent ev;
    set<LoopIdleIO*>::iterator idle_it;
    BaseIO *base_io;
    sockaddr_in m_addr;
    socklen_t   m_len;
    ssize_t length_recv;
    int fd;
    TcpSession *session;
    
/*    for (int i = 0 ; i< 32 ; i ++)
        signal(i,signal_cv);*/
    
    _mutex->Lock();
    exit = _exit;
    _mutex->UnLock();
    
    if (exit)
        return;
    
    if (kevent(_kq, NULL, 0, &ev, 1, NULL) == -1)
    {
        return;
     //       _FATAL_ERROR2("kevent() failed",errno);
    }
    
    if (ev.udata == &read_pipe)
    {
        ProcessRequest();
        return;
    }    
    
    for (idle_it = _idle_set.begin(); idle_it != _idle_set.end(); idle_it ++)
        (*idle_it)->OnLoopIdle(); 
    
    base_io = (BaseIO *)ev.udata;
    
    
    m_len = sizeof(struct sockaddr_in);    
    switch (base_io->_ioType) 
    {
        case IO_UDP_SOCKET:
        {
            UdpSocketIO *udpSocket = (UdpSocketIO *)base_io;            
            
            length_recv = recvfrom(udpSocket->_sock, _buffer, ev.data, 0, (struct sockaddr *)&m_addr, &m_len);            
            if (length_recv == -1)
                _FATAL_ERROR2("recvfrom() failed",errno);
            
//            assert(length_recv == ev.data);
            
            udpSocket->OnRecvFrom(_buffer, length_recv, (struct sockaddr *)&m_addr, m_len);              
        }
            break;
        case IO_TCP_SERVER:
        {
            TcpServerIO *tcpServer = (TcpServerIO *)base_io;
                                    
            fd = accept(tcpServer->_sock, (struct sockaddr *)&m_addr, &m_len);
            if (fd == -1)
                _FATAL_ERROR2("accept() failed",errno);
            
            _fast_new(session,_memory,_logger,fd,m_addr,m_len);
            
            EV_SET(&ev,fd,EVFILT_READ,EV_ADD|EV_ENABLE,0,0,session);
            if (kevent(_kq, &ev, 1, NULL, 0, NULL) == -1)
                _FATAL_ERROR2("kevent() failed while adding tcp session",errno);
            
            tcpServer->_sessions.insert(session);
            session->tcpServer = tcpServer;
            tcpServer->OnConnect(session);                                                                      
        }
            break;
        case IO_TCP_CLIENT:            
        {
            TcpClientIO *client = (TcpClientIO *)base_io;
            
            if (ev.flags & EV_EOF)
            {                
                client->OnDisconnect();
                close(client->_sock);
                client->_connected = false;
                return;
            }
                        
            if (ev.filter == EVFILT_WRITE)
            {
                EV_SET(&ev,client->_sock,EVFILT_WRITE,EV_DELETE,0,0,0);
                if (kevent(_kq,&ev,1,NULL,0,NULL) == -1)
                    _FATAL_ERROR2("kevent() failed while deleting tcp client write event",errno);
                
                EV_SET(&ev,client->_sock,EVFILT_READ,EV_ADD | EV_ENABLE,0,0,base_io);
                if (kevent(_kq,&ev,1,NULL,0,NULL) == -1)
                    _FATAL_ERROR2("kevent() failed while adding tcp client read event",errno);
                
                client->_connected = true;
                
                client->OnConnect();
                return;
            }
                        
            length_recv = recv(client->_sock, _buffer, ev.data, 0);
                
            if (length_recv == -1)
                _FATAL_ERROR2("recv() failed",errno);
                            
            assert(length_recv == ev.data);
            
            client->OnRecv(_buffer,length_recv);        
        }
            break;
        case IO_TCP_SESSION:
        {
            session = (TcpSession *)base_io;
            
            if (ev.flags & EV_EOF)
            {
                session->_connected = false;
                ((TcpServerIO *)session->tcpServer)->OnDisconnect(session);
                ((TcpServerIO *)session->tcpServer)->_sessions.erase(session);
                close(session->_sock);
                
                _fast_delete(session);
                return;
            }
            
            length_recv = recv(session->_sock, _buffer, ev.data, 0);
            
            if (length_recv == -1)
                _FATAL_ERROR2("recv() failed",errno);
                    
            ((TcpServerIO *)session->tcpServer)->OnRecv(session,_buffer,length_recv);
        }
            break;   
        case IO_TIMER:
        {
            TimerIO *timer = (TimerIO *)base_io;
                        
            timer->OnTimer();
        }
            break;
        default:
            _FATAL_ERROR("invalid io type");
    }
}

void Loop::ProcessRequest()
{
    char ch;
    BaseIO *base_io;
    BaseSocket *base_socket;
    TimerIO *timer_io;
    LoopIdleIO *idle_io;
    TcpClientIO *client_io;
    TimeTask *task;
    struct kevent ev;
    set<TimeTask *>::iterator it;
    
    read(read_pipe,&ch,1);
    
    switch (ch)
    {
        case 't':
            _scheduler->_lock.Lock();
            for (it = _scheduler->_tasks_set.begin(); it != _scheduler->_tasks_set.end(); ++it)
            {
                task = (*it);
                task->OnTimeTask();
            }
            _scheduler->_tasks_set.clear();
            _scheduler->_lock.UnLock();
        break;
        case 'x':
        return;
        break;
        case 'a':
            _add_mutex->Lock();
            base_io = _add_ios.front();
            _add_ios.pop();            
            _add_mutex->UnLock();
                
            switch(base_io->_ioType)
            {    
                case IO_TCP_CLIENT: 
                    base_socket = (BaseSocket*)base_io;
                    EV_SET(&ev,base_socket->_sock,EVFILT_WRITE,EV_ADD | EV_ENABLE,0,0,base_io);    
                    if (kevent(_kq,&ev,1,NULL,0,NULL) ==-1)
                        _FATAL_ERROR2("kevent() failed while adding tcp client",errno);
                    break;
                case IO_TCP_SERVER:
                case IO_TCP_SESSION:
                case IO_UDP_SOCKET:    
                    base_socket = (BaseSocket*)base_io;
                    EV_SET(&ev,base_socket->_sock,EVFILT_READ,EV_ADD | EV_ENABLE,0,0,base_io);
                    if (kevent(_kq,&ev,1,NULL,0,NULL) ==-1)
                        _FATAL_ERROR2("kevent() failed",errno);
                    break;    
                case IO_TIMER:                             
                    timer_io = (TimerIO*)base_io;    
                    EV_SET(&ev,timer_io->_timer,EVFILT_TIMER,EV_ADD | EV_ENABLE,0,timer_io->_timeout,base_io);
                    if (kevent(_kq,&ev,1,NULL,0,NULL) == -1)
                        _FATAL_ERROR2("kevent() failed",errno);
                    break;
                case IO_IDLE:
                    idle_io = (LoopIdleIO*)base_io;    
                    _idle_set.insert(idle_io);
                    break;
                default:
                        _FATAL_ERROR("invalid io type");
                }
                break;
            case 'd':
                _del_mutex->Lock();
                base_io = _del_ios.front();
                _del_ios.pop();
                _del_mutex->UnLock();
                
                switch (base_io->_ioType)
                {
                    case IO_TCP_CLIENT:
                        client_io = (TcpClientIO *)base_io;
                        if (client_io->_connected)                            
                            EV_SET(&ev,client_io->_sock,EVFILT_READ,EV_DELETE,0,0,0);
                        else
                            EV_SET(&ev,client_io->_sock,EVFILT_WRITE,EV_DELETE,0,0,0);
                        if (kevent(_kq,&ev,1,NULL,0,NULL) == -1 && errno != ENOENT)
                            _FATAL_ERROR2("kevent() failed while deleting tcp client",errno);
                        break;
                    case IO_TCP_SERVER:
                    case IO_TCP_SESSION:
                    case IO_UDP_SOCKET:
                        base_socket = (BaseSocket *)base_io;
                        EV_SET(&ev,base_socket->_sock,EVFILT_READ,EV_DELETE,0,0,0);
                        if (kevent(_kq,&ev,1,NULL,0,NULL) == -1 && errno != ENOENT)
                            _FATAL_ERROR2("kevent() failed",errno);
                    break;
                    case IO_TIMER:
                        timer_io = (TimerIO*)base_io;
                        EV_SET(&ev,timer_io->_timer,EVFILT_TIMER,EV_DELETE,0,0,0);
                        if (kevent(_kq,&ev,1,NULL,0,NULL) == -1 && errno != ENOENT)
                            _FATAL_ERROR2("kevent() failed for filter timer",errno);                        
                    break;
                    case IO_IDLE:
                        idle_io = (LoopIdleIO *)base_io;                         
                        _idle_set.erase(idle_io);
                    break;
                    default:
                        _FATAL_ERROR("invalid io type");
                }
                _ASSERT_ERROR(base_io->_sem->Post());
            break;
        }
}

bool Loop::Add(BaseIO *base_io)
{
    char ch = 'a';
    
    _add_mutex->Lock();
    _add_ios.push(base_io);
    _add_mutex->UnLock();
    
    CHECK_RETURN2(write(write_pipe, &ch, 1) != 1,"write() failed in Loop::Add()",errno);
    
    return true;
}


bool Loop::Delete(BaseIO *base_io)
{
    char ch = 'd';
    
    _del_mutex->Lock();
    _del_ios.push(base_io);
    _del_mutex->UnLock();
    
    CHECK_RETURN2(write(write_pipe, &ch, 1) != 1,"write() failed in Loop::Delete()",errno);
    
    return base_io->_sem->Wait();   
}

bool Loop::Add(TcpServerIO *server_io){return Add((BaseIO *)server_io);}
bool Loop::Add(TcpClientIO *client_io){return Add((BaseIO*)client_io);}
bool Loop::Add(UdpSocketIO *udp_io){return Add((BaseIO*)udp_io);}
bool Loop::Add(TimerIO *timer_io){return Add((BaseIO*)timer_io);}
bool Loop::Add(LoopIdleIO *idle_io){return Add((BaseIO*)idle_io);}
bool Loop::Delete(TcpServerIO *server_io){return Delete((BaseIO*)server_io);}
bool Loop::Delete(TcpClientIO *client_io){return Delete((BaseIO*)client_io);}
bool Loop::Delete(UdpSocketIO *udp_io){return Delete((BaseIO*)udp_io);}
bool Loop::Delete(TimerIO *timer_io){return Delete((BaseIO*)timer_io);}
bool Loop::Delete(LoopIdleIO *idle_io){return Delete((BaseIO*)idle_io);}