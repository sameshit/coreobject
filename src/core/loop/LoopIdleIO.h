#ifndef LOOPIDLEIO_H
#define LOOPIDLEIO_H

#include "../socket/BaseIO.h"

namespace CoreObjectLib
{
	class LoopIdleIO
    :public BaseIO
	{
	public:
		LoopIdleIO(Memory *memory, Logger *logger);
		virtual ~LoopIdleIO();

		Event<> OnLoopIdle;
	};
}

#endif //LOOPIDLEIO_H