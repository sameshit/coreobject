#include "../Loop.h"

using namespace std;
using namespace CoreObjectLib;


Loop::Loop(ThreadCache *thread_cache, Memory *memory, Logger *logger)
:_thread_cache(thread_cache), _memory(memory), _logger(logger),_started(false),
_scheduler(NULL),_thread(NULL)
{
	_main_io = NULL;
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		_FATAL_ERROR2("WSAStartup failed",WSAGetLastError());

	_fast_new(_mutex);
	_fast_new(_add_mutex);
	_fast_new(_del_mutex);

	memset(&_request_ovlp, 0, sizeof(OVERLAPPEDEX));
	_request_ovlp.hEvent = WSACreateEvent();
}

Loop::~Loop()
{
	if(_started)
		_ASSERT_ERROR(Stop());

	WSACleanup();

	_fast_delete(_add_mutex);
	_fast_delete(_del_mutex);
	_fast_delete(_mutex);

	WSACloseEvent(_request_ovlp.hEvent);
}

bool Loop::Start()
{
	_exit = false;

	_main_io = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, 0, 0);
	
	RETURN_MSG_IF_TRUE(_main_io == NULL,"Couldn't create iocp port");

	_thread = _thread_cache->GetThread();
	_thread->OnThread.Attach<Loop>(this,&Loop::Work);
	_thread->Wake();
	_started = true;

	_fast_new(_scheduler,_thread_cache,_memory,_logger,_main_io,&_request_ovlp);
	return true;
}

bool Loop::Stop()
{
	bool ret_val;

	_mutex->Lock();
	_exit = true;
	_mutex->UnLock();

	_fast_delete(_scheduler);
	ret_val = CloseHandle(_main_io) == TRUE;
	_thread_cache->ReleaseThread(&_thread);

	_main_io = NULL;	

	_started = false;

	return ret_val;
}

void Loop::Work()
{
	OVERLAPPEDEX *lpOverlapped;
	ULONG_PTR key;
	DWORD dwBytes;
	bool exit;
	BaseIO *base_io;
	TcpSession *session;
	TcpServerIO *server;
	set<LoopIdleIO *>::iterator it;
	int s_local, s_remote;
	TcpClientIO *client_io;
	UdpSocketIO	*udpsocket_io;
	TimerIO		*timer_io;

	_mutex->Lock();
	exit = _exit;
	_mutex->UnLock();

	if(exit)
		return;

	if(!GetQueuedCompletionStatus(_main_io, &dwBytes, &key, (LPOVERLAPPED *)&lpOverlapped, INFINITE))
		return;

	if (lpOverlapped == &_request_ovlp)
	{
		ProcessRequest();
		return;
	}
	
	for(it = _idle_set.begin(); it != _idle_set.end(); it++)
		(*it)->OnLoopIdle();

	base_io = (BaseIO *)lpOverlapped->pData;

	switch(base_io->_ioType)
	{
	case IO_TCP_SERVER:
	{
		server = (TcpServerIO *)lpOverlapped->pData;	
	}
	break;
	case IO_TCP_SESSION:
		{
			session = (TcpSession *)lpOverlapped->pData;
			server = ((TcpServerIO *)session->_wsaOverlapped.lpServer);

			if(dwBytes == 0)
			{
				if(session->_connected)
				{
					session->_connected = false;
					server->OnDisconnect(session);
					server->_sessions.erase(session);
					long_delete(_memory,session);
					return;
				}
				session->_connected = true;
				session->_hCompletionPort = CreateIoCompletionPort((HANDLE)session->_sock,_main_io, 0, 0);

				sockaddr * local = 0, * remote = 0;

				session->_ex_lpfnGetAcceptExSockAddrs(&session->_ex_buffer, 0, 
					sizeof(SOCKADDR_IN) + 16, sizeof(SOCKADDR_IN) + 16, 
					&local, &s_local, &remote, &s_remote);

				sockaddr_in *in_local = (sockaddr_in *)local;
				sockaddr_in *in_remote = (sockaddr_in *)remote;

				memcpy(&session->_addr,local,sizeof(sockaddr_in));
				server->OnConnect(session);
				
				_fast_new(session, _memory, _logger);
				session->_wsaOverlapped.lpServer = (void *)server;

				if(server->_ex_lpfnAcceptEx(server->_sock, session->_sock, &session->_ex_buffer, 0, 
					sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, &session->_dwBytes, &session->_wsaOverlapped) == TRUE 
					|| WSAGetLastError() != WSA_IO_PENDING)
					_FATAL_ERROR2("AcceptEx failed",WSAGetLastError());

				server->_sessions.insert(session);
			}
			else
			{
				server->OnRecv(session, (uint8_t *)session->_wsaBuf.buf, (int &)dwBytes);	
				WSARecv(session->_sock, &session->_wsaBuf, 1, &dwBytes,	&session->_socketFlags, &session->_wsaOverlapped, 0);
			}
			break;
		}
	case IO_TCP_CLIENT:
		{
			client_io = (TcpClientIO *)lpOverlapped->pData;

			if(dwBytes == 0)
			{
				if(client_io->_connected)
				{
					client_io->_connected = false;
					client_io->OnDisconnect();
					return;
				}
				else
				{
					client_io->_connected = true;
					client_io->OnConnect();
				}
			}
			else
			{
				client_io->OnRecv((uint8_t *)client_io->_wsaBuf.buf, (int &)dwBytes);
				WSARecv(client_io->_sock, &client_io->_wsaBuf, 1, &dwBytes, 
					&client_io->_socketFlags, &client_io->_wsaOverlapped, 0);	
			}
			break;
		}
	case IO_UDP_SOCKET:
		{
			udpsocket_io = (UdpSocketIO *)lpOverlapped->pData;
			
			udpsocket_io->OnRecvFrom((uint8_t *)udpsocket_io->_wsaBuf.buf, (int &)dwBytes, 
				(sockaddr *)&udpsocket_io->_addr, udpsocket_io->_len);

			WSARecvFrom(udpsocket_io->_sock, &udpsocket_io->_wsaBuf, 1, &dwBytes, &udpsocket_io->_socketFlags, 
				(sockaddr *)&udpsocket_io->_addr, &udpsocket_io->_len, &udpsocket_io->_wsaOverlapped, 0);
			break;
		}
	case IO_TIMER:
		{
			timer_io = (TimerIO *)lpOverlapped->pData;
			timer_io->OnTimer();
			break;
		}
	break;
	default:
		_FATAL_ERROR("Invalid io type");
		break;
	}
}

void Loop::ProcessRequest()
{
	BaseIO			*base_io;
	TcpServerIO		*server;
	TcpSession		*session;
	TcpClientIO		*client;
	UdpSocketIO		*udp;
	TimerIO			*timer;
	LoopIdleIO		*idle;
	BaseSocket		*base_socket;
	set<TcpSession *>::iterator it;

	while (_add_ios.size() > 0)
	{
		_add_mutex->Lock();
		base_io = _add_ios.front();
		_add_ios.pop();
		_add_mutex->UnLock();


		switch (base_io->_ioType)
		{
			case IO_TCP_SERVER:
				server = (TcpServerIO *)base_io;
				server->_hCompletionPort = CreateIoCompletionPort((HANDLE)server->_sock,_main_io, 0, 0);
				_fast_new(session, _memory, _logger);
				session->_wsaOverlapped.lpServer = (void *)server;

				if(server->_ex_lpfnAcceptEx(server->_sock, session->_sock, &session->_ex_buffer, 0, 
					sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, &session->_dwBytes, &session->_wsaOverlapped) == TRUE || WSAGetLastError() != WSA_IO_PENDING)
					_FATAL_ERROR2("AcceptEx failed", WSAGetLastError());

				server->_sessions.insert(session);
			break;
			case IO_TCP_CLIENT:
				client = (TcpClientIO*)base_io;
				client->_hCompletionPort = CreateIoCompletionPort((HANDLE)client->_sock, 
					_main_io, 0, 0);
			break;
			case IO_UDP_SOCKET:
				udp = (UdpSocketIO *)base_io;
				udp->_hCompletionPort = CreateIoCompletionPort((HANDLE)udp->_sock, 
				_main_io, 0, 0);
	
				WSARecvFrom(udp->_sock, &udp->_wsaBuf, 1, &udp->_dwBytes, &udp->_socketFlags, 
					(sockaddr *)&udp->_addr, &udp->_len, &udp->_wsaOverlapped, 0);
			break;
			case IO_TIMER:
				timer = (TimerIO*)base_io;
				_ASSERT_ERROR(_scheduler->Add(timer));
			break;
			case IO_IDLE:
				idle = (LoopIdleIO *)base_io;
				_idle_set.insert(idle);
			break;
			default:
				_FATAL_ERROR("Invalid io type");
			break;
		}
	}

	while (_del_ios.size() > 0)
	{
		_del_mutex->Lock();
		base_io =_del_ios.front();
		_del_ios.pop();
		_del_mutex->UnLock();

		switch (base_io->_ioType)
		{
			case IO_TCP_SERVER:
				server = (TcpServerIO*)base_io;
				for (it = server->_sessions.begin(); it != server->_sessions.end(); it ++)
				{
					session = (*it);
					if((*it)->_connected && !(*it)->_ex_lpfnDisconnectEx((*it)->_sock, 0, 0, 0))
						_FATAL_ERROR2("DisconnectEx failed",WSAGetLastError());
					closesocket((*it)->_sock);
					(*it)->_sock = INVALID_SOCKET;

					_fast_delete(session);
				}
			break;
			case IO_UDP_SOCKET:
			case IO_TCP_CLIENT:	
				base_socket = (BaseSocket*)base_io;
				if(closesocket(base_socket->_sock) != 0)
					_FATAL_ERROR2("closesocket() failed",WSAGetLastError());
				base_socket->_sock = INVALID_SOCKET;
			break;
			case IO_TIMER:
				timer = (TimerIO*)base_io;
				_scheduler->Delete(timer);
			break;
			case IO_IDLE:
				idle = (LoopIdleIO*)base_io;
				_idle_set.erase(idle);
			break;
			default:
				_FATAL_ERROR("Invalid io type");
		}

		_ASSERT_ERROR(base_io->_sem->Post());
	}

	_scheduler->_lock.Lock();
	for (auto task_it = _scheduler->_tasks_set.begin();
		task_it != _scheduler->_tasks_set.end();
		++task_it)
		(*task_it)->OnTimeTask();
	_scheduler->_tasks_set.clear();
	_scheduler->_lock.UnLock();
}

bool Loop::Add(BaseIO *base_io)
{
	_add_mutex->Lock();
	_add_ios.push((BaseIO*)base_io);
	_add_mutex->UnLock();

	RETURN_MSG_AND_CODE_IF_TRUE(!PostQueuedCompletionStatus(_main_io, 0, 0, (WSAOVERLAPPED*)&_request_ovlp),"Coulndn't post to iocp",WSAGetLastError());

	return true;
}

bool Loop::Delete(BaseIO *base_io)
{	_del_mutex->Lock();
	_del_ios.push((BaseIO*)base_io);
	_del_mutex->UnLock();

	RETURN_MSG_AND_CODE_IF_TRUE(!PostQueuedCompletionStatus(_main_io, 0, 0, (WSAOVERLAPPED*)&_request_ovlp),"Couldn't post to iocp",WSAGetLastError());

	return base_io->_sem->Wait();
}


bool Loop::Add(TcpServerIO *server){return Add((BaseIO*)server);}
bool Loop::Add(TcpClientIO *tcpClient){return Add((BaseIO*)tcpClient);}
bool Loop::Add(UdpSocketIO *udpSocket){return Add((BaseIO*)udpSocket);}
bool Loop::Add(TimerIO * timer){return Add((BaseIO*)timer);}
bool Loop::Add(LoopIdleIO *idle){return Add((BaseIO*)idle);}
bool Loop::Delete(TcpServerIO *tcpServer){return Delete((BaseIO*)tcpServer);}
bool Loop::Delete(TcpClientIO *tcpClient){return Delete((BaseIO*)tcpClient);}
bool Loop::Delete(UdpSocketIO *udpSocket){return Delete((BaseIO*)udpSocket);}
bool Loop::Delete(TimerIO *timer){return Delete((BaseIO*)timer);}
bool Loop::Delete(LoopIdleIO * idle){return Delete((BaseIO*)idle);}