#include "../Scheduler.h"

using namespace CoreObjectLib;
using namespace std;

Scheduler::Scheduler(ThreadCache *thread_cache, Memory *memory, Logger *logger,HANDLE main_io,OVERLAPPEDEX *request_ovlp)
:_thread_cache(thread_cache),_memory(memory),_logger(logger),_main_io(main_io),_exit(false),_request_ovlp(request_ovlp)

{
	_thread = _thread_cache->GetThread();
	_thread->OnThread.Attach(this,&Scheduler::SchedulerThread);
	_thread->Wake();
}

Scheduler::~Scheduler()
{
	_lock.Lock();
	_exit = true;
	_lock.UnLock();
	_sem.Post();

	_thread_cache->ReleaseThread(&_thread);
}

void Scheduler::SchedulerThread()
{
	bool exit;
	ssize_t size;
	uint64_t timer_wait_time,task_wait_time;
	uint32_t wait_time;
	TimerIO *timer;
	TimeTask *timetask;
		
	_lock.Lock();
	size = _timers_by_timeout.size() + _tasks_by_timeout.size();
	exit = _exit;
	_lock.UnLock();

	if (exit)
		return;
					
	if (size == 0)
		_sem.Wait();
	else
	{	
		queue<TimerIO *> timer_delete_queue;
		queue<TimeTask *> task_delete_queue;
		COTime now;

		_lock.Lock();

		multimap<COTime, TimerIO *>::iterator timer_it;
		for (timer_it = _timers_by_timeout.begin(); timer_it != _timers_by_timeout.end(); timer_it++)
		{
			timer = (*timer_it).second;
			if (now >= timer->_next_timeout)
			{
				if(!PostQueuedCompletionStatus(_main_io, 0, 0, &timer->_wsaOverlapped))
					_FATAL_ERROR2("Post to iocp failed",WSAGetLastError());
				timer_delete_queue.push(timer);
			}
			else 
				break;
		}			


		while(timer_delete_queue.size() > 0)
		{
			timer = timer_delete_queue.front();
			_timers_by_timeout.erase(timer->_next_timeout);
			timer->_next_timeout += timer->_timeout;
			_timers_by_timeout.insert(make_pair(timer->_next_timeout, timer));

			timer_delete_queue.pop();
		}


		multimap<COTime, TimeTask *>::iterator task_it;
		for (task_it = _tasks_by_timeout.begin(); task_it != _tasks_by_timeout.end(); task_it++)
		{
			timetask = (*task_it).second;
			if (now >= timetask->_next_timeout)
			{
				_tasks_set.insert(timetask);
				task_delete_queue.push(timetask);
			}
			else 
				break;
		}			

		if (!_tasks_set.empty())
			if(!PostQueuedCompletionStatus(_main_io, 0, 0, _request_ovlp))
				_FATAL_ERROR2("Post to iocp failed",WSAGetLastError());

		while(task_delete_queue.size() > 0)
		{
			timetask = task_delete_queue.front();
			_tasks_by_timeout.erase(timetask->_next_timeout);
			task_delete_queue.pop();
		}
		
		if (_timers_by_timeout.empty() && _tasks_by_timeout.empty())
		{
			_lock.UnLock();
			_sem.Wait();
		}
		else
		{
			timer_wait_time = _timers_by_timeout.empty()?MAXUINT64:(*_timers_by_timeout.begin()).first();
			task_wait_time = _tasks_by_timeout.empty()?MAXUINT64:(*_tasks_by_timeout.begin()).first();
			_lock.UnLock();
			wait_time = (uint32_t)((timer_wait_time<task_wait_time?timer_wait_time:task_wait_time)-now());
			_sem.Wait(wait_time);
		}
	}
}

bool Scheduler::Add(TimerIO *timer)
{
	_lock.Lock();
	_timers_by_timeout.insert(make_pair(timer->_next_timeout, timer));
	_lock.UnLock();

	return _sem.Post();
}

bool Scheduler::Delete(TimerIO *timer)
{
	multimap<COTime, TimerIO *>::iterator it;

	_lock.Lock();

	it = _timers_by_timeout.find(timer->_next_timeout);
	while (it != _timers_by_timeout.end() && (*it).second != timer)
		it++;

	if(it == _timers_by_timeout.end())
	{
		_lock.UnLock();
		COErr::Set("No such timer");
		return false;
	}
	
	_timers_by_timeout.erase(it);

	_lock.UnLock();

	return true;
}

bool Scheduler::Add(TimeTask *task)
{  
    task->_next_timeout.Update();
    task->_next_timeout+=task->_timeout;
    
	_lock.Lock();
	_tasks_by_timeout.insert(make_pair(task->_next_timeout, task));
	_lock.UnLock();
    
	return _sem.Post();
}

bool Scheduler::Delete(TimeTask *task)
{
	multimap<COTime, TimeTask *>::iterator it;
    
	_lock.Lock();
    
    _tasks_set.erase(task);
    
	it = _tasks_by_timeout.find(task->_next_timeout);
	while (it != _tasks_by_timeout.end() && (*it).second != task)
		it++;
    
	if(it == _tasks_by_timeout.end())
	{
		_lock.UnLock();
		COErr::Set("No such timetask");
		return false;
	}
	
	_tasks_by_timeout.erase(it);
    
	_lock.UnLock();
    
	return true;
}