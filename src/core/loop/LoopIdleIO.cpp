#include "LoopIdleIO.h"
using namespace CoreObjectLib;

LoopIdleIO::LoopIdleIO(Memory *memory,Logger *logger)
:BaseIO(memory,logger,IO_IDLE)
{
}

LoopIdleIO::~LoopIdleIO()
{
}