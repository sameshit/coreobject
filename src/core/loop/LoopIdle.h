#ifndef LOOPIDLE_H
#define LOOPIDLE_H

#include "LoopIdleIO.h"
#include "../../CoreObject.h"

namespace CoreObjectLib
{
	class LoopIdle : public LoopIdleIO
	{
    protected:
		CoreObject *_core;
	public:
		LoopIdle(CoreObject *core);
		~LoopIdle();
	};
}

#endif //LOOPIDLE_H
