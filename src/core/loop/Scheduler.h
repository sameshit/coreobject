#ifndef CoreObject_Scheduler_h
#define CoreObject_Scheduler_h

#include "../thread/ThreadCache.h"
#include "../thread/SpinLock.h"
#include "../thread/Semaphore.h"
#include "../logger/Logger.h"
#include "../time/TimeTask.h"
#include "../time/COTime.h"
#include "../time/TimerIO.h"

namespace CoreObjectLib
{
    
class LIBEXPORT Scheduler
{
public:
#if defined(OS_X) || defined(OS_LINUX)
    Scheduler(ThreadCache *thread_cache,Logger *logger,int fd);
#elif defined(OS_WIN)
	Scheduler(ThreadCache *thread_cache,Memory *memory,Logger *logger,HANDLE main_io,OVERLAPPEDEX *request_ovlp);
#endif
    virtual ~Scheduler();
    
    bool Add(TimeTask *task);
    bool Delete(TimeTask *task);
private:
	Memory *_memory;
    int                                 _fd;
    ThreadCache                         *_thread_cache;
    SpinLock                            _lock;
    Semaphore                           _sem;
    Thread                              *_thread;
    bool                                _exit;
    std::multimap<COTime, TimeTask *>   _tasks_by_timeout;
    Logger                              *_logger;
    std::set<TimeTask *>                _tasks_set;
    
    friend class Loop;
    void SchedulerThread();
#if defined(OS_WIN)
	HANDLE _main_io;
	OVERLAPPEDEX *_request_ovlp;
	std::multimap<COTime, TimerIO *>	_timers_by_timeout;
	bool		Add		(TimerIO *timer_io);
	bool		Delete	(TimerIO *timer_io);
#endif
};

}



#endif
