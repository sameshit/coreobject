#include "../Scheduler.h"


using namespace CoreObjectLib;
using namespace std;

Scheduler::Scheduler(ThreadCache *thread_cache,Logger *logger,int fd)
:_thread_cache(thread_cache),_logger(logger),_fd(fd),_exit(false)
{
    _thread = _thread_cache->GetThread();
    _thread->OnThread.Attach<Scheduler>(this,&Scheduler::SchedulerThread);
    _thread->Wake();
}

Scheduler::~Scheduler()
{
    _lock.Lock();
    _exit = true;
    _lock.UnLock();
    
    _sem.Post();
    
    _thread_cache->ReleaseThread(&_thread);
}

void Scheduler::SchedulerThread()
{
	bool exit;
	ssize_t size;
	uint64_t wait_time; 
	TimeTask *task;
    char ch = 't';
    
	_lock.Lock();
	size = _tasks_by_timeout.size();
	exit = _exit;
	_lock.UnLock();
    
	if (exit)
		return;
    
	if (size == 0)
		_sem.Wait();
	else
	{	
		queue<TimeTask *> delete_queue;
		COTime now;
        
		_lock.Lock();
        
		multimap<COTime, TimeTask *>::iterator it;
		for (it = _tasks_by_timeout.begin(); it != _tasks_by_timeout.end(); it++)
		{
			task = (*it).second;
			if (now >= task->_next_timeout)
			{
                if(write(_fd, &ch, 1) != 1)
                    _FATAL_ERROR2("Couldn't write to pipe",errno);
				delete_queue.push((*it).second);
                _tasks_set.insert((*it).second);
			}
			else 
				break;
		}				
		while(delete_queue.size() > 0)
		{
			task = delete_queue.front();
			_tasks_by_timeout.erase(task->_next_timeout);
			delete_queue.pop();
		}
        
        if (_tasks_by_timeout.empty())
        {
            _lock.UnLock();
            _sem.Wait();            
        }
        else 
        {
            it = _tasks_by_timeout.begin();
            task = (*it).second;
            if (task->_next_timeout > now)
                wait_time = task->_next_timeout - now;
            else
                wait_time = 0;
            
            _lock.UnLock();
            _sem.Wait((uint32_t)wait_time);
        }
	}
}

bool Scheduler::Add(TimeTask *task)
{
    
    task->_next_timeout.Update();
    task->_next_timeout+=task->_timeout;
    
	_lock.Lock();
	_tasks_by_timeout.insert(make_pair(task->_next_timeout, task));
	_lock.UnLock();
    
	return _sem.Post();
}

bool Scheduler::Delete(TimeTask *task)
{
	multimap<COTime, TimeTask *>::iterator it;
    
	_lock.Lock();
    
    _tasks_set.erase(task);
    
	it = _tasks_by_timeout.find(task->_next_timeout);
	while (it != _tasks_by_timeout.end() && (*it).second != task)
		it++;
    
	if(it == _tasks_by_timeout.end())
	{
		_lock.UnLock();
		COErr::Set("No such timer");
		return false;
	}
	
	_tasks_by_timeout.erase(it);
    
	_lock.UnLock();
    
	return true;
}

