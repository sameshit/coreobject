#include <sys/epoll.h>

#include "../Loop.h"

using namespace std;
using namespace CoreObjectLib;

Loop::Loop(ThreadCache *thread_cache, Memory *memory, Logger *logger)
:_thread_cache(thread_cache), _memory(memory), _logger(logger),_scheduler(NULL)
{
    _fast_new(_mutex);
    _fast_new(_add_mutex);
    _fast_new(_del_mutex);

    _exit = false;
    _epoll = 0;

    _thread = NULL;
}

Loop::~Loop()
{
    if (_epoll > 0)
        close(_epoll);

	if (_thread != NULL)
		_thread_cache->ReleaseThread(&_thread);

    _fast_delete(_add_mutex);
    _fast_delete(_del_mutex);
    _fast_delete(_mutex);
}

bool Loop::Start()
{
    _epoll = epoll_create(1024);
    pipe(_pipes);

    read_pipe = _pipes[0];
    write_pipe = _pipes[1];

    int flags = fcntl(read_pipe, F_GETFL, 0);
    fcntl(write_pipe, F_SETFL, flags|O_NONBLOCK);

    epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = read_pipe;
    epoll_ctl(_epoll, EPOLL_CTL_ADD, read_pipe, &ev);

    RETURN_MSG_IF_TRUE(_epoll == -1,"epoll_ctl() failed with error: "<<errno);

   _fast_new(_scheduler, _thread_cache, _logger, write_pipe);
    _thread = _thread_cache->GetThread();

    _thread->OnThread.Attach<Loop>(this,&Loop::Work);

    RETURN_IF_TRUE(!_thread->Wake());

    return true;
}

bool Loop::Stop()
{
    _mutex->Lock();
    _exit = true;
    _mutex->UnLock();

    if (_scheduler != NULL)
    {
        _fast_delete(_scheduler);
        _scheduler = NULL;
    }

    char ch = 'x';
    write(write_pipe, &ch, 1);

    _thread_cache->ReleaseThread(&_thread);

    return true;
}

void Loop::Work()
{
    struct epoll_event 		ev;
    bool 			my_exit;
    set<LoopIdleIO *>::iterator it;
    BaseIO 			*base_io;
    sockaddr_in 		m_addr;
    socklen_t   		m_len = sizeof(sockaddr_in);
    UdpSocketIO 		*udp_io;
    ssize_t 			bytes_recvd;
    TcpServerIO 		*tcpserver_io;
    int 			fd;
    TcpSession 			*session;
    TcpClientIO			*tcpclient_io;
    uint64_t			exp;
    TimerIO			*timer_io;
    int             err;


    _mutex->Lock();
    my_exit = _exit;
    _mutex->UnLock();

    if(my_exit)
        return;

    do
    {
        err = epoll_wait(_epoll, &ev, 1, -1);
    }
    while (err == -1 && errno == EINTR);

    if(err == -1)
      _FATAL_ERROR2("epoll_wait() error",errno);

    for(it = _idle_set.begin(); it != _idle_set.end(); it++)
       (*it)->OnLoopIdle();


    if(ev.data.fd == read_pipe)
    {
	ProcessRequest();
	return;
    }

    base_io = (BaseIO* )ev.data.ptr;

    switch(base_io->_ioType)
    {
        case IO_UDP_SOCKET:
        {
            udp_io = (UdpSocketIO *)ev.data.ptr;

	    bytes_recvd = recvfrom(udp_io->_sock,_buffer,MAX_BUFFER_SIZE,0,(struct sockaddr*)&m_addr,&m_len);
	    if (bytes_recvd == -1)
	      _FATAL_ERROR2("recvfrom() failed",errno);

            udp_io->OnRecvFrom(_buffer,bytes_recvd,(struct sockaddr*)&m_addr,m_len);

            break;
        }
        case IO_TCP_SERVER:
        {
            tcpserver_io = (TcpServerIO *)ev.data.ptr;

	    fd = accept(tcpserver_io->_sock,(struct sockaddr*)&m_addr,&m_len);
	    if (fd == -1)
	      _FATAL_ERROR2("accept() failed",errno);

	    _fast_new(session, _memory, _logger, fd, m_addr, m_len);
	    tcpserver_io->_sessions.insert(session);

	    session->_ev.events = EPOLLIN;
	    session->_ev.data.fd = session->_sock;
	    session->_ev.data.ptr = session;
	    session->tcpServer = tcpserver_io;
	    session->_connected = true;

	    if(epoll_ctl(_epoll, EPOLL_CTL_ADD, session->_sock, &session->_ev) == -1)
	      _FATAL_ERROR2("epoll_ctl() failed",errno);

            tcpserver_io->OnConnect(session);
            break;
        }
        case IO_TCP_CLIENT:
        {
            tcpclient_io = (TcpClientIO *)ev.data.ptr;

            if (ev.events & EPOLLOUT)
	    {
                tcpclient_io->_ev.events = EPOLLIN;
                if(epoll_ctl(_epoll,EPOLL_CTL_MOD,tcpclient_io->_sock,&tcpclient_io->_ev) == -1)
                    _FATAL_ERROR2("epoll_ctl() failed while adding tcp client",errno);
                tcpclient_io->OnConnect();
                break;
            }

	    bytes_recvd = recv(tcpclient_io->_sock,_buffer,MAX_BUFFER_SIZE,0);

	    if (bytes_recvd == -1)
	      _FATAL_ERROR2("recv() failed in tcp client",errno);

	    if (bytes_recvd == 0)
	    {
		if (epoll_ctl(_epoll,EPOLL_CTL_DEL,tcpclient_io->_sock,0) == -1)
		  _FATAL_ERROR2("epoll_ctl() failed while deleting tcpclient",errno);

		tcpclient_io->_connected = false;
		tcpclient_io->OnDisconnect();
		break;
	    }

            tcpclient_io->OnRecv(_buffer, bytes_recvd);
            break;
        }
        case IO_TCP_SESSION:
        {
            session 	 = (TcpSession *)ev.data.ptr;
	    tcpserver_io = (TcpServerIO*)session->tcpServer;

	    bytes_recvd = recv(session->_sock,_buffer,MAX_BUFFER_SIZE,0);

	    if (bytes_recvd == -1)
	      _FATAL_ERROR2("recv() failed in tcp session",errno);

            if(bytes_recvd == 0)
            {
                session->_connected = false;
                tcpserver_io->OnDisconnect(session);
                tcpserver_io->_sessions.erase(session);
                if((epoll_ctl(_epoll, EPOLL_CTL_DEL, session->_sock, 0) == -1))
                    _FATAL_ERROR2("epoll_ctl() failed while deleting tcp session",errno);
                _fast_delete(session);
                break;
            }
            break;
        }
        case IO_TIMER:
        {
            timer_io = (TimerIO *)ev.data.ptr;

            bytes_recvd = read(timer_io->_timer, &exp, sizeof(uint64_t));

            if(bytes_recvd != sizeof(uint64_t))
                _FATAL_ERROR2("read() failed in timer io",errno);

            timer_io->OnTimer();
            break;
        }
        default:
	    _FATAL_ERROR("Invalid io type");
        break;
        }
}

void Loop::ProcessRequest()
{
    char ch;
    ssize_t bytes_recvd;
    BaseIO *base_io;
    BaseSocket *base_socket;
    TimerIO *timer_io;
    TimeTask *task;
    set<TimeTask *>::iterator it;

    bytes_recvd = read(read_pipe,&ch,1);
    if (bytes_recvd != 1)
      _FATAL_ERROR2("read() failed while reading from pipe",errno);

    switch (ch)
    {
      case 't':
            _scheduler->_lock.Lock();
            for (it = _scheduler->_tasks_set.begin(); it != _scheduler->_tasks_set.end(); it++)
            {
                task = (*it);
                task->OnTimeTask();
            }
            _scheduler->_tasks_set.clear();
            _scheduler->_lock.UnLock();
      break;
      case 'q':
      break;
      case 'a':
      {
        _add_mutex->Lock();
        base_io = _add_ios.front();
        _add_ios.pop();
        _add_mutex->UnLock();

        switch(base_io->_ioType)
        {
        case IO_TCP_CLIENT:
            base_socket = (BaseSocket *)base_io;
            base_socket->_ev.events   = EPOLLOUT;
            base_socket->_ev.data.fd  = base_socket->_sock;
            base_socket->_ev.data.ptr = base_io;

            if(epoll_ctl(_epoll,EPOLL_CTL_ADD,base_socket->_sock,&base_socket->_ev) == -1)
                _FATAL_ERROR2("epoll_ctl() failed while adding tcp client",errno);
        break;
        case IO_TCP_SERVER:
        case IO_TCP_SESSION:
        case IO_UDP_SOCKET:
	      base_socket = (BaseSocket *)base_io;
	      base_socket->_ev.events   = EPOLLIN;
	      base_socket->_ev.data.fd  = base_socket->_sock;
	      base_socket->_ev.data.ptr = base_io;

	      if(epoll_ctl(_epoll,EPOLL_CTL_ADD,base_socket->_sock,&base_socket->_ev) == -1)
            _FATAL_ERROR2("epoll_ctl() failed",errno);
        break;
        case IO_TIMER:
	      timer_io	= (TimerIO*)base_io;
	      timer_io->_ev.events   = EPOLLIN;
	      timer_io->_ev.data.fd  = timer_io->_timer;
	      timer_io->_ev.data.ptr = base_io;

	      if(epoll_ctl(_epoll,EPOLL_CTL_ADD,timer_io->_timer,&timer_io->_ev) == -1)
            _FATAL_ERROR2("epoll_ctl() failed while adding timer",errno);
        break;
        case IO_IDLE:
	      _idle_set.insert((LoopIdleIO*)base_io);
        break;
        default:
            _FATAL_ERROR("invalid io type");
        break;
        }
      }
      break;
      case 'd':
      {
        _del_mutex->Lock();
        base_io = _del_ios.front();
        _del_ios.pop();
        _del_mutex->UnLock();

        switch(base_io->_ioType)
        {
          case IO_TCP_CLIENT:
          case IO_TCP_SERVER:
          case IO_TCP_SESSION:
          case IO_UDP_SOCKET:
              base_socket = (BaseSocket *)base_io;

              if(epoll_ctl(_epoll,EPOLL_CTL_DEL,base_socket->_sock,0) == -1)
              _FATAL_ERROR2("epoll_ctl() failed",errno);
          break;
          case IO_TIMER:
              timer_io = (TimerIO *)base_io;

              if (epoll_ctl(_epoll,EPOLL_CTL_DEL,timer_io->_timer,0) == - 1)
              _FATAL_ERROR2("epoll_ctl() failed",errno);
          break;
          case IO_IDLE:
              _idle_set.erase((LoopIdleIO*)base_io);
          break;
          default:
            _FATAL_ERROR("invalid io type");
          break;
        }
          _ASSERT_ERROR(base_io->_sem->Post());
      }
          break;
    }
}

bool Loop::Add(BaseIO *base_io)
{
    char ch = 'a';

    _add_mutex->Lock();
    _add_ios.push(base_io);
    _add_mutex->UnLock();

    RETURN_MSG_IF_TRUE(write(write_pipe,&ch,1) == -1,"write() failed. Errno: "<<errno);
    return true;
}

bool Loop::Delete(BaseIO *base_io)
{
    char ch = 'd';

    _del_mutex->Lock();
    _del_ios.push(base_io);
    _del_mutex->UnLock();

    RETURN_MSG_IF_TRUE(write(write_pipe,&ch,1) == -1,"write() failed. Errno: "<<errno);

    return base_io->_sem->Wait();
}

bool Loop::Add(TcpServerIO *tcpserver_io){return Add((BaseIO*)tcpserver_io);}
bool Loop::Add(TcpClientIO *tcpclient_io){return Add((BaseIO*)tcpclient_io);}
bool Loop::Add(UdpSocketIO *udpsocket_io){return Add((BaseIO*)udpsocket_io);}
bool Loop::Add(TimerIO *timer_io){return Add((BaseIO*)timer_io);}
bool Loop::Add(LoopIdleIO *idle_io){return Add((BaseIO*)idle_io);}
bool Loop::Delete(TcpServerIO *tcpserver_io){return Delete((BaseIO*)tcpserver_io);}
bool Loop::Delete(TcpClientIO *tcpclient_io){return Delete((BaseIO*)tcpclient_io);}
bool Loop::Delete(UdpSocketIO *udpsocket_io){return Delete((BaseIO*)udpsocket_io);}
bool Loop::Delete(TimerIO *timer_io){return Delete((BaseIO*)timer_io);}
bool Loop::Delete(LoopIdleIO * idle_io){return Delete((BaseIO*)idle_io);}
