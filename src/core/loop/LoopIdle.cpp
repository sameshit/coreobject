#include "LoopIdle.h"
using namespace CoreObjectLib;

LoopIdle::LoopIdle(CoreObject *core) : LoopIdleIO(core->GetMemory(),core->GetLogger()),_core(core)
{
	_core->GetLoop()->Add(this);
}

LoopIdle::~LoopIdle()
{
	_core->GetLoop()->Delete(this);
}