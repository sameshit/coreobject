#ifndef LOOP__H
#define LOOP__H

#include "../../common/config.h"
#include "../protocols/TCP/TcpServerIO.h"
#include "../protocols/TCP/TcpClientIO.h"
#include "../protocols/TCP/TcpSession.h"
#include "../protocols/UDP/UdpSocketIO.h"
#include "../time/TimerIO.h"
#include "../memory/Memory.h"
#include "LoopIdleIO.h"
#include "../thread/ThreadCache.h"
#include "Scheduler.h"

#define MAX_BUFFER_SIZE 65535

namespace CoreObjectLib
{
	class Loop
	{
	public:
		Loop(ThreadCache *thread_cache, Memory *memory, Logger *logger);
		virtual ~Loop();

		bool Start();
		bool Stop();

		bool Add(TcpServerIO *);
		bool Add(TcpClientIO *);
		bool Add(UdpSocketIO *);
		bool Add(TimerIO *);
		bool Add(LoopIdleIO *);

		bool Delete(TcpServerIO *);
		bool Delete(TcpClientIO *);
		bool Delete(UdpSocketIO *);
		bool Delete(TimerIO *);
		bool Delete(LoopIdleIO *);

        bool IsLoopThread() {return COErr::GetThreadId() == _thread->GetThreadId();}
	private:
        friend class CoreObject;

        Scheduler *_scheduler;
		ThreadCache *_thread_cache;
		Thread *_thread;
		std::set<LoopIdleIO *> _idle_set;
		bool _exit;
		bool _started;
		SpinLock *_mutex;
		Logger *_logger;
		Memory *_memory;
		std::queue<BaseIO*> _add_ios;
		std::queue<BaseIO*> _del_ios;
		SpinLock *_add_mutex;
		SpinLock *_del_mutex;

		void		Work();
		bool		Add(BaseIO *base_io);
		bool		Delete(BaseIO* base_io);
		void		ProcessRequest();
	#ifdef OS_WIN
		HANDLE _main_io;
		OVERLAPPEDEX _request_ovlp;
	#elif defined (OS_LINUX)
		int _epoll;
	#elif defined (OS_X) || defined (OS_IOS)
		int _kq;
	#else
		IMPLEMENT ME
	#endif
	#if defined (OS_X) || defined (OS_LINUX) || defined (OS_IOS)
		uint8_t _buffer [MAX_BUFFER_SIZE];
		int _pipes[2];
		int read_pipe, write_pipe;
	#endif
	};
}

#endif //LOOP__H
