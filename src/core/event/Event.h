#ifndef EVENT___H
#define EVENT___H

// event handlers base

namespace CoreObjectLib
{

template <typename ... Args>
class EventHandlerBase
{
public:
    virtual void notify(Args ... args) = 0;
    EventHandlerBase() {}
    virtual ~EventHandlerBase() {}
};

// event handlers 

template <typename ListenerT,typename ... Args>
class EventHandler : public EventHandlerBase<Args ...>
{
    typedef void (ListenerT::*PtrMember)(Args ... args);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler(ListenerT* object, PtrMember member)
    :EventHandlerBase<Args ...>(), m_object(object), m_member(member)
    {}
    virtual ~EventHandler() {}
    
    void notify(Args ... args)
    {
        (m_object->*m_member)(args...);
    }
};

template <typename ... Args>
class Event
{
    typedef std::map<int,EventHandlerBase<Args ...> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    Event()
    : m_count(0) {}
    
    ~Event()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); ++it)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    int Attach(ListenerT* object,void (ListenerT::*member)(Args ... ))
    {
        ++m_count;
        m_handlers[m_count] = (new EventHandler<ListenerT,
                               Args ...>(object,member));
        return m_count;
    }
    
    bool Deattach(int event_id)
    {
        HandlersIterator it = m_handlers.find(event_id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        return true;
    }

	void Reset()
	{
		m_handlers.clear();
	}
    
    void operator ()(Args ... args)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); ++it)
            it->second->notify(args...);  
    }
};

}
#endif