#ifndef SHAREDQUEUE__H
#define SHAREDQUEUE__H

#include "../thread/SpinLock.h"
#include "../thread/Semaphore.h"

namespace CoreObjectLib 
{
        
   template <class T>
   class SharedQueue
        {
        private: 
            uint32_t _size;
            SpinLock _lock;
            Semaphore *_sem_get,*_sem_put;
            std::queue<T> _queue;            
            bool _exit;
            uint32_t _max_size;
        public:
            SharedQueue(uint32_t max_size = 3)
            :_exit(false),_max_size(max_size)
            {
                _size = 0;
                _sem_get = new Semaphore;
                _sem_put = new Semaphore(_max_size);
            }
            
            virtual ~SharedQueue()
            {
                delete _sem_get;
                delete _sem_put;
            }
            
            bool Pop(T *pkt)
            {
                if (_exit)
                    return false;
                
                _sem_get->Wait();
                if (_exit)
                    return false;
                
                _lock.Lock();
                *pkt = _queue.front();
                _queue.pop();
                --_size ;
                _lock.UnLock();
                _sem_put->Post();
                
                return true;
            }
            
            bool Push(const T &pkt)
            {
                if (_exit)
                    return false;
                _sem_put->Wait();
                if (_exit)
                    return false;
                
                _lock.Lock();
                _queue.push(pkt);
                ++_size;
                _lock.UnLock();
                _sem_get->Post();
                
                return true;
            }
            
            void StopAll(uint32_t num_threads = 1)
            {
                _exit = true;
                for (uint32_t i = 0; i < num_threads; i ++)
                {
                    _sem_get->Post();
                    _sem_put->Post();
                }
            }
            
            void Restart()
            {
                _size = 0;
                _exit = false;
                
                delete _sem_get;
                delete _sem_put;
                
                _sem_get = new Semaphore;
                _sem_put = new Semaphore(_max_size);
            }
            
            void GetQueue(std::queue<T>** q)
            {
                *q =&_queue;
            }
            
            uint32_t GetSize()
            {
                return _size;
            }            
        };    
}

#endif