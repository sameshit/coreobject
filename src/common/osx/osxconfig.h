#ifndef OSXCONFIG___H
#define OSXCONFIG___H

#define LIBEXPORT __attribute__ ((visibility ("default")))


// macs
#include <mach/mach_init.h>
#include <mach/mach_time.h>
#include <mach/semaphore.h>
#include <mach/task.h>
#include <libkern/OSAtomic.h>

// unix
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/sysctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <unistd.h>

#ifdef DEBUG
#define DEBUG_MODE_ON
#endif

#endif
