#ifndef GLOBALCONFIG___H
#define GLOBALCONFIG___H

#ifdef _WIN32
	#define OS_WIN
#elif __APPLE__
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
		#define OS_IOS
    #elif TARGET_IPHONE_SIMULATOR
		#define OS_IOS
    #elif defined(TARGET_OS_MAC)
		#define OS_X
    #endif
#elif __linux
	#define OS_LINUX
#endif


// C++
#include <string>
#include <sstream>
#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <queue>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <functional>

// C
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>


#if defined(OS_X) || defined (OS_IOS)
#include "osx/osxconfig.h"
#endif

#if defined(OS_LINUX)
#include "linux/linuxconfig.h"
#endif

#if defined(OS_WIN)
#include "windows/winconfig.h"
#endif

#define MY_MIN(less,a,b) less(a,b)?a:b
#define MY_MAX(less,a,b) less(a,b)?b:a

#endif
