#ifndef LINUXCONFIG_H
#define LINUXCONFIG_H

#define LIBEXPORT __attribute__ ((visibility ("default")))

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <stdint.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdarg>
#include <sys/time.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <netdb.h>
#include <semaphore.h>
#include <execinfo.h>
#include <signal.h>

#endif
