#ifndef WINDOWSCONFIG__H
#define WINDOWSCONFIG__H

#pragma warning( disable: 4251 )

#define LIBEXPORT //__declspec(dllexport)

#pragma once

#include <stdint.h>
#include <WS2tcpip.h>
#include <WinSock2.h>
#include <MSWSock.h>
#include <windows.h>
#include <time.h>

#ifndef ssize_t
#define ssize_t int
#endif

#ifndef suseconds_t
#define suseconds_t long
#endif

#ifndef socklen_t
#define socklen_t int
#endif

#ifdef _DEBUG
#define DEBUG_MODE_ON
#endif

#endif