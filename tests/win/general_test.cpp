#include "../../src/CoreObject.h"
#include "../tests/network/io_test.h"
#include "../tests/network/thread_safe_test.h"
#include "../tests/time/time_test.h"
#include "../tests/variant/VariantTest.h"
#include "../../src/core/error/COErr.h"
#include "../tests/scheduler/SchedulerTest.h"

using namespace CoreObjectLib;
using namespace std;

class CrashReportHandle
{
public:
	void GetReport(Variant &report)
	{
		string str_report;

		report.SerializeXML(&str_report);
		cout << str_report;
	}
};

int main ()
{
	CoreObject *_core;
//	IOTest *test;
    VariantTest *test;
//	SchedulerTest *test;

	_core = new CoreObject;
	fast_new(test,_core);
	test->Test1();
	cin.get();
	fast_delete(test);
    delete _core;


	return 0;
}