#include "../tests/network/io_test.h"
#include "../tests/time/time_test.h"
#include "../tests/network/thread_safe_test.h"
#include "../tests/variant/VariantTest.h"
using namespace CoreObjectLib;
using namespace std;

class CrashHandler
{
    public:
        CrashHandler() {}
        virtual ~CrashHandler() {}

        void ProcessCrash(Variant &report)
        {
            string str;

            report.SerializeXML(&str);
            cout << str << endl;
        }
};

int main()
{
    CoreObject *_core = new CoreObject();
    CrashHandler handler;

    if (!CrashReport::Register())
        FATAL_ERROR("Couldn't register crash report: "<<COErr::Get());

    CrashReport::OnCrash.Attach(&handler,&CrashHandler::ProcessCrash);

    cin.get();

    if (!CrashReport::UnRegister())
        FATAL_ERROR("Couldn't register crash report: "<<COErr::Get());


    delete _core;
    return 0;
}
