#include "../tests/time/time_test.h"
#include "../tests/network/io_test.h"
#include "../tests/network/thread_safe_test.h"
#include "../tests/sharedqueue/SharedQueueTest.h"
#include "../tests/scheduler/SchedulerTest.h"


using namespace CoreObjectLib;
using namespace std;



int main ()
{

    CoreObject *_core = new CoreObject;
    SchedulerTest *st;
    
    fast_new1(SchedulerTest,st,_core);
    
    assert(st->Test());
    
    fast_delete(SchedulerTest,st);
    
    delete _core;
    return 0;
}