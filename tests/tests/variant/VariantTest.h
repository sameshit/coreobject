//
//  VariantTest.h
//  CoreObject
//
//  Created by Oleg on 20.06.13.
//
//

#ifndef __CoreObject__VariantTest__
#define __CoreObject__VariantTest__

#include "../../../src/CoreObject.h"

namespace CoreObjectLib
{
    class VariantTest
    {
    public:
        VariantTest(CoreObject *core);
        virtual ~VariantTest();
        
        bool Test1();
    private:
        CoreObject *_core;
    };
}

#endif /* defined(__CoreObject__VariantTest__) */
