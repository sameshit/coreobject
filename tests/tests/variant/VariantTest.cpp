//
//  VariantTest.cpp
//  CoreObject
//
//  Created by Oleg on 20.06.13.
//
//

#include "VariantTest.h"

using namespace CoreObjectLib;
using namespace std;

VariantTest::VariantTest(CoreObject *core)
:_core(core)
{

}

VariantTest::~VariantTest()
{

}

bool VariantTest::Test1()
{
    Variant variant("good"),variant2;
    string str;
    
    variant = "asdasd";
    
    variant["another"] = false;
    variant["sub"]["good"] = 22.2;
    variant["hello"]["hello"]["hello"] = -1;
    variant["bad"]["hello"] = "hello";
    variant.SerializeXMLToFile("test.xml");
    LOG_INFO(endl<< str);
    
    variant2.DeserializeXMLFromFile("test.xml");
    str.clear();
    variant2.SerializeXML(&str);
    
    LOG_INFO(endl<< str);
    
    assert(!variant2["another"].AsBool());
    LOG_INFO (variant2["another"].AsString());
    
    return true;
}