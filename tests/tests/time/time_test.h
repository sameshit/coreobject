#ifndef TIMETEST__H
#define TIMETEST__H

#include "../../../src/CoreObject.h"
#include "../../../src/core/time/Timer.h"


class TimeTest
{
	private:
		CoreObjectLib::CoreObject *_core;
		void OnTimer();
	public:
    
	TimeTest(CoreObjectLib::CoreObject *core);
    virtual ~TimeTest();
    
    void Test1();
    void Test2();
	void Test3();
};

#endif
