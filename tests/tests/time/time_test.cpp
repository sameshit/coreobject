#include "time_test.h"

using namespace CoreObjectLib;
using namespace std;

TimeTest::TimeTest(CoreObject *core)
	:_core(core)
{

}

TimeTest::~TimeTest()
{

}

void TimeTest::Test1()
{
	COTime tm1,tm2;
	set<COTime> time_set;
	set<COTime>::iterator beg_it;

	tm1.Update();

	tm1 = tm2;
	assert(tm1 == tm2);

	tm2 += 100;

	assert(tm2 - tm1 == 100);
	assert(tm1 < tm2);
	assert(tm2 > tm1);

	time_set.insert(tm2);
	time_set.insert(tm1);

	beg_it = time_set.begin();

	assert(tm1 == (*beg_it));

	++beg_it;

	assert(tm2 == (*beg_it));

	printf("TIME TEST PASSED!\n");
}

void TimeTest::Test2()
{
	COTime tm1,tm2;
	set<COTime> time_set;
	set<COTime>::iterator beg_it;

	tm1 = tm2;
	assert(tm1 == tm2);

	Utils::Delay(1000);
	tm2.Update();

	uint64_t t1 = tm2 - tm1;

	assert(tm2 - tm1 > 950 && tm2 - tm1 < 1050);
	assert(tm1 < tm2);
	assert(tm2 > tm1);

	time_set.insert(tm2);
	time_set.insert(tm1);

	beg_it = time_set.begin();

	assert(tm1 == (*beg_it));

	++beg_it;

	assert(tm2 == (*beg_it));

	printf("TIME TEST 2 PASSED!\n");
}

void TimeTest::Test3()
{
	Timer *timer;

	fast_new(timer,_core,5);

	timer->OnTimer.Attach<TimeTest>(this,&TimeTest::OnTimer);

	cin.get();

	fast_delete(timer);
}

void TimeTest::OnTimer()
{
	LOG_INFO ("on timer!");
}
