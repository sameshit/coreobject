#include "SchedulerTest.h"

using namespace CoreObjectLib;

SchedulerTest::SchedulerTest(CoreObject *core)
:_core(core)
{
    fast_new(_task);
    _task->SetTimeout(4000);
}

SchedulerTest::~SchedulerTest()
{
    fast_delete( _task);
}

void SchedulerTest::OnSchedule()
{
    LOG_INFO("On Schedule !!");
    _sem.Post();
}


bool SchedulerTest::Test()
{
    LOG_INFO("Start schedule");
    _task->OnTimeTask.Attach(this, &SchedulerTest::OnSchedule);
    
    _core->GetScheduler()->Add(_task);
    
    return _sem.Wait();
}