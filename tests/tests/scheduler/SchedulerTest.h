#ifndef CoreObject_SchedulerTest_h
#define CoreObject_SchedulerTest_h


#include "../../../src/CoreObject.h"

class SchedulerTest
{
private:
    CoreObjectLib::CoreObject *_core;    
    void OnSchedule();
    CoreObjectLib::Semaphore _sem;
    CoreObjectLib::TimeTask *_task;
public:
    SchedulerTest(CoreObjectLib::CoreObject *core);
    virtual ~SchedulerTest();
    
    bool Test();
};

#endif
