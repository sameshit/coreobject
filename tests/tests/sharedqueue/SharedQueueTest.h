#ifndef SHAREDQUEUETEST___H
#define SHAREDQUEUETEST___H

#include "../../../src/CoreObject.h"

typedef struct SharedStruct
{
    uint8_t val1;
    uint32_t val2;
}SharedStruct;

class SharedQueueTest
{
private:
    CoreObjectLib::CoreObject *_core;
    CoreObjectLib::Thread *_thread1,*_thread2;
    CoreObjectLib::SharedQueue<SharedStruct> *_sq;
    SharedStruct _some_struct[5];
    uint32_t _ri,_wi;
public:
    SharedQueueTest(CoreObjectLib::CoreObject *core);
    virtual ~SharedQueueTest();
    void OnThread1();    
    void OnThread2();
};

#endif
