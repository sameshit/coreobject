#include "SharedQueueTest.h"

using namespace CoreObjectLib;
using namespace std;

SharedQueueTest::SharedQueueTest(CoreObject *core)
:_core(core)
{
    _ri = _wi = 0;
    srand((unsigned int)time(NULL));
    for (int i = 0 ; i < 5 ; i ++)
    {
        _some_struct[i].val1 = rand() % 255;
        _some_struct[i].val2 = rand() % 4294967295U;
    }
    _thread1 = _core->GetThread();
    _thread2 = _core->GetThread();

    _thread1->OnThread.Attach(this, &SharedQueueTest::OnThread1);
    _thread2->OnThread.Attach(this, &SharedQueueTest::OnThread2);

    fast_new( _sq, 5);

    _thread1->Wake();
    _thread2->Wake();
}


SharedQueueTest::~SharedQueueTest()
{
    _sq->StopAll(1);
    _core->ReleaseThread(&_thread1);
    _core->ReleaseThread(&_thread2);

    fast_delete(_sq);
}

void SharedQueueTest::OnThread1()
{
    COTime t1;
    if (!_sq->Push(_some_struct[_wi]))
        return;
    COTime t2;

    ++_wi;
    if (_wi == 5)
        _wi = 0;


    LOG_INFO("write event took "<< t2-t1 << " milliseconds");
}
void SharedQueueTest::OnThread2()
{
    SharedStruct some_struct = {0,0};
    COTime t1;

    if (!_sq->Pop(&some_struct))
        return;

    assert(some_struct.val1 == _some_struct[_ri].val1 && some_struct.val2 == _some_struct[_ri].val2);

    ++ _ri;
    if (_ri == 5)
        _ri = 0;

    COTime t2;
    LOG_INFO( "read event took "<< t2 -t1 <<" milliseconds");

    Utils::Delay(100);
}

