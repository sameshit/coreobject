#include "io_test.h"

using namespace CoreObjectLib;
using namespace std;

IOTest::IOTest(CoreObject *core) 
:_core(core)
{

    
   tcpServer = new TcpServer(_core);
   tcpClient = new TcpClient(_core);

   udpSocket1 = new UdpSocket(_core);
   udpSocket2 = new UdpSocket(_core);

   timer = new Timer(_core, 1000);
   idle = new LoopIdle(_core);
        
   tcpServer->OnConnect.Attach<IOTest>(this, &IOTest::OnServerConnect);
   tcpServer->OnDisconnect.Attach<IOTest>(this, &IOTest::OnServerDisconnect);
   tcpServer->OnRecv.Attach<IOTest>(this, &IOTest::OnServerRecv);

   tcpClient->OnConnect.Attach<IOTest>(this, &IOTest::OnClientConnect);
   tcpClient->OnDisconnect.Attach<IOTest>(this, &IOTest::OnClientDisconnect);
   tcpClient->OnRecv.Attach<IOTest>(this, &IOTest::OnClientRecv);        

   udpSocket1->OnRecvFrom.Attach<IOTest>(this, &IOTest::OnRecvFromSock1);
   udpSocket2->OnRecvFrom.Attach<IOTest>(this, &IOTest::OnRecvFromSock2);

   timer->OnTimer.Attach<IOTest>(this, &IOTest::OnTimer);
   idle->OnLoopIdle.Attach<IOTest>(this, &IOTest::OnIdle);

   ASSERT_ERROR(udpSocket1->Bind(1357));
   ASSERT_ERROR(udpSocket2->Bind(2468));
   ASSERT_ERROR(tcpServer->Start(12345)); 
   ASSERT_ERROR(tcpClient->Connect((char *)"127.0.0.1", 12345));
}

IOTest::~IOTest()
{
		delete idle;
        delete timer;

        delete udpSocket2;
        delete udpSocket1;

		delete tcpClient;		
	
		Utils::Delay(1000);
		delete tcpServer;			
}
    
void IOTest::OnServerConnect(TcpSession *tcpSession)
{
        std::cout << "TcpServer: connected new session: " << tcpSession->GetHostName() << ":" << tcpSession->GetPort() << std::endl;
}    
    
void IOTest::OnServerDisconnect(TcpSession *tcpSession)
{
        std::cout << "TcpServer: disconnected session: " << tcpSession->GetHostName() << ":" << tcpSession->GetPort() << std::endl;
}
    
void IOTest::OnServerRecv(TcpSession *tcpSession, uint8_t *data, ssize_t &size)
{
      std::cout << "TcpServer: received " << size << " byte(-s) from " << tcpSession->GetHostName() << ":" << tcpSession->GetPort() << "." << std::endl;
      tcpServer->Send(tcpSession, data, size);
}
    
void IOTest::OnClientConnect()
{
      std::cout << "TcpClient: connected to server: " << tcpClient->GetHostName() << ":" << tcpClient->GetPort() << std::endl;
}
    
void IOTest::OnClientDisconnect()
{
    std::cout << "TcpClient: disconnected from server: " << tcpClient->GetHostName() << ":" << tcpClient->GetPort() << std::endl;
}
    
void IOTest::OnClientRecv(uint8_t *data, ssize_t &size)
{
        std::cout << "TcpClient: received " << size << " byte(-s)! " << std::endl;
}
    
void IOTest::OnRecvFromSock1(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len)
{
    std::string str((char *)data,size);
	std::cout << "UdpSocket1: onrecvfrom: (" << udpSocket1->GetHostName() << ":" << udpSocket1->GetPort() << ") : ";
    std::cout << "Len = " << size << ", Data = ";
    cout << str << endl;
}
    
void IOTest::OnRecvFromSock2(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len)
{
    std::string str((char *)data,size);
    std::cout << "UdpSocket2: onrecvfrom: (" << udpSocket2->GetHostName() << ":" << udpSocket2->GetPort() << ") : ";
    std::cout << "Len = " << size << ", Data = ";
    cout << str << endl;
}

void IOTest::SendFirst()
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(2468);
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
        
    int s = 3;

	ASSERT_ERROR(udpSocket1->SendTo((uint8_t *)"0001", 4, (uint8_t *)&s, 1, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)));
}
    
void IOTest::SendSecond()
{
   struct sockaddr_in addr;
   addr.sin_family = AF_INET;
   addr.sin_port = htons(1357);
   addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	

ASSERT_ERROR(udpSocket2->SendTo((uint8_t *)"1234", 4, (uint8_t *)"a", 1, (uint8_t *)"Hello, world!", 14, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)));
}
    
void IOTest::OnTimer()
{
   std::cout << "OnTimer!" << std::endl;
        
   tcpClient->Send((uint8_t *)"Hello, world!", 14);
        
   SendFirst();
   SendSecond();
}
    
void IOTest::OnIdle()
{
        std::cout << "OnLoopIdle!" << std::endl;
}