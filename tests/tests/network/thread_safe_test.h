#ifndef TESTTHREADSAFE_H
#define TESTTHREADSAFE_H

#include "../../../src/core/protocols/TCP/TcpServer.h"
#include "../../../src/core/protocols/TCP/TcpClient.h"
#include "../../../src/core/protocols/TCP/TcpSession.h"
#include "../../../src/core/protocols/UDP/UdpSocket.h"
#include "../../../src/core/time/Timer.h"
#include "../../../src/core/thread/Thread.h"
#include "../../../src/core/loop/LoopIdle.h"
#include "../../../src/CoreObject.h"

class ThreadSafeTest
{
    CoreObjectLib::CoreObject *_core;
    
    CoreObjectLib::TcpServer *server;
    CoreObjectLib::TcpClient *client;
    
    CoreObjectLib::Thread *thread;
public:
    ThreadSafeTest(CoreObjectLib::CoreObject *core);
    virtual ~ThreadSafeTest();


	void OnClientConnect();
	void OnClientDisconnect();
	void OnServerDisconnect(CoreObjectLib::TcpSession *session);
    void OnServerConnect(CoreObjectLib::TcpSession *session);    
    void OnThreadCallback();
};

#endif //TEST2_H
