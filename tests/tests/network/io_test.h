#ifndef IOTEST_H
#define IOTEST_H

#include "../../../src/core/protocols/TCP/TcpServer.h"
#include "../../../src/core/protocols/TCP/TcpClient.h"
#include "../../../src/core/protocols/TCP/TcpSession.h"
#include "../../../src/core/protocols/UDP/UdpSocket.h"
#include "../../../src/core/time/Timer.h"
#include "../../../src/core/loop/LoopIdle.h"
#include "../../../src/CoreObject.h"

class IOTest
{
    CoreObjectLib::CoreObject	*_core;
    
    CoreObjectLib::TcpServer   *tcpServer;
    CoreObjectLib::TcpClient   *tcpClient;
    
    CoreObjectLib::UdpSocket   *udpSocket1;
    CoreObjectLib::UdpSocket   *udpSocket2;

    CoreObjectLib::Timer       *timer;
    CoreObjectLib::LoopIdle    *idle;
public:
    IOTest(CoreObjectLib::CoreObject *core);
    
    virtual ~IOTest();
    
    void OnServerConnect(CoreObjectLib::TcpSession *tcpSession);
    void OnServerDisconnect(CoreObjectLib::TcpSession *tcpSession);    
    void OnServerRecv(CoreObjectLib::TcpSession *tcpSession, uint8_t *data, ssize_t &size);
    void OnClientConnect();    
    void OnClientDisconnect();    
    void OnClientRecv(uint8_t *data, ssize_t &size);
    void OnRecvFromSock1(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len);    
	void OnRecvFromSock2(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len);
    void SendFirst();
    void SendSecond();
    void OnTimer();    
    void OnIdle();
};

#endif //TEST_H
