#include "udp_test.h"

using namespace CoreObjectLib;
using namespace std;

UdpTest::UdpTest(CoreObject *core)
	:_core(core)
{
	fast_new(_udp_sock1,_core);
	fast_new(_udp_sock2,_core);

	ASSERT_ERROR(_udp_sock1->Bind(12345));
	ASSERT_ERROR(_udp_sock2->Bind(12346));

	fast_new(_timer,_core,1000);
	_timer->OnTimer.Attach<UdpTest>(this,&UdpTest::OnTimer);
	
	_udp_sock2->OnRecvFrom.Attach<UdpTest>(this,&UdpTest::OnRecvFrom);
}

UdpTest::~UdpTest()
{
	fast_delete(_timer);
	fast_delete(_udp_sock1);
	fast_delete(_udp_sock2);
}

void UdpTest::OnTimer()
{
	struct sockaddr_in addr;
	string str("hello_world!");

	addr.sin_family = AF_INET;
    addr.sin_port = htons(12346);
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ASSERT_ERROR(
		_udp_sock1->SendTo((uint8_t*)str.c_str(),str.size(),(struct sockaddr*)&addr,sizeof(struct sockaddr_in))
	);
}

void UdpTest::OnRecvFrom(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len)
{
	string str((char *)data,size);

	LOG_INFO(str);
}