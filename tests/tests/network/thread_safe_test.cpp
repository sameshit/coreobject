#include "thread_safe_test.h"

using namespace CoreObjectLib;

ThreadSafeTest::ThreadSafeTest(CoreObject *core) : _core(core)
{
        server = new TcpServer(_core);
        client = new TcpClient(_core);
        
        server->OnConnect.Attach(this, &ThreadSafeTest::OnServerConnect);
		server->OnDisconnect.Attach<ThreadSafeTest>(this,&ThreadSafeTest::OnServerDisconnect);
        
        ASSERT_ERROR(server->Start(12345));

		client->OnDisconnect.Attach<ThreadSafeTest>(this,&ThreadSafeTest::OnClientDisconnect);
		client->OnConnect.Attach<ThreadSafeTest>(this,&ThreadSafeTest::OnClientConnect);
        ASSERT_ERROR(client->Connect("127.0.0.1", 12345));

		thread = _core->GetThread();
        thread->OnThread.Attach<ThreadSafeTest>(this, &ThreadSafeTest::OnThreadCallback);
        thread->Wake();
}
ThreadSafeTest::~ThreadSafeTest()
{
        _core->ReleaseThread(&thread);
        if (client !=0)
			delete client;
		if (server !=0)
			delete server;
}

void ThreadSafeTest::OnClientConnect()
{
   	Utils::Delay(3000);
	LOG_INFO ("Client connected");
}

void ThreadSafeTest::OnClientDisconnect()
{
	LOG_INFO("Client disconnected");
}

void ThreadSafeTest::OnServerDisconnect(TcpSession *session)
{
	LOG_INFO("Server OnDisconnect");
}
    
void ThreadSafeTest::OnServerConnect(TcpSession *session)
{
    LOG_INFO("OnServerConnect: " << session->GetHostName() << ":" << session->GetPort());
}
    
void ThreadSafeTest::OnThreadCallback()
{
    Utils::Delay(2000);
/*    if(server != 0)
    {
       delete server;
       LOG "Server deleted!" << std::endl;            
       server = 0;
    }*/
    
	if (client != 0)
	{
		delete client;
		LOG_INFO("Client deleted!");
	}
	client = 0;
}

