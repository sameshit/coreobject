#ifndef UDPTEST__H
#define UDPTEST__H

#include "../../../src/core/protocols/UDP/UdpSocket.h"
#include "../../../src/CoreObject.h"
#include "../../../src/core/time/Timer.h"

class UdpTest
{
	private:
		CoreObjectLib::CoreObject *_core;
		CoreObjectLib::Timer* _timer;
		CoreObjectLib::UdpSocket *_udp_sock1,*_udp_sock2;
		void OnTimer();
		void OnRecvFrom(uint8_t *data, ssize_t &size, sockaddr *addr, socklen_t &len);
	public:
		UdpTest(CoreObjectLib::CoreObject *core);
		virtual ~UdpTest();
};

#endif